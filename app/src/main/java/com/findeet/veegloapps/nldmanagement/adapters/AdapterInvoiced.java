package com.findeet.veegloapps.nldmanagement.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.findeet.veegloapps.nldmanagement.R;

import java.util.ArrayList;

/**
 * Created by DEBONGZ on 1/6/2018.
 */

public class AdapterInvoiced extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private LayoutInflater inflater;

    ArrayList<String> ocust_number;
    ArrayList<String> ocust_addr;
    ArrayList<String> oorder_total;
    ArrayList<String> odate_of_sale;
    ArrayList<String> oorder_id;
    ArrayList<String> oorder_synce_status;


    ArrayList<String> oitem_synce_state;
    int currentPos=0;

    // create constructor to initialize context and data sent from MainActivity
    public AdapterInvoiced(Context context, ArrayList<String> cust_number, ArrayList<String> cust_addr, ArrayList<String> order_total, ArrayList<String> date_of_sale, ArrayList<String> order_id, ArrayList<String> order_synce_status){
        this.context=context;
        inflater= LayoutInflater.from(context);
        this.ocust_number = cust_number;
        this.ocust_addr = cust_addr;
        this.oorder_total = order_total;
        this.odate_of_sale = date_of_sale;
        this.oorder_id = order_id;
        this.oorder_synce_status = order_synce_status;

    }

    // Inflate the layout when ViewHolder created
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=inflater.inflate(R.layout.container_quotes, parent,false);
        MyHolder holder=new MyHolder(view);

        return holder;
    }

    // Bind data
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        // Get current position of item in RecyclerView to bind data and assign values from list
        MyHolder myHolder= (MyHolder) holder;
        myHolder.txtQcustName.setText(ocust_number.get(position));
        myHolder.txtQorderTotal.setText(oorder_total.get(position));
        myHolder.txtQOrderDate.setText("IN-"+odate_of_sale.get(position));
        //myHolder.txtQOrderNum.setText(oorder_id.get(position));
        myHolder.txtQCustAddr.setText(ocust_addr.get(position));
        myHolder.Isig_img.setVisibility(View.VISIBLE);

        String image =oorder_id.get(position);

        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.placeholder2)
                .error(R.drawable.placeholder2);

        Glide.with(context).load(image).apply(options).into(myHolder.Isig_img);



    }
    // return total item from List
    @Override
    public int getItemCount() {
        return oorder_id.size();
    }

    class MyHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView txtQcustName;
        TextView txtQorderTotal;
        TextView txtQOrderDate;
        TextView txtQOrderNum;
        TextView txtQCustAddr;
        RelativeLayout Porder_sync_prog;
        RelativeLayout Rsent_badge;
        ImageView Isig_img;




        // create constructor to get widget reference
        public MyHolder(View itemViewa) {
            super(itemViewa);
            txtQcustName = (TextView) itemViewa.findViewById(R.id.txtqname);
            txtQorderTotal = (TextView) itemViewa.findViewById(R.id.txtqtotal);
            txtQOrderDate = (TextView) itemViewa.findViewById(R.id.txtqdate);
            txtQOrderNum = (TextView) itemViewa.findViewById(R.id.txtqid);
            txtQCustAddr = (TextView) itemViewa.findViewById(R.id.txtqcustAddr);
            Porder_sync_prog =(RelativeLayout)itemViewa.findViewById(R.id.order_sync_status);
            Rsent_badge =(RelativeLayout) itemViewa.findViewById(R.id.sent_badge);
            Isig_img =(ImageView) itemViewa.findViewById(R.id.sig_img);
            itemViewa.setOnClickListener(this);
        }

        // Click event for all items
        @Override
        public void onClick(View v) {
//
//            String shop_number = "";
//            String shop_name = txtQcustName.getText().toString();
//            String shop_addr = txtQCustAddr.getText().toString();
//            String uniqueID = txtQOrderNum.getText().toString();
//
//            SharedPreferences shared = context.getSharedPreferences("Order", Context.MODE_PRIVATE);
//            SharedPreferences.Editor editor = shared.edit();
//            editor.putString("order_id", uniqueID);
//            editor.putString("cust_number", shop_number);
//            editor.putString("cust_name", shop_name);
//            editor.putString("cust_addr", shop_addr);
//            editor.commit();
//
//
//            Intent intent= new Intent(context, Order.class);
//            context.startActivity(intent);
//            //finish this
//            ((Activity)context).finish();


// go to the new intent with the shop name and adress, and send it to database to create new invoive
//            Intent intent=new Intent(context.getApplicationContext(), Order.class);
//            intent.putExtra("cust_number", shop_number);
//            context.getApplicationContext().startActivity(intent);


        }
    }


}
