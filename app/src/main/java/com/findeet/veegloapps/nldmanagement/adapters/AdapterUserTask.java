package com.findeet.veegloapps.nldmanagement.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.findeet.veegloapps.nldmanagement.R;
import com.findeet.veegloapps.nldmanagement.datas.DataNot;
import com.findeet.veegloapps.nldmanagement.datas.DataUserTask;

import java.util.Collections;
import java.util.List;


/**
 * Created by DEBONGZ on 3/15/2018.
 */

public class AdapterUserTask extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private LayoutInflater inflater;
    List<DataUserTask> data= Collections.emptyList();
    DataNot current;
    int currentPos=0;
    Bitmap bitmap;


    // create constructor to initialize context and data sent from MainActivity
    public AdapterUserTask(Context context, List<DataUserTask> data){
        this.context=context;
        inflater= LayoutInflater.from(context);
        this.data=data;
    }
    // Inflate the layout when ViewHolder created
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=inflater.inflate(R.layout.container_user_tasks, parent,false);
        MyHolder holder=new MyHolder(view);
        return holder;
    }
    // Bind data
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        // Get current position of item in RecyclerView to bind data and assign values from list
        final MyHolder myHolder= (MyHolder) holder;
        final DataUserTask current=data.get(position);

        myHolder.txttask_id.setText(current.task_id);
        myHolder.txttask.setText(current.task_desc);
        myHolder.txtstart_time.setText("From: "+current.task_start_time);
        myHolder.txtend_time.setText("To: "+current.task_end_time);
        myHolder.txtuser_name.setText(current.task_user_name);


    }
    // return total item from List
    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyHolder extends RecyclerView.ViewHolder  {
        TextView txttask_id,txtstart_time,txtend_time,txttask,txtuser_name;
        LinearLayout line;

        // create constructor to get widget reference
        public MyHolder(View itemView) {
            super(itemView);

            line = (LinearLayout) itemView.findViewById(R.id.quote_line);
            txttask_id= (TextView) itemView.findViewById(R.id.task_id);
            txtend_time = (TextView) itemView.findViewById(R.id.end_time);
            txtstart_time = (TextView) itemView.findViewById(R.id.start_time);
            txttask = (TextView) itemView.findViewById(R.id.task);
            txtuser_name = (TextView) itemView.findViewById(R.id.user_name);


            line.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {

//                                            String not_type=txtnot_type.getText().toString();
//                                            String order_id=txtorder_id.getText().toString() ;
//                                            String shop_id=txtshop_id.getText().toString();
//
//
//                                            if (not_type.equalsIgnoreCase("order_status")|| not_type.isEmpty()|| not_type.equalsIgnoreCase(null)){
//                                                String link = "http://nextld.co.za/salesadmin/finalinvoice.php?order_id="+order_id+
//                                                        "&shop_id="+shop_id;
//
//                                                Intent intent = new Intent(Intent.ACTION_VIEW);
//                                                intent.setData(Uri.parse(link));
//                                                context.startActivity(intent);
//                                            }
//                                            else if(not_type.equalsIgnoreCase("due_not")){
//                                                String link = "http://nextld.co.za/salesadmin/due_invoices.php";
//
//                                                Intent intent = new Intent(Intent.ACTION_VIEW);
//                                                intent.setData(Uri.parse(link));
//                                                context.startActivity(intent);
//
//                                            }




                                        }
                });




        }





    }





}



