package com.findeet.veegloapps.nldmanagement.picker;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.findeet.veegloapps.nldmanagement.R;
import com.findeet.veegloapps.nldmanagement.Utils.SQLiteHelperb;
import com.findeet.veegloapps.nldmanagement.datas.DataOrder;
import com.findeet.veegloapps.nldmanagement.picker.adapter.AdapterOrder;
import com.findeet.veegloapps.nldmanagement.picker.adapter.AdapterProducts;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Order extends AppCompatActivity implements View.OnClickListener{
    TextView cust_name,shop_addr,sales_person,save_quote;
    ProgressBar loadprobar,loaditem;
    LinearLayout notdiv;
    // CONNECTION_TIMEOUT and READ_TIMEOUT are in milliseconds
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 15000;
    private RecyclerView mycustRecycleViewa,zmycustRecycleViewa;
    private AdapterProducts mAdaptera;
    private AdapterOrder zmAdaptera;
    RelativeLayout pop;
    EditText editText;
    TextView order_note;
    String url_save = "http://nextld.co.za/salesadmin/app/save_order2.php";
    String url_note = "http://nextld.co.za/salesadmin/app/select_note.php";
    ProgressDialog progressDialog;

    SQLiteDatabase sqLiteDatabase;
    ArrayList<String> p_name_Array;
    ArrayList<String> p_id_NAME_Array;
    ArrayList<String> p_unit_Array;

    SQLiteHelperb sqLiteHelperb;
    Cursor cursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order2);

        sqLiteHelperb = new SQLiteHelperb(this);
        //Make call to AsyncTask
        p_name_Array = new ArrayList<String>();

        p_id_NAME_Array = new ArrayList<String>();

        p_unit_Array = new ArrayList<String>();

        notdiv =(LinearLayout) findViewById(R.id.note) ;
        order_note =(TextView)findViewById(R.id.thenotetxtx);
        editText = (EditText) findViewById(R.id.serach_txt);
        cust_name = (TextView) findViewById(R.id.shop_name);
        shop_addr = (TextView) findViewById(R.id.shop_addr);
        sales_person = (TextView) findViewById(R.id.sales_person);
        loadprobar = (ProgressBar) findViewById(R.id.load_re_items);
        loaditem = (ProgressBar) findViewById(R.id.load_order);
        save_quote = (TextView) findViewById(R.id.save_quote);

        SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
        final String repname = shared.getString("user_name", "");

        SharedPreferences ordershared = getSharedPreferences("Order", Context.MODE_PRIVATE);
        final String order_id = ordershared.getString("order_id", "");
        String cust_nu = ordershared.getString("cust_number", "");
        String cust_na = ordershared.getString("cust_name", "");
        final String cust_addr = ordershared.getString("cust_addr", "");
        String seorderid = order_id;
        //showNote(seorderid);

        final String full_shop_name = cust_nu + " " + cust_na;
        String full_addr = cust_addr;
        String full_repname = "Sales Person: " + repname;

        cust_name.setText(full_shop_name);
        shop_addr.setText(full_addr);
        sales_person.setText(full_repname);
        new AsyncFetcha().execute();

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                //Updating Array Adapter ListView after typing inside EditText.
                String e = editText.getText().toString();
                ShowSQLiteDBdata() ;


                //new AsyncFetchSearch().execute();



            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                ShowSQLiteDBdata() ;
                //new AsyncFetchSearch().execute();
            }

            @Override
            public void afterTextChanged(Editable editable) {

                //new AsyncFetchSearch().execute();

            }
        });
    }

    public void hideThePop() {
        pop = (RelativeLayout)findViewById(R.id.add_items_pop_up);
        pop.setVisibility(View.INVISIBLE);
    }

    public void ReloadItems() {
        new AsyncFetcha().execute();
    }

    public void showPop(View view) {
        pop = (RelativeLayout)findViewById(R.id.add_items_pop_up);
        pop.setVisibility(View.VISIBLE);
    }
    @Override
    public void onBackPressed()
    {
        SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
        String repname = shared.getString("user_name", "");
        if (repname.equalsIgnoreCase("alex") || repname.equalsIgnoreCase("james") || repname.equalsIgnoreCase("debongz09")){
            Intent intent = new Intent(Order.this, PickQuotes.class);
            startActivity(intent);
            finish();
        }
        else{
            Intent intent = new Intent(Order.this, PickQuotes.class);
            startActivity(intent);
            finish();

        }




    }

    public void goback(View view) {

        SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
        String repname = shared.getString("user_name", "");
        if (repname.equalsIgnoreCase("alex") || repname.equalsIgnoreCase("james") || repname.equalsIgnoreCase("debongz09")){
            shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
            repname = shared.getString("user_name", "");
            SharedPreferences ordershared = getSharedPreferences("Order", Context.MODE_PRIVATE);
            String order_id = ordershared.getString("order_id", "");
            String cust_nu = ordershared.getString("cust_number", "");
            String cust_na = ordershared.getString("cust_name", "");
            String cust_addr = ordershared.getString("cust_addr", "");


            String full_shop_name = cust_nu + " " + cust_na;
            String full_addr = cust_addr;
            String full_repname = "Sales Person: " + repname;
            String seorderid = order_id;
            String secustname = full_shop_name;
            String secustaddr = cust_addr;
            String sesalesrep = repname;

            SAVEQUOTE(seorderid, secustname, secustaddr, sesalesrep,"");
        }
        else{
            SaveOrder ();
        }




    }

    public void SAVEQUOTE(final String seorderid, final String secustname, final String secustaddr, final String sesalesrep, final String block) {

        Log.i("Hiteshurl", "" + url_save);
        RequestQueue requestQueuea = Volley.newRequestQueue(Order.this);
        StringRequest stringRequesta = new StringRequest(Request.Method.POST, url_save, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                Intent intent = new Intent(Order.this, PickQuotes.class);
                startActivity(intent);
                finish();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i("Hitesh", "" + error);
                Toast.makeText(Order.this, "Error ! Please check your network connection", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                Intent intent = new Intent(Order.this, PickQuotes.class);
                startActivity(intent);
                finish();

            }
        }) {

            // seorderid, secustname, secustaddr, sesalesrep
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> stringMapa = new HashMap<>();
                stringMapa.put("order_id", seorderid);
                stringMapa.put("cust_name", secustname);
                stringMapa.put("cust_addr", secustaddr);
                stringMapa.put("sales_rep", sesalesrep);
                stringMapa.put("block", block);
                return stringMapa;
            }

        };

        requestQueuea.add(stringRequesta);
        //initialize the progress dialog and show it
        progressDialog = new ProgressDialog(Order.this);
        progressDialog.setMessage("Saving ...");
//        progressDialog.setCancelable(false);
        progressDialog.show();

        progressDialog.setCancelable(true);

        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener(){

            public void onCancel(DialogInterface dialog) {
                Toast.makeText(Order.this, "Saving in background", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(Order.this, PickQuotes.class);
                startActivity(intent);
                finish();

            }});

        progressDialog.setOnDismissListener(new DialogInterface.OnDismissListener(){

            public void onDismiss(DialogInterface dialog) {
                // TODO Auto-generated method stub

            }});
        //ends of save request

    }

    public void addNote(View view) {
    }

    public void goback2(View view) {
     finish();

    }

    public class AsyncFetcha extends AsyncTask<String, String, String> {

        HttpURLConnection conn;
        URL urla = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            loaditem.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            try {

                // Enter URL address where your json file resides
                // Even you can make call to php file which returns json data

                urla = new URL("http://nextld.co.za/salesadmin/app/select_items2.php");

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return e.toString();
            }
            try {

                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection) urla.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput to true as we send and recieve data
                conn.setDoInput(true);
                conn.setDoOutput(true);

                SharedPreferences ordershared = getSharedPreferences("Order", Context.MODE_PRIVATE);
                String order_id = ordershared.getString("order_id", "");


                // add parameter to our above url
                Uri.Builder builder = new Uri.Builder().appendQueryParameter("order_number", order_id);
                String query = builder.build().getEncodedQuery();

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();


            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                return e1.toString();
            }

            try {

                int response_code = conn.getResponseCode();

                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    // Pass data to onPostExecute method
                    return (result.toString());

                } else {

                    return ("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return e.toString();
            } finally {
                conn.disconnect();
            }


        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread

            loaditem.setVisibility(View.INVISIBLE);
            List<DataOrder> data = new ArrayList<>();

            loaditem.setVisibility(View.INVISIBLE);
            try {

                JSONArray jArray = new JSONArray(result);

                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {

                    JSONObject json_data = jArray.getJSONObject(i);
                    DataOrder proData = new DataOrder();
                    int orgQ = Integer.parseInt(json_data.getString("item_quantity"));
                    int newQ = Integer.parseInt(json_data.getString("item_new_quantity"));

                    SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
                    String repname = shared.getString("user_name", "");
                    if (repname.equalsIgnoreCase("alex") || repname.equalsIgnoreCase("james") || repname.equalsIgnoreCase("debongz09")){
                        proData.OderState = json_data.getString("edit_stat2");
                    }
                    else{
                        proData.OderState = json_data.getString("edit_stat");
                    }

                    proData.OderName = json_data.getString("item_name");
                    proData.OrderUnit = json_data.getString("item_unit");
                    if (newQ>0){
                        proData.OrderQty = json_data.getString("item_new_quantity");
                    }
                    else{
                        proData.OrderQty = json_data.getString("item_quantity");
                    }

                    proData.OrderID=json_data.getString("order_id");
                    proData.OrderTotal = "R " + json_data.getString("item_total");
                    proData.OrderNum = json_data.getString("item_number");
                    data.add(proData);

                }

                // Setup and Handover data to recyclerview
                zmycustRecycleViewa = (RecyclerView) findViewById(R.id.list_order);
                zmAdaptera = new AdapterOrder(Order.this, data);
                zmycustRecycleViewa.setAdapter(zmAdaptera);
                zmycustRecycleViewa.setLayoutManager(new LinearLayoutManager(Order.this));

            } catch (JSONException e) {
//                Toast.makeText(Order.this, "add iems", Toast.LENGTH_LONG).show();
            }

        }
    }

    private void ShowSQLiteDBdata() {

        sqLiteDatabase = sqLiteHelperb.getWritableDatabase();
        // List<DataCustomers> data=new ArrayList<>();

        String value = "%" + editText.getText().toString() + "%";

        cursor = sqLiteDatabase.rawQuery("SELECT * FROM "+SQLiteHelperb.TABLE_P_NAME+" WHERE "+SQLiteHelperb.Table_Column_1_Subject_Pname+" LIKE '"+value+"'  ", null);
        p_name_Array.clear();
        p_id_NAME_Array.clear();
        p_unit_Array.clear();

        if (cursor.moveToFirst()) {
            do {

                p_id_NAME_Array.add(cursor.getString(cursor.getColumnIndex(SQLiteHelperb.Table_Column_1_Subject_Pid)));

                p_unit_Array.add(cursor.getString(cursor.getColumnIndex(SQLiteHelperb.Table_Column_1_Subject_Punit)));

                p_name_Array.add(cursor.getString(cursor.getColumnIndex(SQLiteHelperb.Table_Column_1_Subject_Pname)));



            }

            while (cursor.moveToNext());
        }

        // Setup and Handover data to recyclerview
        mycustRecycleViewa = (RecyclerView) findViewById(R.id.product_list);
        mAdaptera = new AdapterProducts(Order.this, p_id_NAME_Array,p_name_Array,p_unit_Array);
        mycustRecycleViewa.setAdapter(mAdaptera);
        mycustRecycleViewa.setLayoutManager(new LinearLayoutManager(Order.this));


        cursor.close();
    }


    public void SaveOrder (){
        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(Order.this);
        View mView = layoutInflaterAndroid.inflate(R.layout.order_block_dialog_box, null);
        AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(Order.this);
        alertDialogBuilderUserInput.setView(mView);

        final TextView a1 = (TextView)mView.findViewById(R.id.a1);
        final TextView a2 = (TextView)mView.findViewById(R.id.a2);
        final TextView a3 = (TextView)mView.findViewById(R.id.a3);
        final TextView a4 = (TextView)mView.findViewById(R.id.a4);
        final TextView b1 = (TextView)mView.findViewById(R.id.b1);
        final TextView b2 = (TextView)mView.findViewById(R.id.b2);
        final TextView b3 = (TextView)mView.findViewById(R.id.b3);
        final TextView b4 = (TextView)mView.findViewById(R.id.b4);
        final TextView c1 = (TextView)mView.findViewById(R.id.c1);
        final TextView c2 = (TextView)mView.findViewById(R.id.c2);
        final TextView c3 = (TextView)mView.findViewById(R.id.c3);
        final TextView c4 = (TextView)mView.findViewById(R.id.c4);
        a1.setOnClickListener(this);
        a2.setOnClickListener(this);
        a3.setOnClickListener(this);
        a4.setOnClickListener(this);

        b1.setOnClickListener(this);
        b2.setOnClickListener(this);
        b3.setOnClickListener(this);
        b4.setOnClickListener(this);

        c1.setOnClickListener(this);
        c2.setOnClickListener(this);
        c3.setOnClickListener(this);
        c4.setOnClickListener(this);



        alertDialogBuilderUserInput
                .setCancelable(true)
                 ;

        AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
        alertDialogAndroid.show();
    }
    @Override
    public void onClick(View v){
        switch(v.getId()){
            case R.id.a1:
                // handle button1 click
                SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
                String repname = shared.getString("user_name", "");
                SharedPreferences ordershared = getSharedPreferences("Order", Context.MODE_PRIVATE);
                String order_id = ordershared.getString("order_id", "");
                String cust_nu = ordershared.getString("cust_number", "");
                String cust_na = ordershared.getString("cust_name", "");
                String cust_addr = ordershared.getString("cust_addr", "");


                String full_shop_name = cust_nu + " " + cust_na;
                String full_addr = cust_addr;
                String full_repname = "Sales Person: " + repname;
                String seorderid = order_id;
                String secustname = full_shop_name;
                String secustaddr = cust_addr;
                String sesalesrep = repname;
                String block = "a1";

                SAVEQUOTE(seorderid, secustname, secustaddr, sesalesrep,block);
                break;
            case R.id.a2:
                // handle button3 click
                // handle button1 click
                shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
                repname = shared.getString("user_name", "");
                 ordershared = getSharedPreferences("Order", Context.MODE_PRIVATE);
                order_id = ordershared.getString("order_id", "");
                cust_nu = ordershared.getString("cust_number", "");
                 cust_na = ordershared.getString("cust_name", "");
                cust_addr = ordershared.getString("cust_addr", "");


                full_shop_name = cust_nu + " " + cust_na;
                full_addr = cust_addr;
                 full_repname = "Sales Person: " + repname;
                 seorderid = order_id;
                secustname = full_shop_name;
                 secustaddr = cust_addr;
                 sesalesrep = repname;
                 block = "a2";

                SAVEQUOTE(seorderid, secustname, secustaddr, sesalesrep,block);
                break;
            case R.id.a3:
                // handle button3 click
                shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
                repname = shared.getString("user_name", "");
                ordershared = getSharedPreferences("Order", Context.MODE_PRIVATE);
                order_id = ordershared.getString("order_id", "");
                cust_nu = ordershared.getString("cust_number", "");
                cust_na = ordershared.getString("cust_name", "");
                cust_addr = ordershared.getString("cust_addr", "");


                full_shop_name = cust_nu + " " + cust_na;
                full_addr = cust_addr;
                full_repname = "Sales Person: " + repname;
                seorderid = order_id;
                secustname = full_shop_name;
                secustaddr = cust_addr;
                sesalesrep = repname;
                block = "a3";

                SAVEQUOTE(seorderid, secustname, secustaddr, sesalesrep,block);
                break;

            case R.id.a4:
                // handle button3 click
                shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
                repname = shared.getString("user_name", "");
                ordershared = getSharedPreferences("Order", Context.MODE_PRIVATE);
                order_id = ordershared.getString("order_id", "");
                cust_nu = ordershared.getString("cust_number", "");
                cust_na = ordershared.getString("cust_name", "");
                cust_addr = ordershared.getString("cust_addr", "");


                full_shop_name = cust_nu + " " + cust_na;
                full_addr = cust_addr;
                full_repname = "Sales Person: " + repname;
                seorderid = order_id;
                secustname = full_shop_name;
                secustaddr = cust_addr;
                sesalesrep = repname;
                block = "a4";

                SAVEQUOTE(seorderid, secustname, secustaddr, sesalesrep,block);
                break;
            // etc...

            case R.id.b1:
                // handle button1 click
                shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
                repname = shared.getString("user_name", "");
                ordershared = getSharedPreferences("Order", Context.MODE_PRIVATE);
                order_id = ordershared.getString("order_id", "");
                cust_nu = ordershared.getString("cust_number", "");
                cust_na = ordershared.getString("cust_name", "");
                cust_addr = ordershared.getString("cust_addr", "");


                full_shop_name = cust_nu + " " + cust_na;
                full_addr = cust_addr;
                full_repname = "Sales Person: " + repname;
                seorderid = order_id;
                secustname = full_shop_name;
                secustaddr = cust_addr;
                sesalesrep = repname;
                block = "b1";

                SAVEQUOTE(seorderid, secustname, secustaddr, sesalesrep,block);
                break;
            case R.id.b2:
                // handle button3 click
                shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
                repname = shared.getString("user_name", "");
                ordershared = getSharedPreferences("Order", Context.MODE_PRIVATE);
                order_id = ordershared.getString("order_id", "");
                cust_nu = ordershared.getString("cust_number", "");
                cust_na = ordershared.getString("cust_name", "");
                cust_addr = ordershared.getString("cust_addr", "");


                full_shop_name = cust_nu + " " + cust_na;
                full_addr = cust_addr;
                full_repname = "Sales Person: " + repname;
                seorderid = order_id;
                secustname = full_shop_name;
                secustaddr = cust_addr;
                sesalesrep = repname;
                block = "b2";

                SAVEQUOTE(seorderid, secustname, secustaddr, sesalesrep,block);
                break;
            case R.id.b3:
                // handle button3 click
                shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
                repname = shared.getString("user_name", "");
                ordershared = getSharedPreferences("Order", Context.MODE_PRIVATE);
                order_id = ordershared.getString("order_id", "");
                cust_nu = ordershared.getString("cust_number", "");
                cust_na = ordershared.getString("cust_name", "");
                cust_addr = ordershared.getString("cust_addr", "");


                full_shop_name = cust_nu + " " + cust_na;
                full_addr = cust_addr;
                full_repname = "Sales Person: " + repname;
                seorderid = order_id;
                secustname = full_shop_name;
                secustaddr = cust_addr;
                sesalesrep = repname;
                block = "b3";

                SAVEQUOTE(seorderid, secustname, secustaddr, sesalesrep,block);
                break;

            case R.id.b4:
                // handle button3
                shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
                repname = shared.getString("user_name", "");
                ordershared = getSharedPreferences("Order", Context.MODE_PRIVATE);
                order_id = ordershared.getString("order_id", "");
                cust_nu = ordershared.getString("cust_number", "");
                cust_na = ordershared.getString("cust_name", "");
                cust_addr = ordershared.getString("cust_addr", "");


                full_shop_name = cust_nu + " " + cust_na;
                full_addr = cust_addr;
                full_repname = "Sales Person: " + repname;
                seorderid = order_id;
                secustname = full_shop_name;
                secustaddr = cust_addr;
                sesalesrep = repname;
                block = "b4";

                SAVEQUOTE(seorderid, secustname, secustaddr, sesalesrep,block);
                break;

            case R.id.c1:
                // handle button1 click
                shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
                repname = shared.getString("user_name", "");
                ordershared = getSharedPreferences("Order", Context.MODE_PRIVATE);
                order_id = ordershared.getString("order_id", "");
                cust_nu = ordershared.getString("cust_number", "");
                cust_na = ordershared.getString("cust_name", "");
                cust_addr = ordershared.getString("cust_addr", "");


                full_shop_name = cust_nu + " " + cust_na;
                full_addr = cust_addr;
                full_repname = "Sales Person: " + repname;
                seorderid = order_id;
                secustname = full_shop_name;
                secustaddr = cust_addr;
                sesalesrep = repname;
                block = "c1";

                SAVEQUOTE(seorderid, secustname, secustaddr, sesalesrep,block);
                break;
            case R.id.c2:
                // handle button3 click
                shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
                repname = shared.getString("user_name", "");
                ordershared = getSharedPreferences("Order", Context.MODE_PRIVATE);
                order_id = ordershared.getString("order_id", "");
                cust_nu = ordershared.getString("cust_number", "");
                cust_na = ordershared.getString("cust_name", "");
                cust_addr = ordershared.getString("cust_addr", "");


                full_shop_name = cust_nu + " " + cust_na;
                full_addr = cust_addr;
                full_repname = "Sales Person: " + repname;
                seorderid = order_id;
                secustname = full_shop_name;
                secustaddr = cust_addr;
                sesalesrep = repname;
                block = "c2";

                SAVEQUOTE(seorderid, secustname, secustaddr, sesalesrep,block);
                break;
            case R.id.c3:
                // handle button3 click
                shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
                repname = shared.getString("user_name", "");
                ordershared = getSharedPreferences("Order", Context.MODE_PRIVATE);
                order_id = ordershared.getString("order_id", "");
                cust_nu = ordershared.getString("cust_number", "");
                cust_na = ordershared.getString("cust_name", "");
                cust_addr = ordershared.getString("cust_addr", "");


                full_shop_name = cust_nu + " " + cust_na;
                full_addr = cust_addr;
                full_repname = "Sales Person: " + repname;
                seorderid = order_id;
                secustname = full_shop_name;
                secustaddr = cust_addr;
                sesalesrep = repname;
                block = "c3";

                SAVEQUOTE(seorderid, secustname, secustaddr, sesalesrep,block);
                break;

            case R.id.c4:
                // handle button3 click
                shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
                repname = shared.getString("user_name", "");
                ordershared = getSharedPreferences("Order", Context.MODE_PRIVATE);
                order_id = ordershared.getString("order_id", "");
                cust_nu = ordershared.getString("cust_number", "");
                cust_na = ordershared.getString("cust_name", "");
                cust_addr = ordershared.getString("cust_addr", "");


                full_shop_name = cust_nu + " " + cust_na;
                full_addr = cust_addr;
                full_repname = "Sales Person: " + repname;
                seorderid = order_id;
                secustname = full_shop_name;
                secustaddr = cust_addr;
                sesalesrep = repname;
                block = "c4";

                SAVEQUOTE(seorderid, secustname, secustaddr, sesalesrep,block);
                break;
        }
    }

    @Override
    protected void onResume() {
        ReloadItems();
        super.onResume();
    }
}
