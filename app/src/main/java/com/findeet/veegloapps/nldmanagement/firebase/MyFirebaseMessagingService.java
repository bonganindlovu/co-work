package com.findeet.veegloapps.nldmanagement.firebase;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Build;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.findeet.veegloapps.nldmanagement.EmpDash;
import com.findeet.veegloapps.nldmanagement.EndPoints;
import com.findeet.veegloapps.nldmanagement.Notifications;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;


/**
 * Created by DEBONGZ on 3/14/2018.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    static SQLiteDatabase sqLiteDatabase;
    static Cursor cursor, cursora, cursorb, cursor_attach;
    private static final String TAG = "MyFirebaseMsgService";

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.e("NEW_TOKEN",s);
    }



    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {


        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());
            try {
                String results = remoteMessage.getData().toString();

                //JSONObject json = new JSONObject(results);
                JSONObject json = new JSONObject(remoteMessage.getData());
                sendPushNotification(json);
            } catch (Exception e) {
                Log.e(TAG, "Exception Error: " + e.getMessage());
            }
        }
    }

    private static boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }

    //this method will display the notification
    //We are passing the JSONObject that is received from
    //firebase cloud messaging
    @SuppressLint("NewApi")
    private void sendPushNotification(JSONObject json) {
        //optionally we can display the json into log
        Log.e(TAG, "Notification JSON " + json.toString());
        try {
            String imageUrl="";
            //getting the json data
            JSONObject data = new JSONObject(json.toString());

            //parsing json data
            String title = data.getString("title");
            String message = data.getString("message");
            if(data.getString("image").equalsIgnoreCase("http://bootycall.singles/findit/images/ic_launcher.png")){
                imageUrl="";
            }
            else{
                imageUrl = data.getString("image");
            }

            String user = data.getString("user");
            String type = data.getString("type");
            String imageUrl2 = data.getString("image");


            //creating MyNotificationManager object
            MyNotificationManager mNotificationManager = new MyNotificationManager(getApplicationContext());

            //creating an intent for the notification
            String user_name = title;
            String user_email = "debongz091@gmail.com";
            //String user_token = "cEQzzyDKu6o:APA91bHyr_yWi-PC3yBB-p9vVKCaDllcQMA3biREAbQj0LNY-VfuaQelEqpQ1-ebXmwC_UVGTc038zocjcMVSuCZsdMLXlOx7uCwmPHTiVxs5tyVcvxYMtzuvqbJ1WMnkIUK90LNGobG";
            String img_url = imageUrl;

            ActivityManager am = (ActivityManager) this.getSystemService(ACTIVITY_SERVICE);
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            Log.d("topActivity", "CURRENT Activity ::" + taskInfo.get(0).topActivity.getClassName());
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            componentInfo.getPackageName();


            String current_act = taskInfo.get(0).topActivity.getClassName();

            if(type.equalsIgnoreCase("rep_not")) {

                SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
                int unOpenCount = shared.getInt("NOTICOUNT", +1);



                Integer tcount = unOpenCount+1;

                shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = shared.edit();
                editor.putInt("NOTICOUNT", tcount);
                editor.commit();
                BadgeUtils.setBadge(MyFirebaseMessagingService.this,tcount);


                        mNotificationManager.playNotificationSoundMain();
                Intent intent = new Intent(getApplicationContext(), Notifications.class);;
                mNotificationManager.showBigNotification(title, message, imageUrl2, intent);

            }
            else if(type.equalsIgnoreCase("due_not")) {

                SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
                int unOpenCount = shared.getInt("NOTICOUNT", +1);



                Integer tcount = unOpenCount+1;

                shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = shared.edit();
                editor.putInt("NOTICOUNT", tcount);
                editor.commit();
                BadgeUtils.setBadge(MyFirebaseMessagingService.this,tcount);


                mNotificationManager.playNotificationSoundMain();
                Intent intent = new Intent(getApplicationContext(), Notifications.class);;
                mNotificationManager.showBigNotification(title, message, imageUrl2, intent);

            }

            else if(type.equalsIgnoreCase("tasks")) {

                SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
                int unOpenCount = shared.getInt("NOTICOUNT", +1);



                Integer tcount = unOpenCount+1;

                shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = shared.edit();
                editor.putInt("NOTICOUNT", tcount);
                editor.commit();
                BadgeUtils.setBadge(MyFirebaseMessagingService.this,tcount);


                //mNotificationManager.playNotificationSoundMain();
               // Intent intent = new Intent(getApplicationContext(), EmpDash.class);;
                //mNotificationManager.showBigNotification(title, message, imageUrl2, intent);

                sendBroadcast(new Intent(EndPoints.ORDER_SAVED_BROADCAST+"tasks"));

            }
            else {
                SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
                int unOpenCount = shared.getInt("NOTICOUNT", +1);



                Integer tcount = unOpenCount+1;

                shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = shared.edit();
                editor.putInt("NOTICOUNT", tcount);
                editor.commit();
                BadgeUtils.setBadge(MyFirebaseMessagingService.this,tcount);


                mNotificationManager.playNotificationSoundMain();
                Intent intent = new Intent(getApplicationContext(), Notifications.class);;
                mNotificationManager.showBigNotification(title, message, imageUrl2, intent);
            }

//            else if (type.equalsIgnoreCase("delivery_report")){
//                Intent pushNotification = new Intent(EndPoint.MY_SYNC_BROADCAST+"reload_chats");
//                SyncServices.updateDelivery(this,user);
//                LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
//
//            }
//
//            else if (type.equalsIgnoreCase("seen_report")){
//                //mNotificationManager.playNotificationSound2();
//                SyncServices.updateSeen(this,title,user);
//            }
//            else if(type.equalsIgnoreCase("credits_push")) {
//                Intent intent = new Intent(getApplicationContext(), InfoMe.class);
//                mNotificationManager.showBigNotification(title, message, imageUrl2, intent);
//            }
//
//            else if(type.equalsIgnoreCase("web_push")) {
//                Intent intent = new Intent(Intent.ACTION_VIEW);
//                intent.setData(Uri.parse(user));
//                mNotificationManager.showBigNotification(title, message, imageUrl2, intent);
//
//            }
//            else if(type.equalsIgnoreCase("request_push")) {
//                Intent intent = new Intent(getApplicationContext(), FriendRequest.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                mNotificationManager.showBigNotification(title, message, imageUrl2, intent);
//
//            }
//            else if(type.equalsIgnoreCase("request_push")) {
//                Intent intent = new Intent(getApplicationContext(), FriendRequest.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                mNotificationManager.showBigNotification(title, message, imageUrl2, intent);
//
//            }
//
//            else if (type.equalsIgnoreCase("post_actions")) {
//
//                Intent pushNotification = new Intent("shownewnotification");
//                pushNotification.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
//                SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
//                int unOpenCount = shared.getInt("NOTICOUNT", +1);
//
//                Intent intent = new Intent(getApplicationContext(), NotificationsList.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                intent.putExtra("friend_email", user);
//                intent.putExtra("friend_name", user_name);
//                intent.putExtra("friend_image", img_url);
//                //getApplicationContext().startActivity(intent);
//
//
//                Integer tcount = unOpenCount + 1;
//
//                shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
//                SharedPreferences.Editor editor = shared.edit();
//                editor.putInt("NOTICOUNT", tcount);
//                editor.commit();
//
//                //This is for bladge on home icon
//                BadgeUtils.setBadge(MyFirebaseMessagingService.this, tcount);
//
//                SharedPreferences setshared = getSharedPreferences("FindiT_Settings", Context.MODE_PRIVATE);
//                String notification_sound = setshared.getString("notification_sound", "");
//                if (notification_sound.equalsIgnoreCase("on")) {
//                    mNotificationManager.showBigNotification(title, message, imageUrl2, intent);
//                } else {
//                }
//            }
//
//            else if(type.equalsIgnoreCase("single_push")) {
//
//                // open the chatting activity
//                Intent intent = new Intent(getApplicationContext(), IndividulasChats.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                intent.putExtra("friend_email", user);
//                intent.putExtra("friend_name", user_name);
//                intent.putExtra("friend_image", img_url);
//                //getApplicationContext().startActivity(intent);
//
//                //set the notification not to show when chat screen activity is open
//                SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
//                String val = shared.getString("to_user", "");
//                shared = getSharedPreferences("FindiT_MainPref", Context.MODE_PRIVATE);
//                String email = shared.getString("email", "");
//
//                //Toast.makeText(this, "user_email"+user, Toast.LENGTH_SHORT).show();
//                SyncServices.recieveChats(this,user,email);
//
//
//                if (current_act.equals("com.findeet.veegloapps.findeet.IndividulasChats") && val.equals(user) && !MyFirebaseMessagingService.isAppIsInBackground(getApplicationContext())) {
//
//                    Intent pushNotification = new Intent(EndPoint.MY_SYNC_BROADCAST+"reload_chats");
//                    pushNotification.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    pushNotification.putExtra("user_name", user_name);
//                    pushNotification.putExtra("user_email", user);
//                    //intent.putExtra("user_token", user_token);
//                    pushNotification.putExtra("img_url", img_url);
//                    pushNotification.putExtra("message", message);
//                    LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
//                     shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
//                    int unOpenCount = shared.getInt("NOTICOUNT", +1);
//
//
//                    Integer tcount = unOpenCount+1;
//
//                    shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
//                    SharedPreferences.Editor editor = shared.edit();
//                    editor.putInt("NOTICOUNT", tcount);
//                    editor.commit();
//                    BadgeUtils.setBadge(MyFirebaseMessagingService.this,tcount);
//                    SharedPreferences setshared =  getSharedPreferences("FindiT_Settings", Context.MODE_PRIVATE);
//                    String message_sound = setshared.getString("message_sound", "");
//                    if (message_sound.equalsIgnoreCase("on")){
//                        mNotificationManager.playNotificationSound2();
//                    }
//                    else{}
//                }
//                else {
//
//                    Intent pushNotification = new Intent(EndPoint.MY_SYNC_BROADCAST+"load_friends");
//                    pushNotification.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
//
//                    String new_tittle="";
//                    String new_message="";
//                    String new_image="";
//                    Intent intent2 = Intent.getIntent("");
//
//                    int msgCount=0;
//                    sqLiteHelper = new SQLiteHelper(getApplicationContext());
//                    sqLiteDatabase = sqLiteHelper.getWritableDatabase();
//
//                    cursor = sqLiteDatabase.rawQuery("SELECT * FROM conversation_reply WHERE (sync_status ='not_seen' AND user_email_fk != '"+email+"') ", null);
//                    if (cursor.moveToFirst()){
//                        msgCount = cursor.getCount()+1;
//
//                            cursorb = sqLiteDatabase.rawQuery("SELECT DISTINCT user_email_fk FROM conversation_reply WHERE (sync_status ='not_seen' AND user_email_fk != '"+email+"') ", null);
//                            int userCount = 1;
//                            if (cursorb.moveToFirst()){
//                                userCount= cursorb.getCount();
//
//                            }
//                            else {
//                                userCount=1;
//
//                            }
//                            cursorb.close();
//
//
//                            String db_msg = "";
//
//
//
//                        if (msgCount>1){
//                            if (userCount>1){
//                                new_message="From "+(userCount)+" users";
//                                intent2 = new Intent(getApplicationContext(), MainActivity.class);
//                            }
//                            else {
//                                new_message="From "+title;
//                                intent2 = intent;
//                            }
//
//                            new_tittle=String.valueOf(msgCount)+" new messages";
//                            //new_message= message;
//                            new_image="http://bootycall.singles/findit/images/ic_launcher.png";
//
//
//                        }
//                        else{
//                            new_tittle=title;
//                            new_message=message;
//                            new_image=imageUrl2;
//                            intent2 = intent;
//                        }
//                    }
//                    else{
//                        new_tittle=title;
//                        new_message=message;
//                        new_image=imageUrl2;
//                        intent2 = intent;
//                    }
//                    cursor.close();
//
//
//                    mNotificationManager.showBigNotification(new_tittle, new_message, new_image, intent2);
//
//                    shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
//                    int unOpenCount = shared.getInt("NOTICOUNT", +1);
//
//
//                    Integer tcount = unOpenCount+1;
//
//                    shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
//                    SharedPreferences.Editor editor = shared.edit();
//                    editor.putInt("NOTICOUNT", tcount);
//                    editor.commit();
//
//                    //This is for bladge on home icon
//                    BadgeUtils.setBadge(MyFirebaseMessagingService.this,tcount);
//
//
//                }
//            }

        } catch (JSONException e) {
            Log.e(TAG, "Json Exception: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }

}
