package com.findeet.veegloapps.nldmanagement;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.findeet.veegloapps.nldmanagement.adapters.AdapterFullUserTask;
import com.findeet.veegloapps.nldmanagement.adapters.AdapterUserTask;
import com.findeet.veegloapps.nldmanagement.datas.DataUserTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class FullUserTasks extends AppCompatActivity {
    Toolbar toolbar_task;
    TextView not_found;
    ProgressDialog dialog;
    private AdapterFullUserTask adapter;
    SwipeRefreshLayout refresh;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_user_tasks);
        toolbar_task = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar_task);
        setSupportActionBar(toolbar_task);
        ActionBar mybar = getSupportActionBar();

        Intent intent = getIntent();
        String user_name = intent.getStringExtra("user_name");
        mybar.setTitle(user_name+"'s tasks");
        dialog = new ProgressDialog(FullUserTasks.this);
        refresh = (SwipeRefreshLayout)findViewById(R.id.swipeRefreshLayout) ;
        fetchTasks();

        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchTasks();
            }
        });
    }
    private void fetchTasks() {
        refresh.setRefreshing(true);
        RequestQueue requestQueuemom = Volley.newRequestQueue(this);
        Intent intent = getIntent();
        String user_name = intent.getStringExtra("user_name");
        StringRequest stringRequestabc = new StringRequest(Request.Method.GET, EndPoints.URL_FETCH_USER_TASKS+"?user_name="+user_name,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String result) {
                        try {

                            //Toast.makeText(Notifications.this, ""+result, Toast.LENGTH_SHORT).show();

                            JSONArray jArray = new JSONArray(result);

                            List<DataUserTask> data = new ArrayList<>();

                            for (int i = 0; i < jArray.length(); i++) {
                                JSONObject d = jArray.getJSONObject(i);
                                JSONObject json_data = jArray.getJSONObject(i);
                                DataUserTask proData = new DataUserTask();
                                proData.task_id = json_data.getString("task_id");
                                proData.task_desc = json_data.getString("task_desc");
                                proData.task_start_time = json_data.getString("task_start_time");
                                proData.task_end_time = json_data.getString("task_end_time");
                                proData.task_user_name = json_data.getString("task_user_name");

                                data.add(proData);

                            }
                            refresh.setRefreshing(false);

//

                            RecyclerView myRecycler = (RecyclerView) findViewById(R.id.taskRecycle);
                            myRecycler.setLayoutManager(new LinearLayoutManager(FullUserTasks.this));
                            adapter = new AdapterFullUserTask(FullUserTasks.this, data);
                            myRecycler.setAdapter(adapter);
                            adapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            Toast.makeText(FullUserTasks.this, ""+e, Toast.LENGTH_SHORT).show();
                            refresh.setRefreshing(false);
                            e.printStackTrace();
                        }

                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //myrefresh.setRefreshing(false);
                        refresh.setRefreshing(false);
                        Toast.makeText(FullUserTasks.this, "Loading failed", Toast.LENGTH_SHORT).show();
                    }
                }) {

        };

        requestQueuemom.add(stringRequestabc);
    }
}
