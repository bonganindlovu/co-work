package com.findeet.veegloapps.nldmanagement;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.os.Handler;
import android.support.annotation.IdRes;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.findeet.veegloapps.nldmanagement.Utils.SQLiteHelper;
import com.findeet.veegloapps.nldmanagement.fragments.ChatsFragment;
import com.findeet.veegloapps.nldmanagement.fragments.ContactsFragment;
import com.findeet.veegloapps.nldmanagement.fragments.GroupFragment;
import com.findeet.veegloapps.nldmanagement.picker.PickQuotes;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private NavigationView navigationView;
    private DrawerLayout drawer;
    private View navHeader;
    private ImageView imgNavHeaderBg, imgProfile;
    private TextView txtName, txtWebsite;
    private Toolbar toolbar;
    private FloatingActionButton fab;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    TextView menu_not;
    SQLiteDatabase sqLiteDatabase;

    // urls to load navigation header background image
    // and profile image
    private static final String urlNavHeaderBg = "https://api.androidhive.info/images/nav-menu-header-bg.jpg";
    private static final String urlProfileImg = "https://lh3.googleusercontent.com/eCtE_G34M9ygdkmOpYvCag1vBARCmZwnVS6rS5t4JLzJ6QgQSBquM0nuTsCpLhYbKljoyS-txg";


    // index to identify current nav menu item
    public static int navItemIndex = 0;

    // tags used to attach the fragments
    private static final String TAG_HOME = "home";
    private static final String TAG_PHOTOS = "photos";
    private static final String TAG_MOVIES = "movies";
    private static final String TAG_NOTIFICATIONS = "notifications";
    private static final String TAG_SETTINGS = "settings";
    public static String CURRENT_TAG = TAG_HOME;
    public  static final int RequestPermissionCode  = 1 ;

    // toolbar titles respected to selected nav menu item
    private String[] activityTitles;

    // flag to load home fragment when user presses back key
    private boolean shouldLoadHomeFragOnBackPress = true;
    private Handler mHandler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        EnableRuntimePermission();
        mHandler = new Handler();
        SQLiteDataBaseBuild();
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        // Navigation view header
        navHeader = navigationView.getHeaderView(0);
        txtName = (TextView) navHeader.findViewById(R.id.name);
        txtWebsite = (TextView) navHeader.findViewById(R.id.website);
        imgNavHeaderBg = (ImageView) navHeader.findViewById(R.id.img_header_bg);
        imgProfile = (ImageView) navHeader.findViewById(R.id.img_profile);



//        menu_not = (TextView) MenuItemCompat.getActionView(navigationView.getMenu().
//                findItem(R.id.nav_notifications));

        menu_not = (TextView) MenuItemCompat.getActionView(navigationView.getMenu().findItem(R.id.nav_notifications));

        //initializeCountDrawer();
        // load toolbar titles from string resources
        activityTitles = getResources().getStringArray(R.array.nav_item_activity_titles);

//        Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                .setAction("Action", null).show();

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        loadNavHeader();

        // initializing navigation menu
        setUpNavigationView();

        //setMenuCounter(R.id.nav_notifications,10);
        //initializeCountDrawer(10);

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new ChatsFragment(), "CHATS");
        adapter.addFragment(new GroupFragment(), "GROUPS");
        adapter.addFragment(new ContactsFragment(), "CONTACTS");
        viewPager.setAdapter(adapter);
    }
    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }
        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    private void loadNavHeader() {
        SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
        final String fnamename = shared.getString("user_name", "");
        final String email = shared.getString("user_email", "");
        // name, website
        txtName.setText(fnamename);
        txtWebsite.setText(email);
//
//        // Loading profile image
//        Glide.with(this).load(urlProfileImg)
//                .thumbnail(0.5f)
//                .into(imgProfile);
    }

    private void loadHomeFragment() {
        // selecting appropriate nav menu item
        selectNavMenu();

        // set toolbar title
        setToolbarTitle();

        // if user select the current navigation menu again, don't do anything
        // just close the navigation drawer
        if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
            drawer.closeDrawers();

            // show or hide the fab button
            toggleFab();
            return;
        }

        // Sometimes, when fragment has huge data, screen seems hanging
        // when switching between navigation menus
        // So using runnable, the fragment is loaded with cross fade effect
        // This effect can be seen in GMail app

        // show or hide the fab button
        toggleFab();

        //Closing drawer on item click
        drawer.closeDrawers();

        // refresh toolbar menu
        invalidateOptionsMenu();
    }

    private Fragment getHomeFragment() {
        switch (navItemIndex) {
            case 0:
                // home
            case 1:
                // photos
        }
        return null;
    }

    private void setToolbarTitle() {
        getSupportActionBar().setTitle(activityTitles[navItemIndex]);
    }

    private void selectNavMenu() {
        navigationView.getMenu().getItem(navItemIndex).setChecked(true);
    }

    private void setUpNavigationView() {
        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                //Check to see which item was being clicked and perform appropriate action
                switch (menuItem.getItemId()) {
                    //Replacing the main content with ContentFragment Which is our Inbox View;
                    case R.id.nav_dispatch:
                        drawer.closeDrawers();

                        SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
                        final String fnamename = shared.getString("user_name", "");
                        if (fnamename.equalsIgnoreCase("james") || fnamename.equalsIgnoreCase("debongz09") || fnamename.equalsIgnoreCase("bianca")){
                            startActivity(new Intent(MainActivity.this, Dispatch.class));
                        }
                        else{
                            Toast.makeText(MainActivity.this,"You don't have dispatch permission!", Toast.LENGTH_LONG).show();
                        }

                        break;
                    case R.id.nav_deliver:
                        drawer.closeDrawers();

                            startActivity(new Intent(MainActivity.this, Delivery.class));

                        break;
                    case R.id.nav_quotes:
                        drawer.closeDrawers();
                        startActivity(new Intent(MainActivity.this, PickQuotes.class));

                        break;
                    case R.id.nav_notifications:
                        startActivity(new Intent(MainActivity.this, Notifications.class));
                        drawer.closeDrawers();
                        break;
                    case R.id.nav_settings:
                        navItemIndex = 4;
                        CURRENT_TAG = TAG_SETTINGS;
                        break;
                    case R.id.nav_routes:
                        drawer.closeDrawers();
                        startActivity(new Intent(MainActivity.this, Routes.class));
                        break;
                        //nav_routes
                    case R.id.nav_orders:
                        drawer.closeDrawers();
                        startActivity(new Intent(MainActivity.this, Orders.class));
                        return true;
                    case R.id.nav_task:
                        drawer.closeDrawers();
                        startActivity(new Intent(MainActivity.this, Tasks.class));
                        return true;
                    case R.id.nav_activity:
                        drawer.closeDrawers();
                        SharedPreferences shared5 = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
                        final String fnamename5 = shared5.getString("user_name", "");
                        if ( fnamename5.equalsIgnoreCase("debongz09") || fnamename5.equalsIgnoreCase("alex")){
                            startActivity(new Intent(MainActivity.this, EmpDash.class));
                        }
                        else
                            {
                            Toast.makeText(MainActivity.this,"You don't have this permission!", Toast.LENGTH_LONG).show();
                            }

                        return true;
                    case R.id.nav_add_barcode:
                        SharedPreferences shared2 = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
                        String fnamename2 = shared2.getString("user_name", "");
                        if (fnamename2.equalsIgnoreCase("jamesrep") || fnamename2.equalsIgnoreCase("debongz091")|| fnamename2.equalsIgnoreCase("percy")){
                            drawer.closeDrawers();
                            startActivity(new Intent(MainActivity.this, CustomerList.class));
                        }
                        else {
                            drawer.closeDrawers();
                            startActivity(new Intent(MainActivity.this, AddBarCode.class));
                        }
                        return true;
                    case R.id.nav_logout:
                        drawer.closeDrawers();
                        SharedPreferences settings = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
                        settings.edit().clear().apply();

                        SharedPreferences settingsa = getSharedPreferences("SetupPref", Context.MODE_PRIVATE);
                        settingsa.edit().clear().apply();

                        Toast.makeText(MainActivity.this, "Loged out", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(MainActivity.this, Login.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        return true;
                    default:
                        navItemIndex = 0;
                }

                //Checking if the item is in checked state or not, if not make it in checked state
                if (menuItem.isChecked()) {
                    menuItem.setChecked(false);
                } else {
                    menuItem.setChecked(true);
                }
                menuItem.setChecked(true);
                return true;
            }
        });


        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawer.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
            return;
        }
        super.onBackPressed();
    }
    // show or hide the fab
    private void toggleFab() {
        if (navItemIndex == 0)
            fab.show();
        else
            fab.hide();
    }
    public void EnableRuntimePermission(){

        if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION))
        {

            Toast.makeText(MainActivity.this,"ACCESS_FINE_LOCATION permission allows us to Access GPS in app", Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(MainActivity.this,new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION}, RequestPermissionCode);

        }
    }
    @Override
    public void onRequestPermissionsResult(int RC, String per[], int[] PResult) {

        switch (RC) {

            case RequestPermissionCode:

                if (PResult.length > 0 && PResult[0] == PackageManager.PERMISSION_GRANTED) {

                    //Toast.makeText(getActivity(),"Permission Granted.", Toast.LENGTH_LONG).show();

                } else {

                    Toast.makeText(MainActivity.this,"Permission Canceled.", Toast.LENGTH_LONG).show();

                }
                break;
        }
    }
    public void SQLiteDataBaseBuild() {
        sqLiteDatabase = openOrCreateDatabase(SQLiteHelper.DATABASE_NAME, Context.MODE_PRIVATE, null);
        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS sales_order_items (order_id VARCHAR(200),item_number VARCHAR(50), item_name VARCHAR(100), item_unit VARCHAR(10), item_quantity INT(10),item_total VARCHAR(50), sync_state VARCHAR(50))");
        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS sales_order (order_id VARCHAR(200),cust_name TEXT, cust_addr TEXT, time_sent INT(100), date_of_sale VARCHAR(100),order_status VARCHAR(50), sales_rep VARCHAR(100),order_total DOUBLE(13,2),note TEXT,quote_number TEXT, invoice_number TEXT , pulled_by TEXT , sync_state VARCHAR(50), return_order VARCHAR(50))");
        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS routes_notes (order_id VARCHAR(200),order_note TEXT)");
        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS loads (load_id VARCHAR(200),vehicle_name TEXT,drive_name TEXT,load_time TEXT,load_status TEXT,synce_status TEXT)");
        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS loads_shops (id INTEGER PRIMARY KEY AUTOINCREMENT,load_id VARCHAR(200),shop_id TEXT,shop_name TEXT,invoice_number TEXT,invoice_amount DOUBLE(13,2),sync_status TEXT)");
        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS routes (date_created VARCHAR(50),route_name VARCHAR(50), avr_days DOUBLE(13,1), sync_state VARCHAR(50))");
        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS routes_shops (id INT(200),route_name VARCHAR(100),cust_number VARCHAR(100),cust_name VARCHAR(100), last_date_seen VARCHAR(100), last_order_size VARCHAR(100), last_person_seen VARCHAR(100),last_message TEXT, sync_state VARCHAR(50),lat_long TEXT)");
        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS driver_shops (id INT(20), list_order TEXT, load_id VARCHAR(200),shop_id TEXT,shop_name TEXT,invoice_number TEXT,invoice_amount DOUBLE(13,2),payment_status TEXT, amount_paid DOUBLE(13,2), arival_time INT(200), left_time INT(200),shop_adress TEXT,shop_lat TEXT, shop_long TEXT,sync_status TEXT)");
    }
    public void PlaySent() {
        AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        float vol = (float) 0.5; //This will be half of the default system sound
        am.playSoundEffect(AudioManager.FX_KEY_CLICK, vol);
    }
    private void initializeCountDrawer( int unOpenCount) {    //Gravity property aligns the text

//        SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
//        int unOpenCount = shared.getInt("NOTICOUNT", +1);


        menu_not.setGravity(Gravity.CENTER_VERTICAL);
        menu_not.setTypeface(null, Typeface.BOLD);
        menu_not.setTextColor(getResources().getColor(R.color.colorAccent));
        if(unOpenCount<1){

        }
        else{
            menu_not.setText(unOpenCount);
        }

    }
    private void setMenuCounter(@IdRes int itemId, int count) {
        menu_not.setText(count > 0 ? String.valueOf(count) : null);

    }

}
