package com.findeet.veegloapps.nldmanagement;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.findeet.veegloapps.nldmanagement.Utils.SQLiteHelper;
import com.findeet.veegloapps.nldmanagement.Utils.SQLiteHelperb;
import com.findeet.veegloapps.nldmanagement.adapters.AdapterDispatchLoads;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

public class AddBarCode extends AppCompatActivity implements View.OnClickListener {
    // use a compound button so either checkbox or switch widgets work.
    private CompoundButton autoFocus;
    private CompoundButton useFlash;
    private TextView statusMessage;
    private TextView barcodeValue;
    private Button scanBtn;
    private static final int MY_CAMERA_REQUEST_CODE = 100;

    SQLiteDatabase sqLiteDatabase;
    SQLiteHelperb sqLiteHelperb;
    Cursor cursor;

    private static final int RC_BARCODE_CAPTURE = 9001;
    private static final String TAG = "BarcodeMain";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_bar_code);
        SQLiteDataBaseBuild();
        sqLiteHelperb = new SQLiteHelperb(this);

        statusMessage = (TextView)findViewById(R.id.status_message);
        barcodeValue = (TextView)findViewById(R.id.barcode_value);

        autoFocus = (CompoundButton) findViewById(R.id.auto_focus);
        useFlash = (CompoundButton) findViewById(R.id.use_flash);
        findViewById(R.id.scan_barcode).setOnClickListener(this);

        findViewById(R.id.read_barcode).setOnClickListener(this);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA},
                        MY_CAMERA_REQUEST_CODE);
            }
        }
    }
    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.read_barcode) {
            // launch barcode activity.
            Intent intent = new Intent(this, BarcodeCaptureActivity.class);
            intent.putExtra(BarcodeCaptureActivity.AutoFocus, autoFocus.isChecked());
            intent.putExtra(BarcodeCaptureActivity.UseFlash, useFlash.isChecked());

            startActivityForResult(intent, RC_BARCODE_CAPTURE);
        }
        else if (v.getId() == R.id.scan_barcode) {
            IntentIntegrator scanIntegrator = new IntentIntegrator(this);
            scanIntegrator.initiateScan();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == MY_CAMERA_REQUEST_CODE) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                Toast.makeText(this, "camera permission granted", Toast.LENGTH_LONG).show();

            } else {

                Toast.makeText(this, "camera permission denied", Toast.LENGTH_LONG).show();

            }

        }}//en

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode == RC_BARCODE_CAPTURE) {
//            if (resultCode == CommonStatusCodes.SUCCESS) {
//                if (data != null) {
//                    Barcode barcode = data.getParcelableExtra(BarcodeCaptureActivity.BarcodeObject);
//                    Toast.makeText(AddBarCode.this, R.string.barcode_success+"No:"+barcode.displayValue, Toast.LENGTH_SHORT).show();
//                    //statusMessage.setText(R.string.barcode_success);
//                    //seach for database item that matches the barcode
//                    //if none select product that it belongs to if found
//                    //set the results to the product name
//                    String bar = barcode.displayValue;
//                    LookForBarCode(bar);
//                    Log.d(TAG, "Barcode read: " + barcode.displayValue);
//                } else {
//                    Toast.makeText(AddBarCode.this, R.string.barcode_failure, Toast.LENGTH_SHORT).show();
//                    //statusMessage.setText(R.string.barcode_failure);
//                    Log.d(TAG, "No barcode captured, intent data is null");
//                }
//            } else {
//                statusMessage.setText(String.format(getString(R.string.barcode_error),
//                        CommonStatusCodes.getStatusCodeString(resultCode)));
//            }
//        }
//        else {
//            super.onActivityResult(requestCode, resultCode, data);
//        }
//    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
//retrieve scan result
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanningResult != null) {

//we have a result
            String scanContent = scanningResult.getContents();
            LookForBarCode(scanContent);
        }
        else{
            Toast toast = Toast.makeText(getApplicationContext(),
                    "No scan data received!", Toast.LENGTH_SHORT);
            toast.show();
        }

    }

    public void SQLiteDataBaseBuild(){

        sqLiteDatabase = openOrCreateDatabase(SQLiteHelper.DATABASE_NAME, Context.MODE_PRIVATE, null);

    }

    public void LookForBarCode(final String barcode_txt){
        sqLiteDatabase = sqLiteHelperb.getWritableDatabase();

        cursor = sqLiteDatabase.rawQuery("SELECT * FROM products WHERE barcode ='"+barcode_txt+"' ", null);

        if (cursor.moveToFirst()) {
            //set that the product is found
            String product_name = cursor.getString(cursor.getColumnIndex("pname"));
            statusMessage.setText(R.string.barcode_success+"\n"+product_name+"\n"+barcode_txt);
            Toast.makeText(AddBarCode.this, ""+product_name, Toast.LENGTH_SHORT).show();
        }
        else{
           //update sqlite, and mtsql
          //call the list of the products to chose from
            Toast.makeText(AddBarCode.this, "barcode "+barcode_txt+" was not found", Toast.LENGTH_SHORT).show();
            SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = shared.edit();
            editor.putString("thebarcode", barcode_txt);
            editor.commit();
            Intent intent = new Intent(AddBarCode.this, ChoseBarItem.class);
            startActivity(intent);
        }


        cursor.close();
    }

    public void goback(View view) {
        super.onBackPressed();
    }
}
