package com.findeet.veegloapps.nldmanagement.adapters;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.findeet.veegloapps.nldmanagement.DispatchLoads;
import com.findeet.veegloapps.nldmanagement.R;
import com.findeet.veegloapps.nldmanagement.Utils.SQLiteHelper;
import com.findeet.veegloapps.nldmanagement.Utils.SQLiteHelperb;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by DEBONGZ on 1/6/2018.
 */

public class AdapterDispatchLoads extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    // id_Array,load_id_Array,shop_id_Array,shop_name_Array,invoice_number_Array,invoice_amount_Array,sync_state_Array;

    ArrayList<String> pid;
    ArrayList<String> pload_id;
    ArrayList<String> pshop_id;
    ArrayList<String> pshop_name;
    ArrayList<String> pinvoice_number;
    ArrayList<String> pinvoice_amount;
    ArrayList<String> psync_state;


    Cursor cursor;

    private Context context;
    private LayoutInflater inflater;
//    List<DataProducts> data= Collections.emptyList();
//    DataProducts current;
    int currentPos=0;



    public AdapterDispatchLoads(Context context,
                                ArrayList<String> id,
                                ArrayList<String> load_id,
                                ArrayList<String> shop_id,
                                ArrayList<String> shop_name,
                                ArrayList<String> invoice_number,
                                ArrayList<String> invoice_amount,
                                ArrayList<String> sync_state){
        this.context=context;
        inflater= LayoutInflater.from(context);
        //this.data=data;

        this.pid = id;
        this.pload_id = load_id;
        this.pshop_id = shop_id;
        this.pshop_name = shop_name;
        this.pinvoice_number = invoice_number;
        this.pinvoice_amount = invoice_amount;
        this.psync_state = sync_state;


    }



    // Inflate the layout when ViewHolder created
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=inflater.inflate(R.layout.container_quotes, parent,false);
        MyHolder holder=new MyHolder(view);

        return holder;
    }

    // Bind data
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        // Get current position of item in RecyclerView to bind data and assign values from list
        MyHolder myHolder= (MyHolder) holder;


        myHolder.txtQcustName.setText(pshop_id.get(position)+" "+pshop_name.get(position));
        myHolder.txtQorderTotal.setText("R"+pinvoice_amount.get(position));
        myHolder.txtQOrderDate.setText(pinvoice_number.get(position));
        myHolder.txtQOrderNum.setText(pid.get(position));
        myHolder.txtQCustAddr.setText("");
        String synce_status = psync_state.get(position);

       // Toast.makeText(context, ""+pshop_name.get(position)+" "+synce_status, Toast.LENGTH_SHORT).show();
        if (synce_status.equalsIgnoreCase("not_synced")){
            myHolder.Rsent_badge.setVisibility(View.GONE);
        }
        else{
            myHolder.Rsent_badge.setVisibility(View.VISIBLE);
        }



    }
    // return total item from List
    @Override
    public int getItemCount() {
        return pload_id.size();
    }



    class MyHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView txtQcustName;
        TextView txtQorderTotal;
        TextView txtQOrderDate;
        TextView txtQOrderNum;
        TextView txtQCustAddr;
        RelativeLayout Porder_sync_prog;
        RelativeLayout Rsent_badge;

        // create constructor to get widget reference
        public MyHolder(View itemViewa) {
            super(itemViewa);
            txtQcustName = (TextView) itemViewa.findViewById(R.id.txtqname);
            txtQorderTotal = (TextView) itemViewa.findViewById(R.id.txtqtotal);
            txtQOrderDate = (TextView) itemViewa.findViewById(R.id.txtqdate);
            txtQOrderNum = (TextView) itemViewa.findViewById(R.id.txtqid);
            txtQCustAddr = (TextView) itemViewa.findViewById(R.id.txtqcustAddr);
            Porder_sync_prog =(RelativeLayout)itemViewa.findViewById(R.id.order_sync_status);
            Rsent_badge =(RelativeLayout) itemViewa.findViewById(R.id.sent_badge);
            itemViewa.setOnClickListener(this);
        }



        // Click event for all items
        @Override
        public void onClick(View v) {

//
//            String load_id = txtQOrderNum.getText().toString();
//            String vehicle_name = txtQcustName.getText().toString();
//
//            Intent intent= new Intent(context, DispatchLoads.class);
//            intent.putExtra("load_id", load_id);
//            intent.putExtra("vehicle_name", vehicle_name);
//            context.startActivity(intent);


        }
    }

}
