package com.findeet.veegloapps.nldmanagement;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.findeet.veegloapps.nldmanagement.Utils.VolleySingleton;
import com.findeet.veegloapps.nldmanagement.adapters.AdapterNot;
import com.findeet.veegloapps.nldmanagement.adapters.AdapterUserTask;
import com.findeet.veegloapps.nldmanagement.datas.DataNot;
import com.findeet.veegloapps.nldmanagement.datas.DataUserTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Tasks extends AppCompatActivity {
    Toolbar toolbar_task;
    TextView not_found;
    ProgressDialog dialog;
    private AdapterUserTask adapter;
    SwipeRefreshLayout refresh;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tasks);
        not_found = (TextView)findViewById(R.id.not_found);
        toolbar_task = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar_task);
        setSupportActionBar(toolbar_task);
        ActionBar mybar = getSupportActionBar();
        mybar.setTitle("My Tasks");
        dialog = new ProgressDialog(Tasks.this);
        refresh = (SwipeRefreshLayout)findViewById(R.id.swipeRefreshLayout) ;
        fetchTasks();

        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchTasks();
            }
        });

    }

    public void addNewTask(View view) {
        SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
        final String repname = shared.getString("user_name", "");


            LayoutInflater layoutInflaterAndroid = LayoutInflater.from(Tasks.this);
            View mView = layoutInflaterAndroid.inflate(R.layout.add_task_dialog_box, null);
            final AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(Tasks.this);
            alertDialogBuilderUserInput.setView(mView);
            TextView clock_in,clock_out,paper_work,checks,receiving,pulling,dispatching,cleaning,lunch,general;

            clock_in = (TextView)mView.findViewById(R.id.clock_in);
            clock_out = (TextView)mView.findViewById(R.id.clock_out);
            paper_work = (TextView)mView.findViewById(R.id.paper_work);
            checks = (TextView)mView.findViewById(R.id.checks);
            receiving = (TextView)mView.findViewById(R.id.receiving);
            pulling = (TextView)mView.findViewById(R.id.pulling);
            dispatching = (TextView)mView.findViewById(R.id.dispatching);
            cleaning = (TextView)mView.findViewById(R.id.cleaning);
            lunch = (TextView)mView.findViewById(R.id.lunch);
            general = (TextView)mView.findViewById(R.id.general);

            clock_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //sendTask(repname,"Clocked In");

                Intent intent= new Intent(Tasks.this, ClockSign.class);
                intent.putExtra("user_name", repname);
                intent.putExtra("action", "Clocked In");
                startActivity(intent);

            }
            });
            clock_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //sendTask(repname,"Clocked Out");

                Intent intent= new Intent(Tasks.this, ClockSign.class);
                intent.putExtra("user_name", repname);
                intent.putExtra("action", "Clocked Out");
                startActivity(intent);

            }
           });
           paper_work.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendTask(repname,"Doing Paper Work");

            }
        });
           checks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendTask(repname,"Doing Vehicle Checks / Maintenance");

            }
        });
          receiving.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendTask(repname,"Receiving Stock");

            }
        });
        pulling.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendTask(repname,"Pulling Orders");

            }
        });
        dispatching.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendTask(repname,"Dispatching Orders");

            }
        });
        cleaning.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendTask(repname,"Cleaning / Washing");

            }
        });
        lunch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //sendTask(repname,"On Lunch");

                Intent intent= new Intent(Tasks.this, ClockSign.class);
                intent.putExtra("user_name", repname);
                intent.putExtra("action", "On Lunch");
                startActivity(intent);


            }
        });
        general.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendTask(repname,"Doing General Tasks");


            }
        });


        alertDialogBuilderUserInput
                    .setCancelable(true);

            AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
            alertDialogAndroid.show();



    }
    public void sendTask(final String user_name, final String task_name) {
        dialog.setMessage("Adding task, Please wait...");
        dialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, EndPoints.SEND_TASKS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();

                            finish();
                            startActivity(new Intent(Tasks.this, Tasks.class));
                            Toast.makeText(Tasks.this, "Task added", Toast.LENGTH_LONG).show();
                            fetchTasks();


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        finish();
                        startActivity(new Intent(Tasks.this, Tasks.class));
                        Toast.makeText(Tasks.this, ""+error, Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("user_name",user_name);
                params.put("task_name",task_name);
                return params;
                //route_name, cust_number, last_person_seen, last_message
            }
        };

        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }
    private void fetchTasks() {
        refresh.setRefreshing(true);
        RequestQueue requestQueuemom = Volley.newRequestQueue(this);
        SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
        final String fnamename = shared.getString("user_name", "");
        StringRequest stringRequestabc = new StringRequest(Request.Method.GET, EndPoints.URL_FETCH_USER_TASKS+"?user_name="+fnamename,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String result) {
                        try {

                            //Toast.makeText(Notifications.this, ""+result, Toast.LENGTH_SHORT).show();

                            JSONArray jArray = new JSONArray(result);

                            List<DataUserTask> data = new ArrayList<>();

                            for (int i = 0; i < jArray.length(); i++) {
                                JSONObject d = jArray.getJSONObject(i);
                                JSONObject json_data = jArray.getJSONObject(i);
                                DataUserTask proData = new DataUserTask();
                                proData.task_id = json_data.getString("task_id");
                                proData.task_desc = json_data.getString("task_desc");
                                proData.task_start_time = json_data.getString("task_start_time");
                                proData.task_end_time = json_data.getString("task_end_time");
                                proData.task_user_name = json_data.getString("task_user_name");

                                data.add(proData);

                            }
                            refresh.setRefreshing(false);

//

                            RecyclerView myRecycler = (RecyclerView) findViewById(R.id.taskRecycle);
                            myRecycler.setLayoutManager(new LinearLayoutManager(Tasks.this));
                            adapter = new AdapterUserTask(Tasks.this, data);
                            myRecycler.setAdapter(adapter);
                            adapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            Toast.makeText(Tasks.this, ""+e, Toast.LENGTH_SHORT).show();
                            refresh.setRefreshing(false);
                            e.printStackTrace();
                        }

                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //myrefresh.setRefreshing(false);
                        refresh.setRefreshing(false);
                        Toast.makeText(Tasks.this, "Loading failed", Toast.LENGTH_SHORT).show();
                    }
                }) {

        };

        requestQueuemom.add(stringRequestabc);
    }

}
