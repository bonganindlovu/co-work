package com.findeet.veegloapps.nldmanagement;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.findeet.veegloapps.nldmanagement.Utils.SQLiteHelper;

public class Note extends AppCompatActivity {
    EditText my_note;
    private Context context =this;
    SQLiteDatabase sqLiteDatabase;
    SQLiteHelper sqLiteHelper;
    Cursor cursor,cursor2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);
        SQLiteDataBaseBuild();
        Toolbar mytoolbar = (Toolbar) findViewById(R.id.toolbaritem);
        setSupportActionBar(mytoolbar);
        getSupportActionBar().setTitle("ADD NOTE");
        sqLiteHelper = new SQLiteHelper(context);
        my_note =(EditText) findViewById(R.id.notetxt);
        Intent in = getIntent();
        String seorderid = in.getExtras().getString("order_id");
        showNote(seorderid);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_note, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.delete_note:
                Intent in = getIntent();
                String seorderid = in.getExtras().getString("order_id");
                Toast.makeText(Note.this, "Note Deleted", Toast.LENGTH_SHORT).show();
                DELETEITEM(seorderid);
                finish();

                break;
            case R.id.save_note:
                in = getIntent();
                seorderid = in.getExtras().getString("order_id");
                String note_text= my_note.getText().toString();

                if (note_text.isEmpty()) {
                    Toast.makeText(Note.this, "Note can not be empty", Toast.LENGTH_SHORT).show();

                } else {
                    in = getIntent();
                    seorderid = in.getExtras().getString("order_id");
                    String seorder_note = note_text;


                    checkNote(seorderid, seorder_note);
                    Toast.makeText(Note.this, "Note Saved", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                break;
        }
        return true;
    }



    public void SQLiteDataBaseBuild() {
        sqLiteDatabase = openOrCreateDatabase(SQLiteHelper.DATABASE_NAME, Context.MODE_PRIVATE, null);
    }

    public void checkNote(final  String seorderid, final  String  seorder_note){
        sqLiteDatabase = sqLiteHelper.getWritableDatabase();
        cursor = sqLiteDatabase.rawQuery("SELECT * FROM routes_notes WHERE order_id = '"+seorderid+"'  ", null);

        int count = 0;
        if (cursor.moveToFirst()) {
            String thenote =seorder_note.replace("'","");
            sqLiteDatabase.execSQL("UPDATE routes_notes SET order_note ='"+thenote+"' WHERE order_id ='"+seorderid+"' ");
            finish();

        }
        else{
            String thenote =seorder_note.replace("'","");
            String SQLiteDataBaseQueryHolder = "INSERT INTO routes_notes (order_id,order_note) VALUES('"+seorderid+"', '"+thenote+"');";
            sqLiteDatabase.execSQL(SQLiteDataBaseQueryHolder);
            finish();
        }

    }

    public void showNote(final  String seorderid){
        sqLiteDatabase = sqLiteHelper.getWritableDatabase();
        cursor = sqLiteDatabase.rawQuery("SELECT * FROM routes_notes WHERE order_id = '"+seorderid+"'  ", null);
        if (cursor.moveToFirst()) {
            String order_id= cursor.getString(cursor.getColumnIndex("order_id"));
            String note_text= cursor.getString(cursor.getColumnIndex("order_note"));

            if(note_text.equalsIgnoreCase("Click here to add note...") || note_text==null) {
                my_note.clearFocus();
            } else {
                my_note.setText(note_text);
            }

        }
    }

    private void DELETEITEM(String seorderid) {
        sqLiteDatabase.execSQL("DELETE FROM routes_notes WHERE order_id ='"+seorderid+"' ");

    }
}
