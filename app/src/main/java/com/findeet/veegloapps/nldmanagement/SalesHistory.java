package com.findeet.veegloapps.nldmanagement;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.findeet.veegloapps.nldmanagement.Utils.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SalesHistory extends AppCompatActivity {
    private WebView wv1;
    android.support.v7.widget.Toolbar sales_history_toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_history);
        sales_history_toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.sales_history_toolbar);
        setSupportActionBar(sales_history_toolbar);
        ActionBar mybar = getSupportActionBar();
        mybar.setTitle("Sales History");

        Intent in = getIntent();
        String cust_number= in.getExtras().getString("cust_number");

        String url ="http://nextld.co.za/salesadmin/app_invoice.php?cust_no="+cust_number;
        wv1=(WebView)findViewById(R.id.webview);
        wv1.setWebViewClient(new MyBrowser());

        wv1.getSettings().setLoadsImagesAutomatically(true);
        wv1.getSettings().setAppCacheMaxSize( 5 * 1024 * 1024 ); // 5MB
        wv1.getSettings().setAppCachePath( getApplicationContext().getCacheDir().getAbsolutePath() );
        wv1.getSettings().setAllowFileAccess( true );
        wv1.getSettings().setAppCacheEnabled( true );
        wv1.getSettings().setJavaScriptEnabled( true );
        wv1.getSettings().setCacheMode( WebSettings.LOAD_DEFAULT ); // load online by default

        if ( !isNetworkAvailable() ) { // loading offline
            wv1.getSettings().setCacheMode( WebSettings.LOAD_CACHE_ELSE_NETWORK );
        }
        wv1.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        wv1.loadUrl(url);
    }
    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.move_route, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.movea:
                Intent in = getIntent();
                String cust_number= in.getExtras().getString("cust_number");
                String seaction ="move to a";
                move(cust_number,seaction);
                break;
            case R.id.moveb:
                in = getIntent();
                cust_number= in.getExtras().getString("cust_number");
                seaction ="move to b";
                move(cust_number,seaction);
                break;
            case R.id.moveab:
                in = getIntent();
                cust_number= in.getExtras().getString("cust_number");
                seaction ="move to ab";
                move(cust_number,seaction);
                break;
            case R.id.remove:
                in = getIntent();
                cust_number= in.getExtras().getString("cust_number");
                seaction ="remove";
                move(cust_number,seaction);
                break;
            case R.id.move_to_house:
                in = getIntent();
                cust_number= in.getExtras().getString("cust_number");
                seaction ="move_to_house";
                move(cust_number,seaction);
                break;
            // move_to_house
            default:
                break;
        }
        return true;
    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService( CONNECTIVITY_SERVICE );
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    public void move(final String secust_number, final String seaction) {
        //Toast.makeText(context, "" + snote, Toast.LENGTH_SHORT).show();
        SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
        final String repname =shared.getString("user_name", "");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, EndPoints.MOVE_SHOP,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
//                            JSONArray jsonArray = jsonObject.getJSONArray("result");
//                            JSONObject jsonObject1 = jsonArray.getJSONObject(0);
//                            String status = jsonObject1.getString("status");

                            Toast.makeText(SalesHistory.this, "Submited" , Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("cust_number", secust_number);
                params.put("action", seaction);
                params.put("user_id", repname);
                return params;
            }
        };

        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }
}
