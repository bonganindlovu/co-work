package com.findeet.veegloapps.nldmanagement;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.findeet.veegloapps.nldmanagement.Utils.SQLiteHelper;

public class CustomerDetailsList extends AppCompatActivity implements View.OnClickListener {
    TextView customer_name, customer_address, customer_registration, customer_vat, contact_name,
            contact_mobile, contact_website;


    SQLiteDatabase sqLiteDatabase;
    SQLiteHelper sqLiteHelper;
    Cursor cursor, cursorb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_details_list);
        sqLiteHelper = new SQLiteHelper(this);
        SQLiteDataBaseBuild();

        customer_name = (TextView) findViewById(R.id.customer_name);
        customer_address = (TextView) findViewById(R.id.customer_address);
        customer_address.setOnClickListener(this);

        contact_name = (TextView) findViewById(R.id.contact_name);
        contact_mobile = (TextView) findViewById(R.id.contact_mobile);
        contact_mobile.setOnClickListener(this);

        ;

        Intent intent = getIntent();
        String c_id = intent.getStringExtra("customer_id");
        getCustDetails(c_id);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.customer_address:

                String address = customer_address.getText().toString();
                Uri gmmIntentUri = Uri.parse("google.navigation:q=" + address + "&mode=d");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);

                //
                break;
            case R.id.contact_mobile:

                if (Build.VERSION.SDK_INT > 22) {

                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                        ActivityCompat.requestPermissions(CustomerDetailsList.this, new String[]{Manifest.permission.CALL_PHONE}, 101);

                        return;
                    }
                    String mobile_number = contact_mobile.getText().toString();
                    String phone_no= mobile_number.replaceAll("-", "");
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:+" + phone_no.trim()));
                    startActivity(callIntent);
                } else {
                    String mobile_number = contact_mobile.getText().toString();
                    String phone_no= mobile_number.replaceAll("-", "");
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:+" + phone_no.trim()));
                    startActivity(callIntent);
                }



        }
    }

    public void goBack(View view) {
        super.onBackPressed();
    }
    public void SQLiteDataBaseBuild() {
        sqLiteDatabase = openOrCreateDatabase(SQLiteHelper.DATABASE_NAME, Context.MODE_PRIVATE, null);

    }
    @Override
    protected void onResume() {
        Intent intent = getIntent();
        String c_id = intent.getStringExtra("customer_id");
        getCustDetails(c_id);
        super.onResume();
    }

    public void getCustDetails(final String id) {
        sqLiteDatabase = sqLiteHelper.getWritableDatabase();

        cursor = sqLiteDatabase.rawQuery("SELECT * FROM " + SQLiteHelper.TABLE_NAME+ " WHERE " + SQLiteHelper.ID_COLUMN + " = '" + id + "'  ", null);

        if (cursor.moveToFirst()) {
            String c_name = cursor.getString(cursor.getColumnIndex("cust_name"));
            String c_address = cursor.getString(cursor.getColumnIndex("addr_line_one"));
            String cont_name = cursor.getString(cursor.getColumnIndex("contact_name"));
            String cont_mobile = cursor.getString(cursor.getColumnIndex("contact_phone"));


            customer_name.setText(c_name);
            customer_address.setText(c_address);
            contact_name.setText(cont_name);
            contact_mobile.setText(cont_mobile);


        }
    }
}
