package com.findeet.veegloapps.nldmanagement;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.os.Build;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.Toast;

import com.findeet.veegloapps.nldmanagement.Utils.SQLiteHelperb;
import com.findeet.veegloapps.nldmanagement.adapters.AdapterPulled;
import com.findeet.veegloapps.nldmanagement.adapters.AdapterRouteShops;
import com.findeet.veegloapps.nldmanagement.adapters.AdapterRoutes;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

public class RouteShops extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {
    android.support.v7.widget.Toolbar routes_toolbar;
    private RecyclerView list_q,zmycustRecycleViewa;
    private AdapterRouteShops zmAdaptera;
    SwipeRefreshLayout myrefresh;
    SQLiteHelperb sqLiteHelperb;
    SQLiteDatabase sqLiteDatabase;
    ImageView refresh;

    private Location location;
    private GoogleApiClient googleApiClient;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private LocationRequest locationRequest;
    private static final long UPDATE_INTERVAL = 5000, FASTEST_INTERVAL = 5000; // = 5 seconds
    // lists for permissions
    private ArrayList<String> permissionsToRequest;
    private ArrayList<String> permissionsRejected = new ArrayList<>();
    private ArrayList<String> permissions = new ArrayList<>();
    // integer for permissions results request
    private static final int ALL_PERMISSIONS_RESULT = 1011;

    Cursor cursor;
    private final Context context = this;
    ArrayList<String> route_name_Array,cust_number_Array,cust_name_Array,last_date_seen_Array,last_order_size_Array;
    private BroadcastReceiver broadcastReceiver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route_shops);
        routes_toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.route_shop_toolbar);
        setSupportActionBar(routes_toolbar);

        Intent in = getIntent();
        String route_name= in.getExtras().getString("route_name");
        ActionBar mybar = getSupportActionBar();
        mybar.setTitle(route_name);

        route_name_Array = new ArrayList<String>();
        cust_number_Array = new ArrayList<String>();
        cust_name_Array = new ArrayList<String>();
        last_date_seen_Array = new ArrayList<String>();
        last_order_size_Array = new ArrayList<String>();
        sqLiteHelperb = new SQLiteHelperb(this);
        showRouteShops();



        // we add permissions we need to request location of the users
        permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);

        permissionsToRequest = permissionsToRequest(permissions);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (permissionsToRequest.size() > 0) {
                requestPermissions(permissionsToRequest.toArray(
                        new String[permissionsToRequest.size()]), ALL_PERMISSIONS_RESULT);
            }
        }

        // we build google api client
        googleApiClient = new GoogleApiClient.Builder(this).
                addApi(LocationServices.API).
                addConnectionCallbacks(this).
                addOnConnectionFailedListener(this).build();


    }
    private ArrayList<String> permissionsToRequest(ArrayList<String> wantedPermissions) {
        ArrayList<String> result = new ArrayList<>();

        for (String perm : wantedPermissions) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
        }

        return true;
    }

    private void showRouteShops() {

        sqLiteDatabase = sqLiteHelperb.getWritableDatabase();
        Intent in = getIntent();
        String route_name= in.getExtras().getString("route_name");

        cursor = sqLiteDatabase.rawQuery("SELECT * FROM routes_shops WHERE route_name = '"+route_name+"' AND cust_name !='GeorgeImport' ORDER BY id ASC ", null);
        route_name_Array.clear();
        cust_number_Array.clear();
        cust_name_Array.clear();
        last_date_seen_Array.clear();
        last_order_size_Array.clear();

        if (cursor.moveToFirst()) {
            do {
                //  DOUBLE(13,2),note VARCHAR(50),
                route_name_Array.add(cursor.getString(cursor.getColumnIndex("route_name")));
                cust_number_Array.add(cursor.getString(cursor.getColumnIndex("cust_number")));
                cust_name_Array.add(cursor.getString(cursor.getColumnIndex("cust_name")));
                last_date_seen_Array.add(cursor.getString(cursor.getColumnIndex("last_date_seen")));
                last_order_size_Array.add(cursor.getString(cursor.getColumnIndex("last_order_size")));
            }

            while (cursor.moveToNext());
        }

        // Setup and Handover data to recyclerview
        zmycustRecycleViewa = (RecyclerView) findViewById(R.id.list_route_shop);
        zmAdaptera = new AdapterRouteShops(this, route_name_Array,cust_number_Array,cust_name_Array,last_date_seen_Array,last_order_size_Array);
        zmycustRecycleViewa.setAdapter(zmAdaptera);
        zmycustRecycleViewa.setLayoutManager(new LinearLayoutManager(this));

        cursor.close();
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (googleApiClient != null) {
            googleApiClient.connect();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!checkPlayServices()) {
            //        SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = shared.edit();
//        editor.putString("LatLong", String.valueOf(latLng));
//        editor.apply();
            Toast.makeText(this, "You need to install Google Play Services to use the App properly", Toast.LENGTH_LONG).show();


        }
    }
    @Override
    protected void onPause() {
        super.onPause();

        // stop location updates
        if (googleApiClient != null  &&  googleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
            googleApiClient.disconnect();
        }
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);

        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST);
            } else {
                finish();
            }

            return false;
        }
        return true;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                &&  ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        // Permissions ok, we get last location
        location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);

        if (location != null) {
            //locationTv.setText("Latitude : " + location.getLatitude() + "\nLongitude : " + location.getLongitude());

        String latLng = location.getLatitude()+","+location.getLongitude();
        SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = shared.edit();
        editor.putString("LatLong", latLng);
        editor.apply();
        //Toast.makeText(this, "Your location is "+latLng, Toast.LENGTH_LONG).show();

        }

        startLocationUpdates();
    }

    private void startLocationUpdates() {

        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(UPDATE_INTERVAL);
        locationRequest.setFastestInterval(FASTEST_INTERVAL);

        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                &&  ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "You need to enable permissions to display location !", Toast.LENGTH_SHORT).show();
        }

        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            //locationTv.setText("Latitude : " + location.getLatitude() + "\nLongitude : " + location.getLongitude());
            String latLng = location.getLatitude()+","+location.getLongitude();
            SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = shared.edit();
            editor.putString("LatLong", latLng);
            editor.apply();
            //Toast.makeText(this, "Your location is "+latLng, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch(requestCode) {
            case ALL_PERMISSIONS_RESULT:
                for (String perm : permissionsToRequest) {
                    if (!hasPermission(perm)) {
                        permissionsRejected.add(perm);
                    }
                }

                if (permissionsRejected.size() > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) {
                            new AlertDialog.Builder(RouteShops.this).
                                    setMessage("These permissions are mandatory to get your location. You need to allow them.").
                                    setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(permissionsRejected.
                                                        toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    }).setNegativeButton("Cancel", null).create().show();

                            return;
                        }
                    }
                } else {
                    if (googleApiClient != null) {
                        googleApiClient.connect();
                    }
                }

                break;
        }
    }
}
