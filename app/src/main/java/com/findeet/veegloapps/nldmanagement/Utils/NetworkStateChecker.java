package com.findeet.veegloapps.nldmanagement.Utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.hardware.usb.UsbEndpoint;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.findeet.veegloapps.nldmanagement.EndPoints;
import com.findeet.veegloapps.nldmanagement.Login;
import com.findeet.veegloapps.nldmanagement.MainActivity;
import com.findeet.veegloapps.nldmanagement.Orders;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by DEBONGZ on 8/22/2018.
 */

public class NetworkStateChecker extends BroadcastReceiver {

    //context and database helper object
    private Context context;
    SQLiteDatabase sqLiteDatabase;
    SQLiteHelper sqLiteHelper;
    Cursor cursor;


    @Override
    public void onReceive(Context context, Intent intent) {

        this.context = context;

        sqLiteHelper = new SQLiteHelper(context);
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        //if there is a network
        if (activeNetwork != null) {
            //if connected to wifi or mobile data plan
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI || activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                ShowSQLiteDBdata();
            }
        }
    }
    private void ShowSQLiteDBdata() {

        sqLiteDatabase = sqLiteHelper.getWritableDatabase();
        String value ="not_synced";
        cursor = sqLiteDatabase.rawQuery("SELECT * FROM "+SQLiteHelper.TABLE_NAME+" WHERE "+SQLiteHelper.Table_Column_1_Subject_Sync_Status+" = '"+value+"'  ", null);

        int count = 0;
        if (cursor.moveToFirst()) {
            do {
                String custname =cursor.getString(cursor.getColumnIndex(SQLiteHelper.Table_Column_1_Subject_Cname));
                String custaddr =cursor.getString(cursor.getColumnIndex(SQLiteHelper.Table_Column_1_Subject_Caddr));
                String contactperson =cursor.getString(cursor.getColumnIndex(SQLiteHelper.Table_COLUMN_CONTACT_NAME));
                String phone =cursor.getString(cursor.getColumnIndex(SQLiteHelper.Table_COLUMN_CONTACT_PHONE));
                String route = cursor.getString(cursor.getColumnIndex(SQLiteHelper.Table_COLUMN_ROUTE));
                String longi = cursor.getString(cursor.getColumnIndex(SQLiteHelper.Table_COLUMN_LONG));
                String lati = cursor.getString(cursor.getColumnIndex(SQLiteHelper.Table_COLUMN_LAT));
                String rep_id = cursor.getString(cursor.getColumnIndex(SQLiteHelper.Table_COLUMN_USER_ID));
                String cust_id = cursor.getString(cursor.getColumnIndex(SQLiteHelper.Table_COLUMN_CUST_ID));

                saveCustomer(cust_id,custname,custaddr,contactperson,phone,route,longi,lati,rep_id);
                count+=count+1;

            }

            while (cursor.moveToNext());
        }

        int rowCount = count;
        //Toast.makeText(context, "" + rowCount, Toast.LENGTH_SHORT).show();
    }


    /*
    * method taking two arguments
    * name that is to be saved and id of the name from SQLite
    * if the name is successfully sent
    * we will update the status as synced in SQLite
    * */
    public void saveCustomer(final String cust_id,final String custname,final String custaddr,final String contactperson,final String phone,final String route,final String longi,final String lati,final String rep_id) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Orders.URL_SAVE_NAME,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("result");
                            JSONObject jsonObject1 = jsonArray.getJSONObject(0);

                            String status = jsonObject1.getString("status");
                            String cust_id = jsonObject1.getString("cust_id");
                            String cust_number = jsonObject1.getString("cust_number");
                            String cust_nane = jsonObject1.getString("cust_name");
                           // Toast.makeText(context, "status" + status+" cust number" + cust_number, Toast.LENGTH_SHORT).show();
                          if(status.equalsIgnoreCase("done"))
                            {
                                updateCustNumber(cust_number,cust_id,cust_nane);
                                //sending the broadcast to refresh the list
                                context.sendBroadcast(new Intent(EndPoints.CUSTOMER_SAVED_BROADCAST));
                            }
                            else{}

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("cust_id", cust_id);
                params.put("custname", custname);
                params.put("custaddr", custaddr);
                params.put("contactperson", contactperson);
                params.put("phone", phone);
                params.put("route", route);
                params.put("longi", longi);
                params.put("lati", lati);
                params.put("rep_id", rep_id);
                return params;
            }
        };

        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }
    public void updateNameStatus(final String cust_id){
        sqLiteDatabase.execSQL("UPDATE customers SET sync_status ='synced' WHERE cust_id ='"+cust_id+"' ");
    }
    public void updateCustNumber(final String cust_number,final String cust_id,final String cust_name){
        sqLiteDatabase.execSQL("UPDATE customers SET cust_number ='"+cust_number+"', cust_name ='"+cust_name+"', sync_status ='synced' WHERE cust_id ='"+cust_id+"' ");
    }

}
