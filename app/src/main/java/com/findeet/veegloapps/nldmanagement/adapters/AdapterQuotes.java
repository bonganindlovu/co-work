package com.findeet.veegloapps.nldmanagement.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.findeet.veegloapps.nldmanagement.EndPoints;
import com.findeet.veegloapps.nldmanagement.Order;
import com.findeet.veegloapps.nldmanagement.R;
import com.findeet.veegloapps.nldmanagement.Utils.SQLiteHelper;

import java.util.ArrayList;

/**
 * Created by DEBONGZ on 1/6/2018.
 */

public class AdapterQuotes extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    SQLiteDatabase sqLiteDatabase;
    SQLiteHelper sqLiteHelper;
    private Context context;
    private LayoutInflater inflater;

    ArrayList<String> ocust_number;
    ArrayList<String> ocust_addr;
    ArrayList<String> oorder_total;
    ArrayList<String> odate_of_sale;
    ArrayList<String> oorder_id;
    ArrayList<String> oorder_synce_status;


    ArrayList<String> oitem_synce_state;
    int currentPos=0;

    // create constructor to initialize context and data sent from MainActivity
    public AdapterQuotes(Context context, ArrayList<String> cust_number, ArrayList<String> cust_addr, ArrayList<String> order_total, ArrayList<String> date_of_sale, ArrayList<String> order_id,ArrayList<String> order_synce_status){
        this.context=context;
        inflater= LayoutInflater.from(context);
        this.ocust_number = cust_number;
        this.ocust_addr = cust_addr;
        this.oorder_total = order_total;
        this.odate_of_sale = date_of_sale;
        this.oorder_id = order_id;
        this.oorder_synce_status = order_synce_status;

    }

    // Inflate the layout when ViewHolder created
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=inflater.inflate(R.layout.container_quotes, parent,false);
        MyHolder holder=new MyHolder(view);

        return holder;
    }

    // Bind data
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        // Get current position of item in RecyclerView to bind data and assign values from list
        MyHolder myHolder= (MyHolder) holder;
        myHolder.txtQcustName.setText(ocust_number.get(position));
        myHolder.txtQorderTotal.setText(oorder_total.get(position));
        myHolder.txtQOrderDate.setText(odate_of_sale.get(position));
        myHolder.txtQOrderNum.setText(oorder_id.get(position));
        myHolder.txtQCustAddr.setText(ocust_addr.get(position));

        String oder_sync_status = oorder_synce_status.get(position);

        if (oder_sync_status.equalsIgnoreCase("synced")){
            myHolder.Porder_sync_prog.setVisibility(View.GONE);
            myHolder.Rsent_badge.setVisibility(View.VISIBLE);
        }
        else{
            myHolder.Porder_sync_prog.setVisibility(View.VISIBLE);
            myHolder.Rsent_badge.setVisibility(View.GONE);

            myHolder.Rquote_line.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {


                    new AlertDialog.Builder(context)
                            .setTitle("Alert")
                            .setMessage("You are about to delete this order")
                            .setCancelable(true)
                            .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    RemoveEmptyOrder(oorder_id.get(position)) ;
                                    context.sendBroadcast(new Intent(EndPoints.ORDER_SAVED_BROADCAST));
                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    dialog.cancel();
                                }
                            })
                            .show();
                    return false;
                }
            });

        }

    }
    // return total item from List
    @Override
    public int getItemCount() {
        return oorder_id.size();
    }

    class MyHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView txtQcustName;
        TextView txtQorderTotal;
        TextView txtQOrderDate;
        TextView txtQOrderNum;
        TextView txtQCustAddr;
        RelativeLayout Porder_sync_prog;
        RelativeLayout Rsent_badge;
        RelativeLayout Rquote_line;




        // create constructor to get widget reference
        public MyHolder(View itemViewa) {
            super(itemViewa);
            txtQcustName = (TextView) itemViewa.findViewById(R.id.txtqname);
            txtQorderTotal = (TextView) itemViewa.findViewById(R.id.txtqtotal);
            txtQOrderDate = (TextView) itemViewa.findViewById(R.id.txtqdate);
            txtQOrderNum = (TextView) itemViewa.findViewById(R.id.txtqid);
            txtQCustAddr = (TextView) itemViewa.findViewById(R.id.txtqcustAddr);
            Porder_sync_prog =(RelativeLayout)itemViewa.findViewById(R.id.order_sync_status);
            Rsent_badge =(RelativeLayout) itemViewa.findViewById(R.id.sent_badge);
            Rquote_line =(RelativeLayout) itemViewa.findViewById(R.id.quote_line);
            itemViewa.setOnClickListener(this);
        }

        // Click event for all items
        @Override
        public void onClick(View v) {

            String shop_number = "";
            String shop_name = txtQcustName.getText().toString();
            String shop_addr = txtQCustAddr.getText().toString();
            String uniqueID = txtQOrderNum.getText().toString();

            SharedPreferences shared = context.getSharedPreferences("Order", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = shared.edit();
            editor.putString("order_id", uniqueID);
            editor.putString("cust_number", shop_number);
            editor.putString("cust_name", shop_name);
            editor.putString("cust_addr", shop_addr);
            editor.commit();


            Intent intent= new Intent(context, Order.class);
            context.startActivity(intent);
            //finish this
            ((Activity)context).finish();

        }
    }
    public void RemoveEmptyOrder(final String order_id){
        //sales_order_items
        sqLiteDatabase = context.openOrCreateDatabase(SQLiteHelper.DATABASE_NAME, Context.MODE_PRIVATE, null);
        sqLiteDatabase.execSQL("DELETE FROM sales_order WHERE order_id ='"+order_id+"' ");
        sqLiteDatabase.execSQL("DELETE FROM sales_order_items WHERE order_id ='"+order_id+"' ");
    }

}
