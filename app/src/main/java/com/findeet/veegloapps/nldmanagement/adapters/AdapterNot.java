package com.findeet.veegloapps.nldmanagement.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.findeet.veegloapps.nldmanagement.ConfirmSignature;
import com.findeet.veegloapps.nldmanagement.EmpDash;
import com.findeet.veegloapps.nldmanagement.R;
import com.findeet.veegloapps.nldmanagement.datas.DataNot;
import java.util.Collections;
import java.util.List;




/**
 * Created by DEBONGZ on 3/15/2018.
 */

public class AdapterNot extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private LayoutInflater inflater;
    List<DataNot> data= Collections.emptyList();
    DataNot current;
    int currentPos=0;
    Bitmap bitmap;


    // create constructor to initialize context and data sent from MainActivity
    public AdapterNot(Context context, List<DataNot> data){
        this.context=context;
        inflater= LayoutInflater.from(context);
        this.data=data;
    }
    // Inflate the layout when ViewHolder created
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=inflater.inflate(R.layout.container_notification, parent,false);
        MyHolder holder=new MyHolder(view);
        return holder;
    }
    // Bind data
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        // Get current position of item in RecyclerView to bind data and assign values from list
        final MyHolder myHolder= (MyHolder) holder;
        final DataNot current=data.get(position);

        //TextView txtdate_txt,txttitle,txttime,txtshop_name,txttotal,txtnot_id,txtorder_id,txtshop_id;

        myHolder.txtdate_txt.setText(current.date_sent);
        String the_status = current.status;
        String the_type = current.not_type;
        if(the_status.equalsIgnoreCase("Due")){
            myHolder.txttitle.setText("Hi, you have new outstanding orders");
            myHolder.txttotal.setText(" ");

        }
        else if(the_type.equalsIgnoreCase("clock_status") || the_type.equalsIgnoreCase("clock_request")){
            myHolder.txttitle.setText(current.status);
            myHolder.txttotal.setText(" ");
        }
        else if(the_type.equalsIgnoreCase("placed_order")){
            myHolder.txttitle.setText(current.status);
            myHolder.txttotal.setText(" ");
        }
        else{
            myHolder.txttitle.setText("Hi, your order has been "+current.status);
            myHolder.txttotal.setText("R "+current.invoice_amount);
        }

        myHolder.txttime.setText(current.time_sent);
        myHolder.txtshop_name.setText(current.shop_name);

        myHolder.txtnot_id.setText(current.not_id);
        myHolder.txtorder_id.setText(current.order_id);
        myHolder.txtshop_id.setText(current.shop_id);
        myHolder.txtnot_type.setText(current.not_type);

        String cur_date=  current.date_sent;
        String previousTs = "";

        if(position>0){
            int new_pos = position-1;
            //Toast.makeText(context, "new:"+new_pos, Toast.LENGTH_SHORT).show();
            DataNot pm = data.get(position-1);
            previousTs = pm.date_sent;
        }
        setDateTextVisibility(cur_date, previousTs, myHolder.datetab);


    }
    // return total item from List
    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyHolder extends RecyclerView.ViewHolder  {
        TextView txtdate_txt,txttitle,txttime,txtshop_name,txttotal,txtnot_id,txtorder_id,txtshop_id,txtnot_type;
        RelativeLayout datetab;
        LinearLayout line;

        // create constructor to get widget reference
        public MyHolder(View itemView) {
            super(itemView);

            line = (LinearLayout) itemView.findViewById(R.id.quote_line);
            datetab = (RelativeLayout)itemView.findViewById(R.id.date_tab);
            txtnot_type= (TextView) itemView.findViewById(R.id.not_type);
            txtdate_txt = (TextView) itemView.findViewById(R.id.date_txt);
            txttitle = (TextView) itemView.findViewById(R.id.title);
            txttime = (TextView) itemView.findViewById(R.id.not_time);
            txtshop_name = (TextView) itemView.findViewById(R.id.shop_name);
            txttotal = (TextView) itemView.findViewById(R.id.total);
            txtnot_id = (TextView) itemView.findViewById(R.id.not_id);
            txtorder_id = (TextView) itemView.findViewById(R.id.order_id);
            txtshop_id = (TextView) itemView.findViewById(R.id.shop_id);

            line.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {

                                            String not_type=txtnot_type.getText().toString();
                                            String order_id=txtorder_id.getText().toString() ;
                                            String shop_id=txtshop_id.getText().toString();


                                            if (not_type.equalsIgnoreCase("order_status")|| not_type.isEmpty()|| not_type.equalsIgnoreCase(null)){
                                                String link = "http://nextld.co.za/salesadmin/finalinvoice.php?order_id="+order_id+
                                                        "&shop_id="+shop_id;

                                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                                intent.setData(Uri.parse(link));
                                                context.startActivity(intent);
                                            }
                                            else if(not_type.equalsIgnoreCase("placed_order")){
                                                String link = "http://nextld.co.za/salesadmin/new_order.php?order_id="+order_id;

                                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                                intent.setData(Uri.parse(link));
                                                context.startActivity(intent);

                                            }
                                            else if(not_type.equalsIgnoreCase("clock_status")){
                                                Intent intent = new Intent(context, EmpDash.class);
                                                context.startActivity(intent);
                                                ((Activity)context).finish();

                                            }
                                            else if(not_type.equalsIgnoreCase("clock_request")){
                                                Intent intent = new Intent(context, ConfirmSignature.class);
                                                intent.putExtra("order_id", order_id);
                                                context.startActivity(intent);
                                                ((Activity)context).finish();

                                            }
                                            else if(not_type.equalsIgnoreCase("due_not")){
                                                String link = "http://nextld.co.za/salesadmin/due_invoices.php";

                                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                                intent.setData(Uri.parse(link));
                                                context.startActivity(intent);

                                            }




                                        }
                });




        }





    }

    private void setDateTextVisibility(String ts1, String ts2, RelativeLayout timeText){
        //Toast.makeText(context, "current:"+ts1+" \nPrev:"+ts2, Toast.LENGTH_SHORT).show();
        if(ts2.isEmpty()){
            timeText.setVisibility(View.VISIBLE);
        }else if (ts1.equalsIgnoreCase(ts2)){
            timeText.setVisibility(View.GONE);

        }
        else{
            timeText.setVisibility(View.VISIBLE);
        }
    }



}



