package com.findeet.veegloapps.nldmanagement;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.findeet.veegloapps.nldmanagement.Utils.DBController;
import com.findeet.veegloapps.nldmanagement.Utils.NetworkStateChecker;
import com.findeet.veegloapps.nldmanagement.Utils.SQLiteHelper;
import com.findeet.veegloapps.nldmanagement.adapters.AdapterLoadCustomers;

import java.util.ArrayList;

public class LoadCustomers extends AppCompatActivity {
    // CONNECTION_TIMEOUT and READ_TIMEOUT are in milliseconds
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 15000;
    private RecyclerView mycustRecycleView;
    private AdapterLoadCustomers mAdapter;
    ProgressBar loadcustbar;
    TextView search;
    DBController controller = new DBController(this);
    SearchView searchView = null;
    SQLiteDatabase sqLiteDatabase;
    ArrayList<String> cust_number_Array;
    ArrayList<String> cust_NAME_Array;
    ArrayList<String> custAddr_Array;
    FloatingActionButton add_new_customer;
    private BroadcastReceiver broadcastReceiver;
    android.support.v7.widget.Toolbar customers_toolbar;
    SQLiteHelper sqLiteHelper;
    Cursor cursor;

    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customers);
        loadcustbar = (ProgressBar)findViewById(R.id.load_re);
        search = (TextView)findViewById(R.id.serach_cust_txt);
        sqLiteHelper = new SQLiteHelper(this);
        add_new_customer = (FloatingActionButton)findViewById(R.id.add_new_customer);

        customers_toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.shop_toolbar);
        setSupportActionBar(customers_toolbar);
        SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
        String repname =shared.getString("user_name", "");
        ActionBar mybar = getSupportActionBar();
        mybar.setTitle("");


        cust_number_Array = new ArrayList<String>();
        cust_NAME_Array = new ArrayList<String>();
        custAddr_Array = new ArrayList<String>();
        add_new_customer.setVisibility(View.GONE);
        ShowSQLiteDBdata() ;
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                //Updating Array Adapter ListView after typing inside EditText.
                String e = search.getText().toString();
                ShowSQLiteDBdata() ;
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            ShowSQLiteDBdata() ;
            }

            @Override
            public void afterTextChanged(Editable editable) {
                ShowSQLiteDBdata() ;
            }
        });

        //the broadcast receiver to update sync status
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                //loading the names again
                ShowSQLiteDBdata() ;
            }
        };

        //registering the broadcast receiver to update sync status
        registerReceiver(broadcastReceiver, new IntentFilter(EndPoints.CUSTOMER_SAVED_BROADCAST));

    }

    private void ShowSQLiteDBdata() {

        sqLiteDatabase = sqLiteHelper.getWritableDatabase();
        // List<DataCustomers> data=new ArrayList<>();

        String value = "%" + search.getText().toString() + "%";
        String value2 = search.getText().toString();

        cursor = sqLiteDatabase.rawQuery("SELECT * FROM "+SQLiteHelper.TABLE_NAME+" WHERE "+SQLiteHelper.Table_Column_1_Subject_Cname+" LIKE '"+value+"' OR cust_number = '"+value2+"' ", null);
        cust_number_Array.clear();
        cust_NAME_Array.clear();
        custAddr_Array.clear();
//        cursor.getColumnCount();

        if (cursor.moveToFirst()) {
            do {
                cust_number_Array.add(cursor.getString(cursor.getColumnIndex(SQLiteHelper.Table_Column_1_Subject_Cnumber)));
                cust_NAME_Array.add(cursor.getString(cursor.getColumnIndex(SQLiteHelper.Table_Column_1_Subject_Cname)));
                custAddr_Array.add(cursor.getString(cursor.getColumnIndex(SQLiteHelper.Table_Column_1_Subject_Caddr)));
            }

            while (cursor.moveToNext());
        }

        mycustRecycleView = (RecyclerView) findViewById(R.id.list_view);
        mAdapter = new AdapterLoadCustomers(LoadCustomers.this, cust_number_Array,cust_NAME_Array,custAddr_Array);
        mycustRecycleView.setAdapter(mAdapter);
        mycustRecycleView.setLayoutManager(new LinearLayoutManager(LoadCustomers.this));

        cursor.close();
    }

    @Override
    protected void onResume() {
        registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        ShowSQLiteDBdata() ;

        super.onResume();
    }
}
