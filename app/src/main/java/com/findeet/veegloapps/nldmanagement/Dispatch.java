package com.findeet.veegloapps.nldmanagement;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.findeet.veegloapps.nldmanagement.Utils.SQLiteHelper;
import com.findeet.veegloapps.nldmanagement.Utils.SQLiteHelperb;
import com.findeet.veegloapps.nldmanagement.Utils.VolleySingleton;
import com.findeet.veegloapps.nldmanagement.adapters.AdapterDispatch;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class Dispatch extends AppCompatActivity {
    Toolbar toolbar;
    SQLiteDatabase sqLiteDatabase;
    SQLiteHelperb sqLiteHelperb;
    FloatingActionButton add_dispatch;
    Spinner vehicle_txt,driver_txt;
    BroadcastReceiver broadcastReceiver;
    private RecyclerView zmycustRecycleViewa;
    private AdapterDispatch mAdaptera;
    private AdapterDispatch zmAdaptera;
    Cursor cursor;
    ArrayList<String> load_id_Array,vehicle_name_Array,driver_name_Array,load_time_Array,sync_state_Array;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dispatch);
        toolbar = (Toolbar) findViewById(R.id.toolbar_dispatch);
        SQLiteDataBaseBuild();
        sqLiteHelperb = new SQLiteHelperb(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Dispatch");
        final Context c = this;
        add_dispatch = (FloatingActionButton)findViewById(R.id.add_dispatch);
        load_id_Array = new ArrayList<String>();
        vehicle_name_Array = new ArrayList<String>();
        driver_name_Array = new ArrayList<String>();
        sync_state_Array=  new ArrayList<String>();
        load_time_Array=new ArrayList<String>();
        loadsSQLite();

        //the broadcast receiver to update sync status
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                //loading the names again
                loadsSQLite();
            }
        };

        //registering the broadcast receiver to update sync status
        registerReceiver(broadcastReceiver, new IntentFilter(EndPoints.ORDER_SAVED_BROADCAST+"loads"));


        add_dispatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater layoutInflaterAndroid = LayoutInflater.from(c);
                View mView = layoutInflaterAndroid.inflate(R.layout.dispatch_dialog_box, null);
                AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(c);
                alertDialogBuilderUserInput.setView(mView);
                vehicle_txt = (Spinner)mView.findViewById(R.id.vehicle_txt);
                driver_txt = (Spinner)mView.findViewById(R.id.driver_txt);
               // final EditText feedback = (EditText) mView.findViewById(R.id.feedback_txt);


                alertDialogBuilderUserInput
                        .setCancelable(false)
                        .setPositiveButton("ADD", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                String vehicle_text = String.valueOf(vehicle_txt.getSelectedItem());
                                String driver_text = String.valueOf(driver_txt.getSelectedItem());

                                if (driver_text.equalsIgnoreCase("THOMAS")){
                                    driver_text ="thomas";
                                }
                                else if (driver_text.equalsIgnoreCase("GILBERT")){
                                    driver_text ="gilbert";
                                }
                                else if (driver_text.equalsIgnoreCase("OFENTSE")){
                                    driver_text ="ofentse";
                                }
                                else if (driver_text.equalsIgnoreCase("BONGANI")){
                                    driver_text ="debongz09";
                                }
                                else if (driver_text.equalsIgnoreCase("MARVELLOUS")){
                                    driver_text ="marve";
                                }
                                else{
                                    driver_text = String.valueOf(driver_txt.getSelectedItem());
                                }


                                if (vehicle_text.equalsIgnoreCase("Select Vehicle")){
                                    Toast.makeText(Dispatch.this, "Please Chose Vehicle", Toast.LENGTH_SHORT).show();
                                }
                                else if (driver_text.equalsIgnoreCase("Select Driver")){
                                    Toast.makeText(Dispatch.this, "Please Chose Driver", Toast.LENGTH_SHORT).show();
                                }
                                else{
                                    //add load to database
                                    String load_id = UUID.randomUUID().toString();
                                    sendLoads(vehicle_text,driver_text,load_id);
                                }


                            }
                        })

                        .setNegativeButton("CANCEL",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogBox, int id) {
                                        //dialogBox.cancel();
                                    }
                                });

                AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
                alertDialogAndroid.show();

            }
        });
    }

    public void sendLoads(final String vehicle_name,final String drive_name, final String load_id){



        StringRequest stringRequest = new StringRequest(Request.Method.POST, EndPoints.SEND_LOADS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String result) {
                        try {
                            JSONArray jArray = new JSONArray(result);

                            for (int i = 0; i < jArray.length(); i++) {
                                JSONObject d = jArray.getJSONObject(i);
                                JSONObject json_data = jArray.getJSONObject(i);
                                String load_id = json_data.getString("load_id");

                                AddLoadOn(vehicle_name,drive_name,load_id);

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        AddLoadOff(vehicle_name,drive_name,load_id);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                String timeStamp = String.valueOf(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()));
                Map<String, String> params = new HashMap<>();
                params.put("load_id", load_id);
                params.put("vehicle_name", vehicle_name);
                params.put("drive_name", drive_name);
                params.put("load_time", timeStamp);
                return params;
            }
        };
        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);

    }
    public void AddLoadOn(final String vehicle_name, final String driver_name, final String load_id){
        sqLiteDatabase.beginTransaction(); //getTimestamp
        String database = "loads";
        String timeStamp = String.valueOf(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()));
        String SQLiteDataBaseQueryHolder = "INSERT INTO "+database+" (load_id,vehicle_name,drive_name,load_time,synce_status" +
                ") VALUES('"+load_id+"', '"+vehicle_name+"', '"+driver_name+"','"+timeStamp+"', 'synced');";
        sqLiteDatabase.execSQL(SQLiteDataBaseQueryHolder);
        Toast.makeText(Dispatch.this, "Load Inserted", Toast.LENGTH_SHORT).show();
        sqLiteDatabase.setTransactionSuccessful();
        sqLiteDatabase.endTransaction();
        loadsSQLite();
    }
    public void AddLoadOff(final String vehicle_name, final String driver_name, final String load_id){
        sqLiteDatabase.beginTransaction(); //getTimestamp
        String database = "loads";
        String timeStamp = String.valueOf(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()));
        String SQLiteDataBaseQueryHolder = "INSERT INTO "+database+" (load_id,vehicle_name,drive_name,load_time,synce_status" +
                ") VALUES('"+load_id+"', '"+vehicle_name+"', '"+driver_name+"','"+timeStamp+"', 'not_synced');";
        sqLiteDatabase.execSQL(SQLiteDataBaseQueryHolder);
        Toast.makeText(Dispatch.this, "Load Inserted", Toast.LENGTH_SHORT).show();
        sqLiteDatabase.setTransactionSuccessful();
        sqLiteDatabase.endTransaction();
        loadsSQLite();
    }
    public void SQLiteDataBaseBuild(){

        sqLiteDatabase = openOrCreateDatabase(SQLiteHelper.DATABASE_NAME, Context.MODE_PRIVATE, null);

    }
    private void loadsSQLite(){
        sqLiteDatabase = sqLiteHelperb.getWritableDatabase();

        cursor = sqLiteDatabase.rawQuery("SELECT * FROM loads ORDER BY load_time DESC ", null);
        load_id_Array.clear();
        vehicle_name_Array.clear();
        driver_name_Array.clear();
        sync_state_Array.clear();
        load_time_Array.clear();
        if (cursor.moveToFirst()) {
            do {
                //  DOUBLE(13,2),note VARCHAR(50),
                load_id_Array.add(cursor.getString(cursor.getColumnIndex("load_id")));
                vehicle_name_Array.add(cursor.getString(cursor.getColumnIndex("vehicle_name")));
                driver_name_Array.add(cursor.getString(cursor.getColumnIndex("drive_name")));
                sync_state_Array.add(cursor.getString(cursor.getColumnIndex("synce_status")));
                load_time_Array.add(cursor.getString(cursor.getColumnIndex("load_time")));
            }

            while (cursor.moveToNext());
        }

        zmycustRecycleViewa = (RecyclerView) findViewById(R.id.dispatchRecycle);
        zmAdaptera = new AdapterDispatch(Dispatch.this, load_id_Array,vehicle_name_Array,driver_name_Array,sync_state_Array,load_time_Array);
        zmycustRecycleViewa.setAdapter(zmAdaptera);
        zmycustRecycleViewa.setLayoutManager(new LinearLayoutManager(Dispatch.this));

        cursor.close();
    }
}
