package com.findeet.veegloapps.nldmanagement.firebase;

/**
 * Created by DEBONGZ on 3/13/2018.
 */

public class Constants {
    public static final String CHANNEL_ID = "my_channel_01";
    public static final String CHANNEL_NAME = "Simplified Coding Notification";
    public static final String CHANNEL_DESCRIPTION = "www.simplifiedcoding.net";
}
