package com.findeet.veegloapps.nldmanagement.picker.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.findeet.veegloapps.nldmanagement.EndPoints;
import com.findeet.veegloapps.nldmanagement.R;
import com.findeet.veegloapps.nldmanagement.datas.DataOrder;
import com.findeet.veegloapps.nldmanagement.picker.OrderItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by DEBONGZ on 1/6/2018.
 */

public class AdapterOrder extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private LayoutInflater inflater;
    List<DataOrder> data= Collections.emptyList();
    DataOrder current;
    int currentPos=0;

    // create constructor to initialize context and data sent from MainActivity
    public AdapterOrder(Context context, List<DataOrder> data){
        this.context=context;
        inflater= LayoutInflater.from(context);
        this.data=data;
    }

    // Inflate the layout when ViewHolder created
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=inflater.inflate(R.layout.container_order2, parent,false);
        MyHolder holder=new MyHolder(view);
        return holder;
    }

    // Bind data
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        //holder.setIsRecyclable(false);

        // Get current position of item in RecyclerView to bind data and assign values from list
        final MyHolder myHolder= (MyHolder) holder;
        myHolder.setIsRecyclable(false);
        final DataOrder current=data.get(position);
        myHolder.txtOname.setText(current.OderName);
        myHolder.txtOunit.setText(current.OrderUnit);
        myHolder.txtOqty.setText(current.OrderQty);
        myHolder.txtOtotal.setText(current.OrderTotal);
        myHolder.txtOnum.setText(current.OrderNum);

        String edit_state = current.OderState;
        if (edit_state.equalsIgnoreCase("is_checked")){
            myHolder.item_box.setBackgroundColor(Color.parseColor("#ffc870"));
            myHolder.check_order_status.setChecked(true);

        }
        else{
            myHolder.item_box.setBackgroundResource(R.drawable.border);
            myHolder.check_order_status.setChecked(false);
        }


        myHolder.check_order_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String check_stt_state = null;
                String edit_state = current.OderState;


                if (edit_state.equalsIgnoreCase("is_checked")){
                    check_stt_state = "is_not_checked";
                    //myHolder.item_box.setBackgroundColor(Color.parseColor("#ffc870"));

                }
                else{
                    check_stt_state = "is_checked";
                    //myHolder.item_box.setBackgroundResource(R.drawable.border);
                }

                String order_id = current.OrderID;
                String item_number = current.OrderNum;


                CheckState (check_stt_state,order_id,item_number);


            }
        });


    }
    // return total item from List
    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView txtOname;
        TextView txtOunit;
        TextView txtOqty;
        TextView txtOtotal;
        TextView txtOnum;
        CheckBox check_order_status;
        RelativeLayout item_box;


        // create constructor to get widget reference
        public MyHolder(View itemViewa) {
            super(itemViewa);
            item_box = (RelativeLayout) itemViewa.findViewById(R.id.item_box);
            check_order_status = (CheckBox)itemViewa.findViewById(R.id.check_order_status);
            txtOname = (TextView) itemViewa.findViewById(R.id.txtoname);
            txtOunit = (TextView) itemViewa.findViewById(R.id.txtounit);
            txtOqty = (TextView) itemViewa.findViewById(R.id.txtoqty);
            txtOtotal = (TextView) itemViewa.findViewById(R.id.txtototal);
            txtOnum = (TextView) itemViewa.findViewById(R.id.txtonumber);
            itemViewa.setOnClickListener(this);
        }

        // Click event for all items
        @Override
        public void onClick(View v) {


            String item_number = txtOnum.getText().toString();
            String item_name = txtOname.getText().toString();
            String item_unit = txtOunit.getText().toString();
            String item_quantity = txtOqty.getText().toString();

            Intent intent= new Intent(context, OrderItem.class);
            intent.putExtra("item_number", item_number);
            intent.putExtra("item_name", item_name);
            intent.putExtra("item_unit", item_unit);
            intent.putExtra("item_quantity", item_quantity);
            context.startActivity(intent);


        }
    }



    public void  CheckState(final String check_state_str,final String order_id,final String item_number) {


        RequestQueue requestQueuea = Volley.newRequestQueue(context);
        StringRequest stringRequesta = new StringRequest(Request.Method.POST, EndPoints.ORDER_TICK, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                progressDialog.dismiss();

                //Toast.makeText(context, "" + response, Toast.LENGTH_SHORT).show();
                ( (com.findeet.veegloapps.nldmanagement.picker.Order)context).ReloadItems();
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i("Hitesh", "" + error);
                Toast.makeText(context, "Error ! Please check your network connection"+error, Toast.LENGTH_SHORT).show();

            }
        }) {

            // seorderid, secustname, secustaddr, sesalesrep
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> stringMapa = new HashMap<>();
                SharedPreferences shared = context.getSharedPreferences("Mypref", Context.MODE_PRIVATE);
                String repname = shared.getString("user_name", "");
                stringMapa.put("user_name", repname);
                stringMapa.put("check_state", check_state_str);
                stringMapa.put("order_id", order_id);
                stringMapa.put("item_number", item_number);
                return stringMapa;
            }

        };

        requestQueuea.add(stringRequesta);


    }




}
