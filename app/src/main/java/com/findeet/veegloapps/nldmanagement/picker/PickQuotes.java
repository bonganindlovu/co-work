package com.findeet.veegloapps.nldmanagement.picker;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.findeet.veegloapps.nldmanagement.EndPoints;
import com.findeet.veegloapps.nldmanagement.R;
import com.findeet.veegloapps.nldmanagement.Utils.VolleySingleton;
import com.findeet.veegloapps.nldmanagement.picker.adapter.AdapterOpenO;
import com.findeet.veegloapps.nldmanagement.datas.DataOpen;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PickQuotes extends AppCompatActivity {
    android.support.v7.widget.Toolbar pick_tool;
    RecyclerView recycleViewChats;
    AdapterOpenO mAdapter;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick_quotes);
        pick_tool = (android.support.v7.widget.Toolbar)findViewById(R.id.toolbar_pick_quote);
        setSupportActionBar(pick_tool);
        getSupportActionBar().setTitle("Pick Quotes");
        recycleViewChats = (RecyclerView)findViewById(R.id.pick_quote_Recycle);
        SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
        String repname =shared.getString("user_name", "");
        PickQuote(repname);
    }
    public void PickQuote(final String user_name){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, EndPoints.PICK_QUOTES,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String result) {
                        progressDialog.dismiss();
                        //Toast.makeText(PickQuotes.this, ""+result, Toast.LENGTH_SHORT).show();
                        try {
                            JSONArray jArray = new JSONArray(result);
                            List<DataOpen> data = new ArrayList<>();
                            for (int i = 0; i < jArray.length(); i++) {
                                DataOpen proData = new DataOpen();
                                JSONObject json_data = jArray.getJSONObject(i);
                                proData.order_id = json_data.getString("id");
                                proData.order_quote_number = json_data.getString("quote_number");
                                proData.order_cust_name = json_data.getString("customer_number_n_name");
                                proData.order_cust_addr = json_data.getString("customer_addr");
                                proData.order_date = json_data.getString("date_of_sale");
                                proData.order_block = json_data.getString("order_block");
                                proData.order_total = json_data.getString("order_total");
                                data.add(proData);

                            }

                            mAdapter = new AdapterOpenO(PickQuotes.this,data);
                            recycleViewChats.setAdapter(mAdapter);
                            recycleViewChats.setLayoutManager(new LinearLayoutManager(PickQuotes.this));
                            mAdapter.notifyDataSetChanged();


                        } catch (JSONException e) {
                            e.printStackTrace();

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(PickQuotes.this, ""+error, Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("user_name", user_name);
                return params;
            }
        };
        VolleySingleton.getInstance(PickQuotes.this).addToRequestQueue(stringRequest);
        progressDialog = new ProgressDialog(PickQuotes.this);
        progressDialog.setMessage("Fetching orders ...");
//        progressDialog.setCancelable(false);
        progressDialog.show();

        progressDialog.setCancelable(true);

    }

}
