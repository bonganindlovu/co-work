package com.findeet.veegloapps.nldmanagement;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.findeet.veegloapps.nldmanagement.Utils.NetworkStateChecker;
import com.findeet.veegloapps.nldmanagement.Utils.SQLiteHelper;
import com.findeet.veegloapps.nldmanagement.Utils.SQLiteHelperb;
import com.findeet.veegloapps.nldmanagement.Utils.SalesOrderItemsSQL;
import com.findeet.veegloapps.nldmanagement.adapters.AdapterOrder;
import com.findeet.veegloapps.nldmanagement.adapters.AdapterProducts;
import com.findeet.veegloapps.nldmanagement.datas.DataOrder;
import com.google.zxing.integration.android.IntentIntegrator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
public class Order extends AppCompatActivity implements View.OnClickListener,GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {
    TextView cust_name,shop_addr,sales_person,save_quote;
    ProgressBar loadprobar,loaditem;
    LinearLayout notdiv;

    private Location location;
    private GoogleApiClient googleApiClient;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private LocationRequest locationRequest;
    private static final long UPDATE_INTERVAL = 5000, FASTEST_INTERVAL = 5000; // = 5 seconds
    // lists for permissions
    private ArrayList<String> permissionsToRequest;
    private ArrayList<String> permissionsRejected = new ArrayList<>();
    private ArrayList<String> permissions = new ArrayList<>();
    // integer for permissions results request
    private static final int ALL_PERMISSIONS_RESULT = 1011;


    // CONNECTION_TIMEOUT and READ_TIMEOUT are in milliseconds
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 15000;
    private RecyclerView mycustRecycleViewa,zmycustRecycleViewa;
    private AdapterProducts mAdaptera;
    private AdapterOrder zmAdaptera;
    RelativeLayout pop;
    EditText editText;
    TextView order_note;
    String url_save = "http://nextld.co.za/salesadmin/app/save_order.php";
    String url_note = "http://nextld.co.za/salesadmin/app/select_note.php";
    SQLiteDatabase sqLiteDatabase;
    ArrayList<String> p_name_Array,p_id_NAME_Array,p_unit_Array;
    private BroadcastReceiver broadcastReceiver;
    private static final int MY_CAMERA_REQUEST_CODE = 100;
    ArrayList<String> item_name_Array,item_unit_Array,item_quantity_Array,item_total_Array,item_numberArray,item_synce_state_Array;


    SQLiteHelperb sqLiteHelperb;
    Cursor cursor,cursorb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        sqLiteHelperb = new SQLiteHelperb(this);
        registerReceiver(new SalesOrderItemsSQL(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        //Make call to AsyncTask


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA},
                        MY_CAMERA_REQUEST_CODE);
            }
        }
        SQLiteDataBaseBuild();
        p_name_Array = new ArrayList<String>();
        p_id_NAME_Array = new ArrayList<String>();
        p_unit_Array = new ArrayList<String>();
//-----------------------------------------------------------------------
        item_name_Array = new ArrayList<String>();
        item_unit_Array = new ArrayList<String>();
        item_quantity_Array = new ArrayList<String>();
        item_total_Array = new ArrayList<String>();
        item_numberArray = new ArrayList<String>();
        item_synce_state_Array= new ArrayList<String>();
//=========================================================================
        notdiv =(LinearLayout) findViewById(R.id.note) ;
        order_note =(TextView)findViewById(R.id.thenotetxtx);
        editText = (EditText) findViewById(R.id.serach_txt);
        cust_name = (TextView) findViewById(R.id.shop_name);
        shop_addr = (TextView) findViewById(R.id.shop_addr);
        sales_person = (TextView) findViewById(R.id.sales_person);
        loadprobar = (ProgressBar) findViewById(R.id.load_re_items);
        loaditem = (ProgressBar) findViewById(R.id.load_order);
        save_quote = (TextView) findViewById(R.id.save_quote);

        findViewById(R.id.scan_btn).setOnClickListener(this);
//==========================================================================
        SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
        final String repname = shared.getString("user_name", "");

        SharedPreferences ordershared = getSharedPreferences("Order", Context.MODE_PRIVATE);
        final String order_id = ordershared.getString("order_id", "");
        String cust_nu = ordershared.getString("cust_number", "");
        String cust_na = ordershared.getString("cust_name", "");
        final String cust_addr = ordershared.getString("cust_addr", "");
        String seorderid = order_id;
        showNote(seorderid);

        final String full_shop_name = cust_nu + " " + cust_na;
        String full_addr = cust_addr;
        String full_repname = "Sales Person: " + repname;

        cust_name.setText(full_shop_name);
        shop_addr.setText(full_addr);
        sales_person.setText(full_repname);
        ShowSQLiteDBdata() ;
        //new AsyncFetchb().execute();
        ProductsSQLite();
        //new AsyncFetch().execute();

        //the broadcast receiver to update sync status
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                //loading the names again
                ProductsSQLite();
            }
        };

        //registering the broadcast receiver to update sync status
        registerReceiver(broadcastReceiver, new IntentFilter(EndPoints.ITEM_SAVED_BROADCAST));

        save_quote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //, return_order VARCHAR(50)

                SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
                final String repname = shared.getString("user_name", "");

                if (repname.equalsIgnoreCase("james")) {

                    LayoutInflater layoutInflaterAndroid = LayoutInflater.from(Order.this);
                    View mView = layoutInflaterAndroid.inflate(R.layout.back_order_dialog, null);
                    AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(Order.this);
                    alertDialogBuilderUserInput.setView(mView);

                    alertDialogBuilderUserInput
                            .setCancelable(true)
                            .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogBox, int id) {
                                    //delete the order
                                    String seorderid = order_id;
                                    String secustname = full_shop_name;
                                    String secustaddr = cust_addr;
                                    String sesalesrep = repname;
                                    SAVEQUOTE(seorderid, secustname, secustaddr, sesalesrep, "yes_return");
                                    dialogBox.cancel();

                                }
                            })

                            .setNegativeButton("NO",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogBox, int id) {
                                            dialogBox.cancel();
                                            String seorderid = order_id;
                                            String secustname = full_shop_name;
                                            String secustaddr = cust_addr;
                                            String sesalesrep = repname;
                                            SAVEQUOTE(seorderid, secustname, secustaddr, sesalesrep, "");
                                            finish();

                                        }
                                    });


                    AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
                    alertDialogAndroid.show();

                }
                else{
                    String seorderid = order_id;
                    String secustname = full_shop_name;
                    String secustaddr = cust_addr;
                    String sesalesrep = repname;
                    SAVEQUOTE(seorderid, secustname, secustaddr, sesalesrep, "");
                }


            }
        });
        // Calling EditText addTextChangedListener method which controls the EditText type sequence.
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                //Updating Array Adapter ListView after typing inside EditText.
                String e = editText.getText().toString();
                ShowSQLiteDBdata() ;


                //new AsyncFetchSearch().execute();



            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                ShowSQLiteDBdata() ;
                //new AsyncFetchSearch().execute();
            }

            @Override
            public void afterTextChanged(Editable editable) {

                //new AsyncFetchSearch().execute();

            }
        });

        // we add permissions we need to request location of the users
        permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);

        permissionsToRequest = permissionsToRequest(permissions);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (permissionsToRequest.size() > 0) {
                requestPermissions(permissionsToRequest.toArray(
                        new String[permissionsToRequest.size()]), ALL_PERMISSIONS_RESULT);
            }
        }

        // we build google api client
        googleApiClient = new GoogleApiClient.Builder(this).
                addApi(LocationServices.API).
                addConnectionCallbacks(this).
                addOnConnectionFailedListener(this).build();
    }

    private ArrayList<String> permissionsToRequest(ArrayList<String> wantedPermissions) {
        ArrayList<String> result = new ArrayList<>();

        for (String perm : wantedPermissions) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
        }

        return true;
    }


    public void hidePop(View view) {
        pop = (RelativeLayout)findViewById(R.id.add_items_pop_up);
        pop.setVisibility(View.INVISIBLE);

    }

    public void showPop(View view) {
        pop = (RelativeLayout)findViewById(R.id.add_items_pop_up);
        pop.setVisibility(View.VISIBLE);
    }
    public void hideThePop() {
        pop = (RelativeLayout)findViewById(R.id.add_items_pop_up);
        pop.setVisibility(View.INVISIBLE);
    }

        @Override
    public void onBackPressed()
    {

        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(Order.this);
        View mView = layoutInflaterAndroid.inflate(R.layout.back_dialog, null);
        AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(Order.this);
        alertDialogBuilderUserInput.setView(mView);

        alertDialogBuilderUserInput
                .setCancelable(true)
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogBox, int id) {
                        //delete the order

                        dialogBox.cancel();
                        SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
                        final String repname = shared.getString("user_name", "");
                        SharedPreferences ordershared = getSharedPreferences("Order", Context.MODE_PRIVATE);
                        final String order_id = ordershared.getString("order_id", "");
                        String cust_nu = ordershared.getString("cust_number", "");
                        String cust_na = ordershared.getString("cust_name", "");
                        final String cust_addr = ordershared.getString("cust_addr", "");


                        final String full_shop_name = cust_nu + " " + cust_na;
                        String full_addr = cust_addr;
                        String full_repname = "Sales Person: " + repname;
                        String seorderid = order_id;
                        String secustname = full_shop_name;
                        String secustaddr = cust_addr;
                        String sesalesrep = repname;


                        SAVEQUOTE(seorderid, secustname, secustaddr, sesalesrep,"");
                    }
                })

                .setNegativeButton("NO",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogBox, int id) {
                                dialogBox.cancel();
                                Intent intent = new Intent(Order.this, Orders.class);
                                startActivity(intent);
                                finish();

                            }
                        });


        AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
        alertDialogAndroid.show();


    }
    private void checkLocationPermission() {
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                new android.app.AlertDialog.Builder(this)
                        .setTitle("give permission")
                        .setMessage("give permission message")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                ActivityCompat.requestPermissions(Order.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                            }
                        })
                        .create()
                        .show();
            }
            else{
                ActivityCompat.requestPermissions(Order.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            }
        }
    }





    public void goback(View view) {
        SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
        final String repname = shared.getString("user_name", "");
        SharedPreferences ordershared = getSharedPreferences("Order", Context.MODE_PRIVATE);
        final String order_id = ordershared.getString("order_id", "");
        String cust_nu = ordershared.getString("cust_number", "");
        String cust_na = ordershared.getString("cust_name", "");
        final String cust_addr = ordershared.getString("cust_addr", "");


        final String full_shop_name = cust_nu + " " + cust_na;
        String full_addr = cust_addr;
        String full_repname = "Sales Person: " + repname;
        String seorderid = order_id;
        String secustname = full_shop_name;
        String secustaddr = cust_addr;
        String sesalesrep = repname;

//        mLocationRequest = new LocationRequest();
//        mLocationRequest.setInterval(1000);
//        mLocationRequest.setFastestInterval(1000);
//        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
//
//        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
//            if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
//
//            }else{
//                checkLocationPermission();
//            }
//        }
//
//        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());


        SAVEQUOTE(seorderid, secustname, secustaddr, sesalesrep,"");
    }

    public void addNote(View view) {
        SharedPreferences ordershared = getSharedPreferences("Order", Context.MODE_PRIVATE);
        final String order_id = ordershared.getString("order_id", "");
        String order_note_txt = order_note.getText().toString();

        Intent intent = new Intent(Order.this, Note.class);
        intent.putExtra("order_id",order_id );
        intent.putExtra("note_text", order_note_txt);
        startActivity(intent);
    }


    private void ProductsSQLite() {

        sqLiteDatabase = sqLiteHelperb.getWritableDatabase();
        // List<DataCustomers> data=new ArrayList<>();

        SharedPreferences ordershared = getSharedPreferences("Order", Context.MODE_PRIVATE);
        final String order_id = ordershared.getString("order_id", "");

        cursor = sqLiteDatabase.rawQuery("SELECT * FROM sales_order_items WHERE order_id = '"+order_id+"'  ", null);
        item_name_Array.clear();
        item_unit_Array.clear();
        item_quantity_Array.clear();
        item_total_Array.clear();
        item_numberArray.clear();
        item_synce_state_Array.clear();

        if (cursor.moveToFirst()) {
            do {
                item_name_Array.add(cursor.getString(cursor.getColumnIndex("item_name")));
                item_unit_Array.add(cursor.getString(cursor.getColumnIndex("item_unit")));
                item_quantity_Array.add(cursor.getString(cursor.getColumnIndex("item_quantity")));
                item_total_Array.add(cursor.getString(cursor.getColumnIndex("item_total")));
                item_numberArray.add(cursor.getString(cursor.getColumnIndex("item_number")));
                item_synce_state_Array.add(cursor.getString(cursor.getColumnIndex("sync_state")));
            }

            while (cursor.moveToNext());
        }

        // Setup and Handover data to recyclerview
        zmycustRecycleViewa = (RecyclerView) findViewById(R.id.list_order);
        zmAdaptera = new AdapterOrder(Order.this, item_name_Array,item_unit_Array,item_quantity_Array,item_total_Array,item_numberArray,item_synce_state_Array);
        zmycustRecycleViewa.setAdapter(zmAdaptera);
        zmycustRecycleViewa.setLayoutManager(new LinearLayoutManager(Order.this));


        cursor.close();
    }

    private void ShowSQLiteDBdata() {

        sqLiteDatabase = sqLiteHelperb.getWritableDatabase();
        // List<DataCustomers> data=new ArrayList<>();

        String value = "%" + editText.getText().toString() + "%";

        cursor = sqLiteDatabase.rawQuery("SELECT * FROM "+ SQLiteHelperb.TABLE_P_NAME+" WHERE "+SQLiteHelperb.Table_Column_1_Subject_Pname+" LIKE '"+value+"'  ", null);
        p_name_Array.clear();
        p_id_NAME_Array.clear();
        p_unit_Array.clear();

        if (cursor.moveToFirst()) {
            do {

                p_id_NAME_Array.add(cursor.getString(cursor.getColumnIndex(SQLiteHelperb.Table_Column_1_Subject_Pid)));

                p_unit_Array.add(cursor.getString(cursor.getColumnIndex(SQLiteHelperb.Table_Column_1_Subject_Punit)));

                p_name_Array.add(cursor.getString(cursor.getColumnIndex(SQLiteHelperb.Table_Column_1_Subject_Pname)));



            }

            while (cursor.moveToNext());
        }

        // Setup and Handover data to recyclerview
        mycustRecycleViewa = (RecyclerView) findViewById(R.id.product_list);
        mAdaptera = new AdapterProducts(Order.this, p_id_NAME_Array,p_name_Array,p_unit_Array);
        mycustRecycleViewa.setAdapter(mAdaptera);
        mycustRecycleViewa.setLayoutManager(new LinearLayoutManager(Order.this));


        cursor.close();
    }


    public void SAVEQUOTE(final String seorderid, final String secustname, final String secustaddr, final String sesalesrep, final String return_order) {
        sqLiteDatabase = sqLiteHelperb.getWritableDatabase();
        cursor = sqLiteDatabase.rawQuery("SELECT * FROM sales_order WHERE order_id = '"+seorderid+"'    ", null);
        if (cursor.moveToFirst()) {
            //do
            //function to check note
            String note =order_note.getText().toString();
           String myorder_note ="";

           if(note.equalsIgnoreCase("Click here to add note...") || note==null) {
                myorder_note ="";
            } else {
              myorder_note =order_note.getText().toString();
            }

            Toast.makeText(Order.this, "Order Updated", Toast.LENGTH_LONG).show();
           sqLiteDatabase.execSQL("UPDATE sales_order SET sync_state ='not_synced', order_status='open', note='"+myorder_note+"' WHERE order_id ='"+seorderid+"'");
            Intent intent = new Intent(Order.this, Orders.class);
            startActivity(intent);
            finish();
        }
        else {
            String note =order_note.getText().toString();
            String myorder_note ="";

            if(note.equalsIgnoreCase("Click here to add note...") || note==null) {
                myorder_note ="";
            } else {
                myorder_note =order_note.getText().toString();
            }
            // add item to sqlite
            String date = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.getDefault()).format(new Date());

      Toast.makeText(Order.this, "Order Added", Toast.LENGTH_LONG).show();
           String SQLiteDataBaseQueryHolder = "INSERT INTO sales_order (order_id,cust_name,cust_addr,date_of_sale,order_status,sales_rep,note,sync_state,return_order) VALUES('" + seorderid + "','" + secustname + "', '" + secustaddr + "','" + date + "', 'open', '" + sesalesrep + "', '" + myorder_note + "', 'not_synced', '" + return_order + "');";
           sqLiteDatabase.execSQL(SQLiteDataBaseQueryHolder);

            ////function to check note and update


            Intent intent = new Intent(Order.this, Orders.class);
            startActivity(intent);
            finish();
        }

        }
    @Override
    protected void onResume() {
        registerReceiver(new SalesOrderItemsSQL(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        ShowSQLiteDBdata() ;
        ProductsSQLite();
        super.onResume();
        if (!checkPlayServices()) {
            //        SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = shared.edit();
//        editor.putString("LatLong", String.valueOf(latLng));
//        editor.apply();
            Toast.makeText(this, "You need to install Google Play Services to use the App properly", Toast.LENGTH_LONG).show();


        }
    }

    public void SQLiteDataBaseBuild() {
        sqLiteDatabase = openOrCreateDatabase(SQLiteHelper.DATABASE_NAME, Context.MODE_PRIVATE, null);
        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS sales_order (order_id VARCHAR(200),cust_name VARCHAR(100), cust_addr TEXT, time_sent INT(100), date_of_sale VARCHAR(100),order_status VARCHAR(50), sales_rep VARCHAR(100),order_total DOUBLE(13,2),note VARCHAR(50), sync_state VARCHAR(50), return_order VARCHAR(50))");
        //sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS products (id INTEGER, cust_number TEXT, cust_name TEXT, addr_line_one TEXT)");

    }
    public void showNote(final  String seorderid){
        sqLiteDatabase = sqLiteHelperb.getWritableDatabase();
        cursor = sqLiteDatabase.rawQuery("SELECT * FROM routes_notes WHERE order_id = '"+seorderid+"'  ", null);
        if (cursor.moveToFirst()) {
            String order_id= cursor.getString(cursor.getColumnIndex("order_id"));
            String note_text= cursor.getString(cursor.getColumnIndex("order_note"));

            order_note.setText(note_text);

//            if(note_text.equalsIgnoreCase("Click here to add note...") || note_text==null) {
//                my_note.clearFocus();
//            } else {
//                my_note.setText(note_text);
//            }

        }
        else{
            order_note.setText("Click here to add note...");
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.scan_btn) {
            IntentIntegrator scanIntegrator = new IntentIntegrator(this);
            scanIntegrator.initiateScan();
        }

    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
//retrieve scan result
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanningResult != null) {

//we have a result
            String scanContent = scanningResult.getContents();
            LookForBarCode(scanContent);
        }
        else{
            Toast toast = Toast.makeText(getApplicationContext(),
                    "No scan data received!", Toast.LENGTH_SHORT);
            toast.show();
        }

    }
    public void LookForBarCode(final String barcode_txt){
        sqLiteDatabase = sqLiteHelperb.getWritableDatabase();

        cursorb = sqLiteDatabase.rawQuery("SELECT * FROM products WHERE barcode ='"+barcode_txt+"' ", null);

        if (cursorb.moveToFirst()) {
            hideThePop();
            //set that the product is found
            String product_name = cursorb.getString(cursorb.getColumnIndex("pname"));
            String product_id = cursorb.getString(cursorb.getColumnIndex("id"));
            String product_unit = cursorb.getString(cursorb.getColumnIndex("punit"));


            Intent intent= new Intent(Order.this, OrderItem.class);
            intent.putExtra("item_number", product_id);
            intent.putExtra("item_name", product_name);
            intent.putExtra("item_unit", product_unit);
            intent.putExtra("item_quantity", "1");
            startActivity(intent);


        }
        else{
            //update sqlite, and mtsql
            //call the list of the products to chose from
            Toast.makeText(Order.this, "barcode "+barcode_txt+" was not found", Toast.LENGTH_SHORT).show();

        }


        cursor.close();
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (googleApiClient != null) {
            googleApiClient.connect();
        }
    }


    @Override
    protected void onPause() {
        super.onPause();

        // stop location updates
        if (googleApiClient != null  &&  googleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
            googleApiClient.disconnect();
        }
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);

        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST);
            } else {
                finish();
            }

            return false;
        }
        return true;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                &&  ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        // Permissions ok, we get last location
        location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);

        if (location != null) {
            //locationTv.setText("Latitude : " + location.getLatitude() + "\nLongitude : " + location.getLongitude());

            String latLng = location.getLatitude()+","+location.getLongitude();
            SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = shared.edit();
            editor.putString("LatLong", latLng);
            editor.apply();
            //Toast.makeText(this, "Your location is "+latLng, Toast.LENGTH_LONG).show();

        }

        startLocationUpdates();
    }

    private void startLocationUpdates() {

        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(UPDATE_INTERVAL);
        locationRequest.setFastestInterval(FASTEST_INTERVAL);

        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                &&  ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "You need to enable permissions to display location !", Toast.LENGTH_SHORT).show();
        }

        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            //locationTv.setText("Latitude : " + location.getLatitude() + "\nLongitude : " + location.getLongitude());
            String latLng = location.getLatitude()+","+location.getLongitude();
            SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = shared.edit();
            editor.putString("LatLong", latLng);
            editor.apply();
            //Toast.makeText(this, "Your location is "+latLng, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch(requestCode) {
            case ALL_PERMISSIONS_RESULT:
                for (String perm : permissionsToRequest) {
                    if (!hasPermission(perm)) {
                        permissionsRejected.add(perm);
                    }
                }

                if (permissionsRejected.size() > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) {
                            new AlertDialog.Builder(Order.this).
                                    setMessage("These permissions are mandatory to get your location. You need to allow them.").
                                    setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(permissionsRejected.
                                                        toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    }).setNegativeButton("Cancel", null).create().show();

                            return;
                        }
                    }
                } else {
                    if (googleApiClient != null) {
                        googleApiClient.connect();
                    }
                }

                break;
        }
    }
}
