package com.findeet.veegloapps.nldmanagement.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.findeet.veegloapps.nldmanagement.Customers;
import com.findeet.veegloapps.nldmanagement.EndPoints;
import com.findeet.veegloapps.nldmanagement.R;
import com.findeet.veegloapps.nldmanagement.Utils.SQLiteHelperb;
import com.findeet.veegloapps.nldmanagement.adapters.AdapterQuoted;
import com.findeet.veegloapps.nldmanagement.adapters.AdapterQuotes;

import java.util.ArrayList;


/**
 * Created by DEBONGZ on 4/18/2018.
 */

public class QuotedFragment extends Fragment {
    public QuotedFragment() {
        // Required empty public constructor
    }

    //defining views
    FloatingActionButton CreateOrder;
    // CONNECTION_TIMEOUT and READ_TIMEOUT are in milliseconds
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 15000;
    private RecyclerView list_q,zmycustRecycleViewa;
    //private AdapterProducts mAdaptera;
    private AdapterQuoted zmAdaptera;
    SwipeRefreshLayout myrefresh;
    SQLiteHelperb sqLiteHelperb;
    SQLiteDatabase sqLiteDatabase;
    Cursor cursor;
    private BroadcastReceiver broadcastReceiver;


    ArrayList<String> cust_number_Array,cust_addr_Array,order_total_Array,date_of_sale_Array,order_id_Array,order_synce_status_Array;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.frag_quoted, container, false);
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        cust_number_Array = new ArrayList<String>();
        cust_addr_Array = new ArrayList<String>();
        order_total_Array = new ArrayList<String>();
        date_of_sale_Array = new ArrayList<String>();
        order_id_Array = new ArrayList<String>();
        order_synce_status_Array = new ArrayList<String>();
        CreateOrder = (FloatingActionButton) view.findViewById(R.id.add_new_order);
        myrefresh = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout) ;
        sqLiteHelperb = new SQLiteHelperb(getContext());
        ProductsSQLite();
        CreateOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), Customers.class);
                startActivity(intent);
            }
        });



        myrefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                ProductsSQLite();
            }
        });

        //the broadcast receiver to update sync status
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                //loading the names again
                ProductsSQLite();
            }
        };

        //registering the broadcast receiver to update sync status
        getContext().registerReceiver(broadcastReceiver, new IntentFilter(EndPoints.ORDER_SAVED_BROADCAST+"quoted"));


    }


    private void ProductsSQLite() {

        sqLiteDatabase = sqLiteHelperb.getWritableDatabase();
        SharedPreferences shared = getActivity().getSharedPreferences("Mypref", Context.MODE_PRIVATE);
        String repname =shared.getString("user_name", "");

        cursor = sqLiteDatabase.rawQuery("SELECT * FROM sales_order WHERE sales_rep = '"+repname+"' AND order_status ='quoted' ORDER BY time_sent DESC ", null);
        cust_number_Array.clear();
        cust_addr_Array.clear();
        order_total_Array.clear();
        date_of_sale_Array.clear();
        order_id_Array.clear();
        order_synce_status_Array.clear();
        myrefresh.setRefreshing(false);
        if (cursor.moveToFirst()) {
            do {
                //  DOUBLE(13,2),note VARCHAR(50),
                cust_number_Array.add(cursor.getString(cursor.getColumnIndex("cust_name")));
                cust_addr_Array.add(cursor.getString(cursor.getColumnIndex("cust_addr")));
                order_total_Array.add(cursor.getString(cursor.getColumnIndex("order_total")));
                date_of_sale_Array.add(cursor.getString(cursor.getColumnIndex("quote_number")));
                order_id_Array.add(cursor.getString(cursor.getColumnIndex("order_id")));
                order_synce_status_Array.add(cursor.getString(cursor.getColumnIndex("sync_state")));
            }

            while (cursor.moveToNext());
        }

        // Setup and Handover data to recyclerview
        zmycustRecycleViewa = (RecyclerView) getView().findViewById(R.id.list_quotes);
        zmAdaptera = new AdapterQuoted(getActivity(), cust_number_Array,cust_addr_Array,order_total_Array,date_of_sale_Array,order_id_Array,order_synce_status_Array);
        zmycustRecycleViewa.setAdapter(zmAdaptera);
        zmycustRecycleViewa.setLayoutManager(new LinearLayoutManager(getActivity()));

        cursor.close();
    }

    @Override
    public void onResume() {
        getContext().registerReceiver(broadcastReceiver, new IntentFilter(EndPoints.ORDER_SAVED_BROADCAST+"quoted"));
        ProductsSQLite();
        super.onResume();
    }
}
