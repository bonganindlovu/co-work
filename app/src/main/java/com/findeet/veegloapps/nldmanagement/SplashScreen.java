package com.findeet.veegloapps.nldmanagement;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.chootdev.csnackbar.Align;
import com.chootdev.csnackbar.Duration;
import com.chootdev.csnackbar.Snackbar;
import com.chootdev.csnackbar.Type;
import com.findeet.veegloapps.nldmanagement.firebase.SharedPrefManager;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.HashMap;
import java.util.Map;

import static com.findeet.veegloapps.nldmanagement.Utils.SyncRoutes.updateToken;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        FirebaseApp.initializeApp(this);
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( SplashScreen.this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String newToken = instanceIdResult.getToken();
                Log.e("newToken",newToken);
                storeToken(newToken);

            }
        });

        Thread thread = new Thread() {

            @Override
            public void run() {
                try {
                    sleep(3000);
                    SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
                    String val = shared.getString("pass", "");
                    if (val.length() == 0) {


                        final String token = SharedPrefManager.getInstance(SplashScreen.this).getDeviceToken();
                        if (token == null || token.isEmpty()) {
                            getT();
                            Intent intent = new Intent(SplashScreen.this, SplashScreen.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Intent intent = new Intent(SplashScreen.this, Login.class);
                            startActivity(intent);
                            finish();
                       }


//For older devices that cant get token
//                        if(token == null ){
//
//                            storeToken(null);
//                            Intent intent = new Intent(SplashScreen.this, Login.class);
//                            startActivity(intent);
//                            finish();
//                        }
//                        else if (token.isEmpty()) {
//                            getT();
//                            Intent intent = new Intent(SplashScreen.this, SplashScreen.class);
//                            startActivity(intent);
//                            finish();
//                        }
//
//                        else {
//                            Intent intent = new Intent(SplashScreen.this, Login.class);
//                            startActivity(intent);
//                            finish();
//                        }




                    } else {
                        SharedPreferences shared2 = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
                        String fnamename = shared2.getString("user_name", "");
                        String token = SharedPrefManager.getInstance(SplashScreen.this).getDeviceToken();

                        updateToken(fnamename,token);

                        SharedPreferences mshared = getSharedPreferences("SetupPref", Context.MODE_PRIVATE);
                        String loaded = mshared.getString("loaded", "");
                        if (loaded.length() == 0) {



                            Intent intent = new Intent(SplashScreen.this, SaveSqlCustomers.class);
                            startActivity(intent);
                            finish();


                        }
                        else {

                            Intent intent = new Intent(SplashScreen.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    }


                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        };

        thread.start();
    }

    public void storeToken(String token) {
//        we will save the token in sharedpreferences later
//        saving the token on shared preferences
        SharedPrefManager.getInstance(getApplicationContext()).saveDeviceToken(token);
    }
    public void getT(){
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( SplashScreen.this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String newToken = instanceIdResult.getToken();
                Log.e("newToken",newToken);
                storeToken(newToken);

            }
        });
    }
    public void updateToken(final String user_name, final String token) {

        RequestQueue requestQueueone = Volley.newRequestQueue(SplashScreen.this);
        StringRequest stringRequestone = new StringRequest(Request.Method.POST, EndPoints.UPDATE_TOKEN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

//                Snackbar.with(SplashScreen.this,null)
//                        .type(Type.SUCCESS)
//                        .message(response)
//                        .duration(Duration.SHORT)
//                        .fillParent(true)
//                        .textAlign(Align.LEFT)
//                        .show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i("Hitesh", "" + error);
//                Snackbar.with(SplashScreen.this,null)
//                        .type(Type.ERROR)
//                        .message("Error! Updating token")
//                        .duration(Duration.SHORT)
//                        .fillParent(true)
//                        .textAlign(Align.LEFT)
//                        .show();
                //Toast.makeText(SplashScreen.this, "" + error, Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> stringMap = new HashMap<>();
                stringMap.put("user_name", user_name);
                stringMap.put("token", token);

                return stringMap;
            }

        };

        requestQueueone.add(stringRequestone);

    }

}
