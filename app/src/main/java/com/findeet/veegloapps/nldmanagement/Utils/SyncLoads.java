package com.findeet.veegloapps.nldmanagement.Utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.findeet.veegloapps.nldmanagement.EndPoints;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by DEBONGZ on 8/22/2018.
 */

public class SyncLoads extends BroadcastReceiver {

    //context and database helper object
    private Context context;
    SQLiteDatabase sqLiteDatabase;
    SQLiteHelper sqLiteHelper;
    Cursor cursor;

    @Override
    public void onReceive(Context context, Intent intent) {

        this.context = context;

        sqLiteHelper = new SQLiteHelper(context);
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        //if there is a network
        if (activeNetwork != null) {

            checkLoads();

        }
    }

    public void checkLoads(){
        sqLiteDatabase = sqLiteHelper.getWritableDatabase();
        cursor = sqLiteDatabase.rawQuery("SELECT * FROM loads WHERE synce_status = 'not_synced'  ", null);
        if (cursor.moveToFirst()) {

            do {
                //  DOUBLE(13,2),note VARCHAR(50),
                String load_id= cursor.getString(cursor.getColumnIndex("load_id"));
                String vehicle_name= cursor.getString(cursor.getColumnIndex("vehicle_name"));
                String drive_name= cursor.getString(cursor.getColumnIndex("drive_name"));
                String load_time= cursor.getString(cursor.getColumnIndex("load_time"));


                sendLoads(load_id,vehicle_name,drive_name,load_time);
            }

            while (cursor.moveToNext());


        }


    }
public LatLng getLatLong(final String onefrom_latlong){

    String twofrom_latlong =onefrom_latlong.replace("lat/lng: (","");
    String from_latlong = twofrom_latlong;

    String[] froml = from_latlong.split(",");
    double fla = Double.parseDouble(froml[0]);
    double flo = Double.parseDouble(froml[1].replace(")",""));
    LatLng theLatLng = new LatLng(fla,flo);
    return theLatLng;

}
public void sendLoads(final String load_id,final String vehicle_name,final String drive_name,final String load_time){

    StringRequest stringRequest = new StringRequest(Request.Method.POST, EndPoints.SEND_LOADS,
            new Response.Listener<String>() {
                @Override
                public void onResponse(String result) {
                    try {
                        JSONArray jArray = new JSONArray(result);

                        for (int i = 0; i < jArray.length(); i++) {
                            JSONObject d = jArray.getJSONObject(i);
                            JSONObject json_data = jArray.getJSONObject(i);
                            String load_id = json_data.getString("load_id");

                            updateSync(load_id);

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }) {
        @Override
        protected Map<String, String> getParams() throws AuthFailureError {
            Map<String, String> params = new HashMap<>();
            params.put("load_id", load_id);
            params.put("vehicle_name", vehicle_name);
            params.put("drive_name", drive_name);
            params.put("load_time", load_time);
            return params;
        }
    };
    VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);

}

    public void updateSync(final String load_id)
    {
        String SQLiteDataBaseQueryHolder = "UPDATE loads SET synce_status ='synced' WHERE load_id='"+load_id+"' ";
        sqLiteDatabase.execSQL(SQLiteDataBaseQueryHolder);
        context.sendBroadcast(new Intent(EndPoints.ORDER_SAVED_BROADCAST+"loads"));

    }
}
