package com.findeet.veegloapps.nldmanagement;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.findeet.veegloapps.nldmanagement.adapters.AdapterDelLoads;
import com.findeet.veegloapps.nldmanagement.adapters.AdapterNot;
import com.findeet.veegloapps.nldmanagement.datas.DataNot;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Notifications extends AppCompatActivity {
    private AdapterNot mAdaptera;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);
        fetchNots();
    }

    public void goback(View view) {
        super.onBackPressed();
    }

    private void fetchNots() {
        RequestQueue requestQueuemom = Volley.newRequestQueue(this);
        SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
        final String fnamename = shared.getString("user_name", "");
        StringRequest stringRequestabc = new StringRequest(Request.Method.GET, EndPoints.URL_FETCH_NOT_THREAD+"?user_name="+fnamename,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String result) {
                        try {

                            //Toast.makeText(Notifications.this, ""+result, Toast.LENGTH_SHORT).show();

                            JSONArray jArray = new JSONArray(result);

                            List<DataNot> data = new ArrayList<>();

                            for (int i = 0; i < jArray.length(); i++) {
                                JSONObject d = jArray.getJSONObject(i);
                                JSONObject json_data = jArray.getJSONObject(i);
                                DataNot proData = new DataNot();
                                proData.not_id = json_data.getString("id");
                                proData.date_sent = json_data.getString("date_sent");
                                proData.time_sent = json_data.getString("time_sent");
                                proData.order_id = json_data.getString("order_id");
                                proData.shop_id = json_data.getString("shop_id");
                                proData.shop_name = json_data.getString("shop_name");
                                proData.rep_name = json_data.getString("rep_name");
                                proData.status = json_data.getString("status");
                                proData.not_type = json_data.getString("type");
                                proData.invoice_amount = json_data.getString("invoice_amount");

                                data.add(proData);

                            }

//

                            RecyclerView myRecycler = (RecyclerView) findViewById(R.id.RecycleNotifications);
                            myRecycler.setLayoutManager(new LinearLayoutManager(Notifications.this));
                            mAdaptera = new AdapterNot(Notifications.this, data);
                            myRecycler.setAdapter(mAdaptera);
                            mAdaptera.notifyDataSetChanged();

                        } catch (JSONException e) {
                            Toast.makeText(Notifications.this, ""+e, Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }

                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //myrefresh.setRefreshing(false);
                        Toast.makeText(Notifications.this, "Loading failed", Toast.LENGTH_SHORT).show();
                    }
                }) {

        };

        requestQueuemom.add(stringRequestabc);
    }
}
