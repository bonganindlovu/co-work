package com.findeet.veegloapps.nldmanagement.adapters;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.findeet.veegloapps.nldmanagement.EndPoints;
import com.findeet.veegloapps.nldmanagement.Paid;
import com.findeet.veegloapps.nldmanagement.R;
import com.findeet.veegloapps.nldmanagement.Utils.SQLiteHelper;
import com.findeet.veegloapps.nldmanagement.Utils.VolleySingleton;
import com.theartofdev.edmodo.cropper.CropImage;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.support.v7.app.AppCompatActivity.RESULT_OK;

/**
 * Created by DEBONGZ on 1/6/2018.
 */

public class AdapterDelLoads extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ArrayList<String> pid;
    ArrayList<String> plist_order;
    ArrayList<String> pshop_adress;
    ArrayList<String> pshop_name;
    ArrayList<String> pshop_lat;
    ArrayList<String> pshop_long;
    ArrayList<String> ppayment_status;
    SQLiteDatabase sqLiteDatabase;
    Bitmap bitmap;
    SQLiteHelper sqLiteHelper;
    Cursor cursor;
    private Context context;
    private LayoutInflater inflater;
    int currentPos=0;
    public AdapterDelLoads(Context context,
                           ArrayList<String> id,
                           ArrayList<String> list_order,
                           ArrayList<String> shop_adress,
                           ArrayList<String> shop_name,
                           ArrayList<String> shop_lat,
                           ArrayList<String> shop_long,
                           ArrayList<String> payment_status){
        this.context=context;
        inflater= LayoutInflater.from(context);
        this.pid = id;
        this.plist_order = list_order;
        this.pshop_adress = shop_adress;
        this.pshop_name = shop_name;
        this.pshop_lat = shop_lat;
        this.pshop_long = shop_long;
        this.ppayment_status = payment_status;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=inflater.inflate(R.layout.container_driver_orders, parent,false);
        MyHolder holder=new MyHolder(view);

        return holder;
    }
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        // Get current position of item in RecyclerView to bind data and assign values from list
        MyHolder myHolder= (MyHolder) holder;
        myHolder.txtlist_order.setText(plist_order.get(position)+".");
        myHolder.txtshop_name.setText(pshop_name.get(position));
        myHolder.txtshop_addr.setText(pshop_adress.get(position));
        String shop_lat = pshop_lat.get(position);
        String shop_long = pshop_long.get(position);
//        String pay_state = ppayment_status.get(position);

//        Toast.makeText(context, "Payment status: "+pay_state, Toast.LENGTH_SHORT).show();
//
//if (pay_state.equalsIgnoreCase(null)){
//
//}
//else {
//    myHolder.txtlist_order.setBackgroundColor(Color.parseColor("#70c050"));
//}

        myHolder.oder_line.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater layoutInflaterAndroid = LayoutInflater.from(context);
                View mView = layoutInflaterAndroid.inflate(R.layout.cust_feed_dialog_box, null);
                AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(context);
                alertDialogBuilderUserInput.setView(mView);
                String shop_name = pshop_name.get(position);
                final TextView h = (TextView) mView.findViewById(R.id.h);
                final TextView payment = (TextView) mView.findViewById(R.id.payment);
                final TextView arrived = (TextView) mView.findViewById(R.id.arrived);
                final TextView depart = (TextView) mView.findViewById(R.id.depart);
                arrived.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String order_id = pid.get(position);
                        String payment_method = "";
                        String payment_amount_text = "";
                        String action ="arrived";
                        Toast.makeText(context, "Updating your action "+action, Toast.LENGTH_SHORT).show();
                        upDatePayment(order_id,payment_method,payment_amount_text, action);

                    }
                });

                depart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String order_id = pid.get(position);
                        String payment_method = "";
                        String payment_amount_text = "";
                        String action ="depart";
                        Toast.makeText(context, "Updating your action "+action, Toast.LENGTH_SHORT).show();
                        upDatePayment(order_id,payment_method,payment_amount_text, action);
                    }
                });

//________________________________________________________________________________________________//
                payment.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String order_id = pid.get(position);
                        Intent intent= new Intent(context, Paid.class);
                        intent.putExtra("order_id", order_id);
                        context.startActivity(intent);

//                        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(context);
//                        View mView = layoutInflaterAndroid.inflate(R.layout.payment_dialog_box, null);
//                        AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(context);
//                        alertDialogBuilderUserInput.setView(mView);
////                        String shop_name = pshop_name.get(position);
////                        final TextView h = (TextView) mView.findViewById(R.id.h);
////                        final TextView payment = (TextView) mView.findViewById(R.id.payment);
//                       final Spinner payment_method = (Spinner)mView.findViewById(R.id.payment_method);
//                       final EditText payment_amount = (EditText)mView.findViewById(R.id.payment_txt) ;
//                        final TextView capture_invoice = (EditText)mView.findViewById(R.id.capture_invoice) ;
//                        final ImageView invoice_image = (ImageView) mView.findViewById(R.id.invoice_image) ;
//                        String order_id = pid.get(position);
//
//                        capture_invoice.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//
//                                    CropImage.activity(null).setGuidelines(CropImageView.Guidelines.ON)
//                                            //.setBorderLineColor(Color.RED)
//                                            //.setScaleType(FIT_CENTER)
//
//                                            .start((Activity) context);
//
//                            }
//                        });
//
//
//
//
//
////                        sqLiteDatabase = sqLiteHelper.getWritableDatabase();
////                        cursor = sqLiteDatabase.rawQuery("SELECT * FROM driver_shops WHERE id ='"+order_id+"' ", null);
////
////
////                        if (cursor.moveToFirst()) {
////
////                            String arrived = cursor.getString(cursor.getColumnIndex("id"));
////
////
////
////                        }
//
//
//
//
//                         alertDialogBuilderUserInput
//                                .setCancelable(true)
//                                .setPositiveButton("Submit", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialogBox, int id) {
//                                        String payment_method_text = String.valueOf(payment_method.getSelectedItem());
//
//                                        if (payment_method_text.equalsIgnoreCase("Select Payment")){
//                                            Toast.makeText(context, "Payment status update failed! \nPlease select Payment!", Toast.LENGTH_SHORT).show();
//                                        }
//
//                                        else {
//                                            String order_id = pid.get(position);
//                                            String payment_method = payment_method_text;
//                                            String payment_amount_text = payment_amount.getText().toString();
//                                            String action ="payment";
//                                            upDatePayment(order_id,payment_method,payment_amount_text, action);
//                                        }
//                      dialogBox.cancel();
//                                    }
//                                });
//
//                        AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
//                        alertDialogAndroid.show();

                    }
                });
                //________________________________________________________________________________//

                h.setText(shop_name);

                alertDialogBuilderUserInput
                        .setCancelable(true);
              AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
                alertDialogAndroid.show();

            }
        });

        //Toast.makeText(context, ""+shop_lat+" "+shop_long, Toast.LENGTH_SHORT).show();
        myHolder.btndirections.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String shop_lat = pshop_lat.get(position);
                String shop_long = pshop_long.get(position);

                //Toast.makeText(context, ""+shop_lat, Toast.LENGTH_SHORT).show();
                Uri gmmIntentUri = Uri.parse("google.navigation:q="+shop_lat+","+shop_long+"&mode=d");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                context.startActivity(mapIntent);

            }
        });

    }
    @Override
    public int getItemCount() {
        return pid.size();
    }
    class MyHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView txtlist_order,txtshop_name,txtshop_addr;
        FloatingActionButton btndirections;
        RelativeLayout oder_line;
        // create constructor to get widget reference
        public MyHolder(View itemViewa) {
            super(itemViewa);
            oder_line = (RelativeLayout)itemViewa.findViewById(R.id.oder_line);
            txtlist_order = (TextView) itemViewa.findViewById(R.id.list_order);
            txtshop_name = (TextView) itemViewa.findViewById(R.id.shop_name);
            txtshop_addr = (TextView) itemViewa.findViewById(R.id.shop_addr);
            btndirections = (FloatingActionButton) itemViewa.findViewById(R.id.directions);
            itemViewa.setOnClickListener(this);
            SQLiteDataBaseBuild();
        }
        // Click event for all items
        @Override
        public void onClick(View v) {


        }
    }
    private void upDatePayment(final String order_id, final String payment_method_txt, final String payment_amount_text, final String action){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, EndPoints.UPDATE_LOADS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String result) {
                        try {
                            JSONArray jArray = new JSONArray(result);
                            Toast.makeText(context, "Payment status updated!", Toast.LENGTH_SHORT).show();
                            for (int i = 0; i < jArray.length(); i++) {
//                                JSONObject d = jArray.getJSONObject(i);
//                                JSONObject json_data = jArray.getJSONObject(i);
                                //String load_id = json_data.getString("load_id");

                                upDatePaymentOn(order_id,payment_method_txt,payment_amount_text);

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, "Payment status will be updated when Internet is reconnected ", Toast.LENGTH_SHORT).show();
                        upDatePaymentOff(order_id,payment_method_txt,payment_amount_text);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("load_id", order_id);
                params.put("payment_method", payment_method_txt);
                params.put("payment_amount", payment_amount_text);
                params.put("action", action);
                return params;
            }
        };
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }
    private void upDatePaymentOn(final String order_id, final String payment_method_txt, final String payment_amount_text){

        sqLiteDatabase.execSQL("UPDATE driver_shops SET sync_status ='synced', payment_status ='"+payment_method_txt+"', amount_paid ='"+payment_amount_text+"' WHERE id ='"+order_id+"' ");
    }
    private void upDatePaymentOff(final String order_id, final String payment_method_txt, final String payment_amount_text){

        sqLiteDatabase.execSQL("UPDATE driver_shops SET sync_status ='not_synced', payment_status ='"+payment_method_txt+"', amount_paid ='"+payment_amount_text+"' WHERE id ='"+order_id+"' ");
    }
    public void SQLiteDataBaseBuild(){

        sqLiteDatabase = context.openOrCreateDatabase(SQLiteHelper.DATABASE_NAME, Context.MODE_PRIVATE, null);

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                Uri uri = result.getUri();

                try {

                    bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);


                } catch (IOException e) {

                    e.printStackTrace();
                }
            }
            else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(context, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }

        }
    }
}
