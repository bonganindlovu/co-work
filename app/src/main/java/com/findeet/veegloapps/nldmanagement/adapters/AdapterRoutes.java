package com.findeet.veegloapps.nldmanagement.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.findeet.veegloapps.nldmanagement.R;
import com.findeet.veegloapps.nldmanagement.RouteShops;

import java.util.ArrayList;

/**
 * Created by DEBONGZ on 1/6/2018.
 */

public class AdapterRoutes extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private LayoutInflater inflater;

    ArrayList<String> oroute_name;
    ArrayList<String> oavr_days;



    ArrayList<String> oitem_synce_state;
    int currentPos=0;

    // create constructor to initialize context and data sent from MainActivity
    //route_name_Array,avr_days_Array);
    public AdapterRoutes(Context context, ArrayList<String> route_name, ArrayList<String> avr_days){
        this.context=context;
        inflater= LayoutInflater.from(context);
        this.oroute_name = route_name;
        this.oavr_days = avr_days;


    }

    // Inflate the layout when ViewHolder created
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=inflater.inflate(R.layout.container_routes, parent,false);
        MyHolder holder=new MyHolder(view);

        return holder;
    }

    // Bind data
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        // Get current position of item in RecyclerView to bind data and assign values from list
        MyHolder myHolder= (MyHolder) holder;
        myHolder.Troute_name.setText(oroute_name.get(position));
        myHolder.Troute_avr.setText(oavr_days.get(position));



    }
    // return total item from List
    @Override
    public int getItemCount() {
        return oroute_name.size();
    }

    class MyHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView Troute_name;
        TextView Troute_avr;
        RelativeLayout Rquote_line;


        // create constructor to get widget reference
        public MyHolder(View itemViewa) {
            super(itemViewa);
            Troute_name = (TextView) itemViewa.findViewById(R.id.route_name);
            Troute_avr = (TextView) itemViewa.findViewById(R.id.route_avr);
            Rquote_line =(RelativeLayout)itemViewa.findViewById(R.id.quote_line);
            itemViewa.setOnClickListener(this);
        }

        // Click event for all items
        @Override
        public void onClick(View v) {



 //go to the new intent with the shop name and adress, and send it to database to create new invoive
            String route_name = Troute_name.getText().toString();
            Intent intent=new Intent(context.getApplicationContext(), RouteShops.class);
            intent.putExtra("route_name", route_name);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.getApplicationContext().startActivity(intent);


        }
    }


}
