package com.findeet.veegloapps.nldmanagement;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.findeet.veegloapps.nldmanagement.Utils.SQLiteHelper;

import java.util.UUID;

public class CreateNewCustomer extends AppCompatActivity {
    SQLiteDatabase sqLiteDatabase;
    EditText cust_name_txt,cust_addr_txt,cust_contact_psn_txt,cust_cell_psn_txt;
    Spinner route_text;
    AppCompatButton btn_save_cust;
    LocationListener listener;
    Location location;
    LocationManager locationManager ;
    boolean GpsStatus = false ;
    Criteria criteria ;
    String Holder;
    final Context c = this;
    private ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_new_customer);
        SQLiteDataBaseBuild();
        cust_name_txt = (EditText)findViewById(R.id.cust_name_txt);
        cust_addr_txt = (EditText)findViewById(R.id.cust_addr_txt);
        cust_contact_psn_txt = (EditText)findViewById(R.id.cust_contact_psn_txt);
        cust_cell_psn_txt = (EditText)findViewById(R.id.cust_cell_psn_txt);
        route_text = (Spinner)findViewById(R.id.route_text);
        btn_save_cust= (AppCompatButton)findViewById(R.id.btn_save_cust);
        locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);


        btn_save_cust.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkDetails();
            }
        });

        criteria = new Criteria();
        Holder = locationManager.getBestProvider(criteria, false);
        GpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        listener =  new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                //Toast.makeText(getActivity(),"Got it!", Toast.LENGTH_LONG).show();
//                textViewLongitude.setText("Longitude:" + location.getLongitude());
//                textViewLatitude.setText("Latitude:" + location.getLatitude());
                String longitude = String.valueOf(location.getLongitude());
                String latitude = String.valueOf(location.getLatitude());
                saveCustToSqlite(longitude,latitude);
                locationManager.removeUpdates(listener);

                LayoutInflater layoutInflaterAndroid = LayoutInflater.from(c);
                View mView = layoutInflaterAndroid.inflate(R.layout.location_dialog_box, null);
                AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(c);
                alertDialogBuilderUserInput.setView(mView);
                AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
                alertDialogAndroid.cancel();


            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };


        if(GpsStatus == true) {
            if (Holder != null) {
                if (ActivityCompat.checkSelfPermission(
                        CreateNewCustomer.this,
                        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        &&
                        ActivityCompat.checkSelfPermission(CreateNewCustomer.this, Manifest.permission.ACCESS_COARSE_LOCATION)
                                != PackageManager.PERMISSION_GRANTED) {

                }
                location = locationManager.getLastKnownLocation(Holder);
                locationManager.requestLocationUpdates(Holder, 12000, 7, listener);
                //locationManager.requestLocationUpdates(Holder, 400, 1,  listener);


            }
        }else {

            Vibrator vi = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            // Vibrate for 500 milliseconds
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                vi.vibrate(VibrationEffect.createOneShot(100, VibrationEffect.DEFAULT_AMPLITUDE));
            } else {
                //deprecated in API 26
                vi.vibrate(100);
            }

            new AlertDialog.Builder(CreateNewCustomer.this)
                    .setTitle("Warning!")
                    .setMessage("You need to enable location services, for you to use this service")
                    .setCancelable(false)
                    .setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            Intent intent1 = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(intent1);
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            dialog.cancel();
                            finish();
                        }
                    })
                    .show();
        }
        //saveCustToSqlite()
    }
    public void checkDetails(){
        String custname =cust_name_txt.getText().toString();
        String custaddr =cust_addr_txt.getText().toString();
        String contactperson =cust_contact_psn_txt.getText().toString();
        String phone =cust_cell_psn_txt.getText().toString();
        String route = String.valueOf(route_text.getSelectedItem());

        if (custname.isEmpty()){
            cust_name_txt.setError("Customer Name is invalid!");

        }
        else  if (custaddr.isEmpty()){
            cust_addr_txt.setError("Customer address is invalid!");

        }
        else  if (contactperson.isEmpty()){
            cust_contact_psn_txt.setError("Contact Person is invalid!");

        } else  if (phone.isEmpty()){
            cust_cell_psn_txt.setError("Phone is invalid!");

        }
        else  if (route.equalsIgnoreCase("Select Route")){
            Toast.makeText(CreateNewCustomer.this, "Please select a valid route" , Toast.LENGTH_SHORT).show();

        }
        else{
            CheckGpsStatus();
        }



    }
    public void saveCustToSqlite(String longitude,String latitude){
        String custname =cust_name_txt.getText().toString();
        String custaddr =cust_addr_txt.getText().toString();
        String longi = longitude;
        String lati=latitude;
        String contactperson =cust_contact_psn_txt.getText().toString();
        String phone =cust_cell_psn_txt.getText().toString();
        String route = String.valueOf(route_text.getSelectedItem());
        SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
        String repname =shared.getString("user_name", "");
        String uniqueID = UUID.randomUUID().toString();
        if (custname.isEmpty()){}
        else if (custaddr.isEmpty()){}
        else if (contactperson.isEmpty()){}
        else if (phone.isEmpty()){}
        else if (route.equalsIgnoreCase("Select Route")){}
        else if (repname.isEmpty()){}
        else {
            String SQLiteDataBaseQueryHolder = "INSERT INTO " + SQLiteHelper.TABLE_NAME + " (cust_id, cust_name, addr_line_one, cust_lat, cust_long, contact_name, contact_phone ,route_name, user_id, sync_status) VALUES('" + uniqueID + "','" + custname + "', '" + custaddr + "', '" + lati + "', '" + longi + "','" + contactperson + "','" + phone + "','" + route + "','" + repname + "', 'not_synced');";
            sqLiteDatabase.execSQL(SQLiteDataBaseQueryHolder);
            finish();
        }

    }
    public void SQLiteDataBaseBuild() {
        sqLiteDatabase = openOrCreateDatabase(SQLiteHelper.DATABASE_NAME, Context.MODE_PRIVATE, null);
        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS customers (id INTEGER, cust_id TEXT,cust_number TEXT, cust_name TEXT, addr_line_one TEXT, cust_lat TEXT,cust_long TEXT, contact_name TEXT, contact_phone TEXT,route_name TEXT,user_id TEXT, sync_status TEXT)");
        //sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS products (id INTEGER, cust_number TEXT, cust_name TEXT, addr_line_one TEXT)");

    }
    public void CheckGpsStatus(){
        checkLoc();
        locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        GpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(c);
        View mView = layoutInflaterAndroid.inflate(R.layout.location_dialog_box, null);
        AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(c);
        alertDialogBuilderUserInput.setView(mView);

        alertDialogBuilderUserInput
                .setCancelable(false)
                .setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogBox, int id) {

                        String longitude = "";
                        String latitude = "";
                        locationManager.removeUpdates(listener);
                        saveCustToSqlite(longitude,latitude);

                    }
                });


        AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
        alertDialogAndroid.setCancelable(false);
        alertDialogAndroid.show();
    }
    public void checkLoc(){
        if (ActivityCompat.checkSelfPermission(
                CreateNewCustomer.this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                &&
                ActivityCompat.checkSelfPermission(CreateNewCustomer.this, Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {

        }
        location = locationManager.getLastKnownLocation(Holder);
        locationManager.requestLocationUpdates(Holder, 12000, 7, listener);

    }

    }


