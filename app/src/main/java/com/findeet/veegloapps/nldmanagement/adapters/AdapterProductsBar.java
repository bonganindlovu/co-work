package com.findeet.veegloapps.nldmanagement.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.findeet.veegloapps.nldmanagement.AddBarCode;
import com.findeet.veegloapps.nldmanagement.ChoseBarItem;
import com.findeet.veegloapps.nldmanagement.R;
import com.findeet.veegloapps.nldmanagement.Utils.SQLiteHelper;
import com.findeet.veegloapps.nldmanagement.Utils.SQLiteHelperb;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by DEBONGZ on 1/6/2018.
 */

public class AdapterProductsBar extends RecyclerView.Adapter<RecyclerView.ViewHolder> {



    ArrayList<String> prid;
    ArrayList<String> prname;
    ArrayList<String> prunit;
    SQLiteDatabase sqLiteDatabase;
    SQLiteHelperb sqLiteHelperb;
    Cursor cursor;

    private Context context;
    private LayoutInflater inflater;
//    List<DataProducts> data= Collections.emptyList();
//    DataProducts current;
    int currentPos=0;



    public AdapterProductsBar(Context context,
                              ArrayList<String> id,
                              ArrayList<String> name,
                              ArrayList<String> unit){
        this.context=context;
        inflater= LayoutInflater.from(context);
        //this.data=data;
        this.prid = id;
        this.prname = name;
        this.prunit = unit;
    }



    // Inflate the layout when ViewHolder created
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=inflater.inflate(R.layout.container_products, parent,false);
        MyHolder holder=new MyHolder(view);

        return holder;
    }

    // Bind data
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        // Get current position of item in RecyclerView to bind data and assign values from list
        MyHolder myHolder= (MyHolder) holder;
        //DataProducts current=data.get(position);

        myHolder.textProName.setText(prname.get(position));
        myHolder.textProUnit.setText(prunit.get(position));
        myHolder.proId.setText(prid.get(position));



    }
    // return total item from List
    @Override
    public int getItemCount() {
        return prid.size();
    }



    class MyHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView textProName;
        TextView textProUnit;
        TextView proId;

        // create constructor to get widget reference
        public MyHolder(View itemViewa) {
            super(itemViewa);
            textProName = (TextView) itemViewa.findViewById(R.id.product_nametxt);
            textProUnit = (TextView) itemViewa.findViewById(R.id.unit_txt);
            proId = (TextView) itemViewa.findViewById(R.id.product_numbertxt);
            itemViewa.setOnClickListener(this);
        }



        // Click event for all items
        @Override
        public void onClick(View v) {
            SQLiteDataBaseBuild();
            sqLiteHelperb = new SQLiteHelperb(context);

            String product_number = proId.getText().toString();
           String product_name = textProName.getText().toString();
            String product_unit = textProUnit.getText().toString();
            SharedPreferences shared = context.getSharedPreferences("Mypref", Context.MODE_PRIVATE);
            final String thebarcode = shared.getString("thebarcode", "");
            Toast.makeText(context, "Checking...", Toast.LENGTH_SHORT).show();
            updateBar(product_number,thebarcode);
            ( (ChoseBarItem)context).hideThePop();

// go to the new intent with the shop name and adress, and send it to database to create new invoive
//            Intent intent=new Intent(context.getApplicationContext(), Order.class);
//            intent.putExtra("cust_number", shop_number);
//            context.getApplicationContext().startActivity(intent);


        }
    }
    public void updateBar(final String item_id, final String item_barcode) {
        //Toast.makeText(context, "Checking...", Toast.LENGTH_SHORT).show();
         String url_like ="http://nextld.co.za/salesadmin/app/update_products.php";
        //Log.i("Hiteshurl", "" + url_like);
        RequestQueue requestQueuea = Volley.newRequestQueue(context);
        StringRequest stringRequesta = new StringRequest(Request.Method.POST, url_like, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                progressDialog.dismiss();

                  //Toast.makeText(context, "done" , Toast.LENGTH_SHORT).show();

                    updateLite(item_id,item_barcode);
                    Toast.makeText(context, "Barcode successfully Updated", Toast.LENGTH_SHORT).show();
                    SharedPreferences shared = context.getSharedPreferences("Mypref", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = shared.edit();
                    editor.putString("thebarcode", "");
                    editor.commit();
                    Intent intent = new Intent(context, AddBarCode.class);
                    context.startActivity(intent);
                    ((AppCompatActivity)context).finish();


            }




        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i("Hitesh", "" + error);
                Toast.makeText(context, "Error ! Please check your network connection", Toast.LENGTH_SHORT).show();

            }
        }) {

            // seorderid, secustname, secustaddr, sesalesrep
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> stringMapa = new HashMap<>();
                stringMapa.put("item_barcode", item_barcode);
                stringMapa.put("item_id", item_id);
                return stringMapa;
            }

        };

        requestQueuea.add(stringRequesta);


    }
    public void SQLiteDataBaseBuild(){

        sqLiteDatabase = context.openOrCreateDatabase(SQLiteHelper.DATABASE_NAME, Context.MODE_PRIVATE, null);

    }

    public void updateLite(String item_id, String item_barcode) {

        sqLiteDatabase.execSQL("UPDATE products SET  barcode ='"+item_barcode+"' WHERE  id = '"+item_id+"'");
    }

}
