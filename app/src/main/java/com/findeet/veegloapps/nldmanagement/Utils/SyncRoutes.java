package com.findeet.veegloapps.nldmanagement.Utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.findeet.veegloapps.nldmanagement.EndPoints;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by DEBONGZ on 8/22/2018.
 */

public class SyncRoutes extends BroadcastReceiver {

    //context and database helper object
    private Context context;
    SQLiteDatabase sqLiteDatabase;
    SQLiteHelper sqLiteHelper;
    Cursor cursor;

    @Override
    public void onReceive(Context context, Intent intent) {

        this.context = context;

        sqLiteHelper = new SQLiteHelper(context);
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        //if there is a network
        if (activeNetwork != null) {
            //if connected to wifi or mobile data plan
            //Toast.makeText(context, "Started", Toast.LENGTH_SHORT).show();
            SharedPreferences shared = context.getSharedPreferences("Mypref", Context.MODE_PRIVATE);
            String repname =shared.getString("user_name", "");
            sqLiteDatabase = context.openOrCreateDatabase(SQLiteHelper.DATABASE_NAME, Context.MODE_PRIVATE, null);
            //getRoutes(repname);

        }
    }



    public void getRoutes(final String repname) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, EndPoints.GET_ROUTES,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String result) {
                        try {
                            JSONArray jArray = new JSONArray(result);

                            for (int i = 0; i < jArray.length(); i++) {
                                JSONObject d = jArray.getJSONObject(i);
                                JSONObject json_data = jArray.getJSONObject(i);
                                String date_created = json_data.getString("date_created");
                                String route_name = json_data.getString("route_name");
                                String avr_days = json_data.getString("avr_days");
                                String sync_status = json_data.getString("sync_status");
                                String route_status = json_data.getString("route_status");

                                if(route_status.equalsIgnoreCase("done") && sync_status.equalsIgnoreCase("synced")){
                                    removeRoute(route_name);
                                }
                                else if(route_status !="done" && sync_status.equalsIgnoreCase("synced")){
                                    //removeRoute(route_name);
                                }

                                else if(sync_status.isEmpty() || sync_status.equalsIgnoreCase("")) {
                                    InsertRoute(date_created,route_name,avr_days);
                                    getRoutesShops(route_name);
                                }


                                //Toast.makeText(context, ""  + result, Toast.LENGTH_SHORT).show();



                            }
                            context.sendBroadcast(new Intent(EndPoints.ORDER_SAVED_BROADCAST+"routes"));


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("rep_name", repname);
                return params;
            }
        };

        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }

    public void updateRoutes(final String route_name) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, EndPoints.UPDATE_ROUTES,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
//                            JSONObject jsonObject = new JSONObject(response);
//                            JSONArray jsonArray = jsonObject.getJSONArray("result");
//                            JSONObject jsonObject1 = jsonArray.getJSONObject(0);
//
//                            String status = jsonObject1.getString("status");



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("route_name", route_name);
                return params;
            }
        };

        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }


    public static void updateToken(Context context, final String user_name, final String token) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, EndPoints.UPDATE_TOKEN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("user_name", user_name);
                params.put("token", token);
                return params;
            }
        };

        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }

    public void removeRoute(final String route_name){
        sqLiteDatabase.execSQL("DELETE FROM routes WHERE route_name ='"+route_name+"' ");
        sqLiteDatabase.execSQL("DELETE FROM routes_shops WHERE route_name ='"+route_name+"' ");

    }
    public void InsertRoute(final String date_created, final String route_name, final String avr_days){
        String SQLiteDataBaseQueryHolder = "INSERT INTO routes (date_created, route_name, avr_days, sync_state) VALUES('" + date_created + "','" + route_name + "', '" + avr_days + "', 'synced');";
        sqLiteDatabase.execSQL(SQLiteDataBaseQueryHolder);
        updateRoutes(route_name);

    }

    public void getRoutesShops(final String route_name) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, EndPoints.GET_ROUTES_SHOPS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String result) {
                        try {
                            JSONArray jArray = new JSONArray(result);

                            for (int i = 0; i < jArray.length(); i++) {
                                JSONObject d = jArray.getJSONObject(i);
                                JSONObject json_data = jArray.getJSONObject(i);
                                String id = json_data.getString("id");
                                String last_seen = json_data.getString("last_seen");
                                String cust_number = json_data.getString("cust_number");
                                String c_name = json_data.getString("cust_name");
                                String last_order = json_data.getString("last_order");
                                String cust_name = c_name.replace("'","");

                                InsertRouteShops(id,route_name,cust_number,cust_name,last_seen,last_order);
                                //getLastInvoice(cust_number);

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("route_name", route_name);
                return params;
            }
        };

        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }

    public void InsertRouteShops(final String id, final String route_name, final String cust_number, final String cust_name, final String last_seen, final String last_order)
    {
        String SQLiteDataBaseQueryHolder = "INSERT INTO routes_shops (id, route_name, cust_number, cust_name, last_date_seen,last_order_size) VALUES('" +id+ "','" + route_name + "', '" +cust_number+ "', '" +cust_name+ "', '" +last_seen+ "', '" +last_order+ "' );";
        sqLiteDatabase.execSQL(SQLiteDataBaseQueryHolder);
        updateRoutes(route_name);

    }
}
