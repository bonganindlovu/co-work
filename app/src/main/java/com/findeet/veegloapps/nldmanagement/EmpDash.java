package com.findeet.veegloapps.nldmanagement;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.findeet.veegloapps.nldmanagement.adapters.AdapterUserTask;
import com.findeet.veegloapps.nldmanagement.adapters.AdapterUsersTask;
import com.findeet.veegloapps.nldmanagement.datas.DataUserTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class EmpDash extends AppCompatActivity {
    Toolbar toolbar_emp_activity;
    private AdapterUsersTask adapter;
    SwipeRefreshLayout refresh;
    BroadcastReceiver broadcastReceiver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emp_dash);
        toolbar_emp_activity = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar_emp_activity);
        setSupportActionBar(toolbar_emp_activity);
        ActionBar mybar = getSupportActionBar();
        mybar.setTitle("Employee Activity");

        refresh = (SwipeRefreshLayout)findViewById(R.id.swipeRefreshLayout) ;
        fetchTasks();
        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchTasks();
            }
        });

        //the broadcast receiver to update sync status
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //loading the names again
                fetchTasks();
            }
        };
        registerReceiver(broadcastReceiver, new IntentFilter(EndPoints.ORDER_SAVED_BROADCAST+"tasks"));
    }

    private void fetchTasks() {
        refresh.setRefreshing(true);
        RequestQueue requestQueuemom = Volley.newRequestQueue(this);
        SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
        final String fnamename = shared.getString("user_name", "");
        StringRequest stringRequestabc = new StringRequest(Request.Method.GET, EndPoints.URL_FETCH_USERS_TASKS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String result) {
                        try {

                            //Toast.makeText(Notifications.this, ""+result, Toast.LENGTH_SHORT).show();

                            JSONArray jArray = new JSONArray(result);

                            List<DataUserTask> data = new ArrayList<>();

                            for (int i = 0; i < jArray.length(); i++) {
                                JSONObject d = jArray.getJSONObject(i);
                                JSONObject json_data = jArray.getJSONObject(i);
                                DataUserTask proData = new DataUserTask();
                                proData.task_id = json_data.getString("task_id");
                                proData.task_desc = json_data.getString("task_desc");
                                proData.task_start_time = json_data.getString("task_start_time");
                                proData.task_end_time = json_data.getString("task_end_time");
                                proData.task_user_name = json_data.getString("task_user_name");

                                data.add(proData);

                            }
                            refresh.setRefreshing(false);

//

                            RecyclerView myRecycler = (RecyclerView) findViewById(R.id.emp_activityRecycle);
                            myRecycler.setLayoutManager(new LinearLayoutManager(EmpDash.this));
                            adapter = new AdapterUsersTask(EmpDash.this, data);
                            myRecycler.setAdapter(adapter);
                            adapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            Toast.makeText(EmpDash.this, ""+e, Toast.LENGTH_SHORT).show();
                            refresh.setRefreshing(false);
                            e.printStackTrace();
                        }

                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //myrefresh.setRefreshing(false);
                        refresh.setRefreshing(false);
                        Toast.makeText(EmpDash.this, "Loading failed", Toast.LENGTH_SHORT).show();
                    }
                }) {

        };

        requestQueuemom.add(stringRequestabc);
    }
}
