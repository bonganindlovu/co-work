package com.findeet.veegloapps.nldmanagement.Utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.findeet.veegloapps.nldmanagement.Dispatch;
import com.findeet.veegloapps.nldmanagement.EndPoints;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Created by DEBONGZ on 8/22/2018.
 */

public class SyncDel extends BroadcastReceiver {

    //context and database helper object
    private Context context;
    SQLiteDatabase sqLiteDatabase;
    SQLiteHelper sqLiteHelper;
    Cursor cursor;

    @Override
    public void onReceive(Context context, Intent intent) {

        this.context = context;

        sqLiteHelper = new SQLiteHelper(context);
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        //if there is a network
        if (activeNetwork != null) {

            checkLoads();

        }
    }

    private void checkLoads() {
        RequestQueue requestQueuemom = Volley.newRequestQueue(context);
        SharedPreferences shared = context.getSharedPreferences("Mypref", Context.MODE_PRIVATE);
      String repname =shared.getString("user_name", "");

        StringRequest stringRequestabc = new StringRequest(Request.Method.GET, EndPoints.GET_LOADS+"?user_name="+repname,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String result) {
                        //Toast.makeText(context, "Res ="+result, Toast.LENGTH_SHORT).show();
                        try {

                            JSONArray jArray = new JSONArray(result);


                            for (int i = 0; i < jArray.length(); i++) {
                                JSONObject json_data = jArray.getJSONObject(i);

                                //proData.user_id = json_data.getString("user_id");

                                String load_id = json_data.getString("load_id");
                                String vehicle_name = json_data.getString("vehicle_name");
                                String load_time = json_data.getString("load_time");

                                sqLiteDatabase = sqLiteHelper.getWritableDatabase();
                                cursor = sqLiteDatabase.rawQuery("SELECT * FROM loads WHERE load_id = '"+load_id+"'  ", null);
                                if (cursor.moveToFirst()) {

                                }
                                else{

                                    AddLoadOn(vehicle_name,load_id, load_time)  ;
                                }



                            }



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {

        };
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequestabc);
    }

    public void AddLoadOn(final String vehicle_name, final String load_id, final String load_time){
        sqLiteDatabase.beginTransaction(); //getTimestamp
        String database = "loads";
        SharedPreferences shared = context.getSharedPreferences("Mypref", Context.MODE_PRIVATE);
        String driver_name =shared.getString("user_name", "");

        String SQLiteDataBaseQueryHolder = "INSERT INTO "+database+" (load_id,vehicle_name,drive_name,load_time,synce_status" +
                ") VALUES('"+load_id+"', '"+vehicle_name+"', '"+driver_name+"','"+load_time+"', 'synced');";
        sqLiteDatabase.execSQL(SQLiteDataBaseQueryHolder);
        sqLiteDatabase.setTransactionSuccessful();
        sqLiteDatabase.endTransaction();
        context.sendBroadcast(new Intent(EndPoints.ORDER_SAVED_BROADCAST+"del"));
    }

}
