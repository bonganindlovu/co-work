package com.findeet.veegloapps.nldmanagement.datas;

/**
 * Created by DEBONGZ on 4/19/2018.
 */

public class DataOpen {
    public String order_id;
    public String order_quote_number;
    public String order_cust_name;
    public String order_cust_addr;
    public String order_total;
    public String order_block;
    public String order_date;
    public String order_status;
    public String order_diff;
    public String getID() {
        return order_id;
    }
}
