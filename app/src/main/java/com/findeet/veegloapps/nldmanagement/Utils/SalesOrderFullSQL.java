package com.findeet.veegloapps.nldmanagement.Utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.findeet.veegloapps.nldmanagement.EndPoints;
import com.findeet.veegloapps.nldmanagement.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by DEBONGZ on 8/22/2018.
 */

public class SalesOrderFullSQL extends BroadcastReceiver {

    //context and database helper object
    private Context context;
    SQLiteDatabase sqLiteDatabase;
    SQLiteHelper sqLiteHelper;
    Cursor cursor;
    String url_save = "http://nextld.co.za/salesadmin/app/save_order.php";;

    @Override
    public void onReceive(Context context, Intent intent) {

        this.context = context;

        sqLiteHelper = new SQLiteHelper(context);
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        //if there is a network
        if (activeNetwork != null) {
            //if connected to wifi or mobile data plan
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI || activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                ShowSQLiteDBdata();
                ShowSQLiteDone();
            }
        }
    }
    private void ShowSQLiteDBdata() {

        sqLiteDatabase = sqLiteHelper.getWritableDatabase();
        String value ="not_synced";
        cursor = sqLiteDatabase.rawQuery("SELECT * FROM sales_order WHERE sync_state = '"+value+"'  ", null);

        int count = 0;
        if (cursor.moveToFirst()) {
            do {
                //order_id,cust_name,cust_addr,order_status,sales_rep,sync_state/
                String seorderid = cursor.getString(cursor.getColumnIndex("order_id"));
                String secustname = cursor.getString(cursor.getColumnIndex("cust_name"));
                String secustaddr = cursor.getString(cursor.getColumnIndex("cust_addr"));
                String sesalesrep = cursor.getString(cursor.getColumnIndex("sales_rep"));
                String sereturn_order = cursor.getString(cursor.getColumnIndex("return_order"));
                String note ="";

                String snote = cursor.getString(cursor.getColumnIndex("note"));
                if (snote !=null){
                    note =cursor.getString(cursor.getColumnIndex("note"));
                }
                else{note ="";}

                SAVEQUOTE(seorderid, secustname, secustaddr, sesalesrep,note,sereturn_order);
                count+=count+1;

            }

            while (cursor.moveToNext());
        }

        int rowCount = count;
        //Toast.makeText(context, "" + rowCount, Toast.LENGTH_SHORT).show();
    }
    private void ShowSQLiteDone() {

        sqLiteDatabase = sqLiteHelper.getWritableDatabase();
        String value ="synced";
        cursor = sqLiteDatabase.rawQuery("SELECT * FROM sales_order WHERE sync_state = '"+value+"'  ", null);

        int count = 0;
        if (cursor.moveToFirst()) {
            do {
                //order_id,cust_name,cust_addr,order_status,sales_rep,sync_state/
                String seorderid = cursor.getString(cursor.getColumnIndex("order_id"));
                Checktatus(seorderid);
                count+=count+1;

            }

            while (cursor.moveToNext());
        }

        int rowCount = count;
        //Toast.makeText(context, "" + rowCount, Toast.LENGTH_SHORT).show();
    }
    public void updateSyncStatus(final String order_id, final String time_sent, final String date_of_sale, final String order_total){

        sqLiteDatabase.execSQL("UPDATE sales_order SET sync_state ='synced', time_sent ='"+time_sent+"', date_of_sale ='"+date_of_sale+"', order_total ='"+order_total+"' WHERE order_id ='"+order_id+"' ");
    }

    public void Checktatus(final String seorderid) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, EndPoints.CHECK_ORDER_STATUS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("result");
                            JSONObject jsonObject1 = jsonArray.getJSONObject(0);
                            String status = jsonObject1.getString("status");
                            String quote_number = jsonObject1.getString("quote_number");
                            String invoice_number = jsonObject1.getString("invoice_number");
                            String pulled_by = jsonObject1.getString("pulled_by");

                            //Toast.makeText(context, "" + message, Toast.LENGTH_SHORT).show();

                            if(status.equalsIgnoreCase("open")) {

                            }
                            else{
                                //delete
                                DeleteOrder(seorderid,status,quote_number,invoice_number,pulled_by);
                                context.sendBroadcast(new Intent(EndPoints.ORDER_SAVED_BROADCAST+""+status));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("order_id", seorderid);
                return params;
            }
        };

        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }

    public void SAVEQUOTE(final String seorderid, final String secustname, final String secustaddr, final String sesalesrep, final String snote, final String return_order) {
        //Toast.makeText(context, "" + snote, Toast.LENGTH_SHORT).show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url_save,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("result");
                            JSONObject jsonObject1 = jsonArray.getJSONObject(0);
                            String status = jsonObject1.getString("status");
                            String message = jsonObject1.getString("message");
                            String time_sent = jsonObject1.getString("time_sent");
                            String date_of_sale = jsonObject1.getString("date_of_sale");
                            String order_total = jsonObject1.getString("order_total");


                            //Toast.makeText(context, "" + message, Toast.LENGTH_SHORT).show();

                            if(status.equalsIgnoreCase("passed")) {
                            //update synce status
                                updateSyncStatus(seorderid,time_sent,date_of_sale,order_total);
                                context.sendBroadcast(new Intent(EndPoints.ORDER_SAVED_BROADCAST));
                                //PlaySent();
                                //Toast.makeText(context, "" + status+"" + message, Toast.LENGTH_SHORT).show();
                            }
                          else{
                                //Toast.makeText(context, "" + status+"" + message, Toast.LENGTH_SHORT).show();
                                RemoveEmptyOrder(seorderid) ;
                                context.sendBroadcast(new Intent(EndPoints.ORDER_SAVED_BROADCAST));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("order_id", seorderid);
                params.put("cust_name", secustname);
                params.put("cust_addr", secustaddr);
                params.put("sales_rep", sesalesrep);
                params.put("order_note", snote);
                params.put("return_order", return_order);
                return params;
            }
        };

        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }
    public void PlaySent() {
        AudioManager am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        float vol = (float) 100; //This will be half of the default system sound
        am.playSoundEffect(AudioManager.FX_KEY_CLICK, vol);
    }
    public void DeleteOrder(final String order_id, final String order_status, final String quote_number, final String invoice_number, final String pulled_by){
//status,quote_number,invoice_number,pulled_by
        sqLiteDatabase.execSQL("UPDATE sales_order SET order_status ='"+order_status+"', quote_number ='"+quote_number+"', invoice_number ='"+invoice_number+"', pulled_by ='"+pulled_by+"' WHERE order_id ='"+order_id+"' ");
    }
    public void RemoveEmptyOrder(final String order_id){
//status,quote_number,invoice_number,pulled_by
        sqLiteDatabase.execSQL("DELETE FROM sales_order WHERE order_id ='"+order_id+"' ");

    }
}
