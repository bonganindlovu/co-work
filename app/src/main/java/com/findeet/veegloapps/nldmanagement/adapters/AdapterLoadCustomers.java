package com.findeet.veegloapps.nldmanagement.adapters;

/**
 * Created by DEBONGZ on 1/5/2018.
 */

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.findeet.veegloapps.nldmanagement.EndPoints;
import com.findeet.veegloapps.nldmanagement.R;
import com.findeet.veegloapps.nldmanagement.Utils.SQLiteHelper;
import com.findeet.veegloapps.nldmanagement.Utils.SQLiteHelperb;
import com.findeet.veegloapps.nldmanagement.Utils.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AdapterLoadCustomers extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<String> custid;
    ArrayList<String> custname;
    ArrayList<String> custaddr;
    SQLiteDatabase sqLiteDatabase;
    SQLiteHelperb sqLiteHelperb;
    Cursor cursor;

    private Context context;
    private LayoutInflater inflater;
    //List<DataCustomers> data= Collections.emptyList();
    //DataCustomers current;
    int currentPos=0;


    // create constructor to initialize context and data sent from MainActivity
    public AdapterLoadCustomers(Context context,
                                ArrayList<String> id,
                                ArrayList<String> sub_name,
                                ArrayList<String> Sub_full){
        this.context=context;
        inflater= LayoutInflater.from(context);
        //this.data=data;
        this.custid = id;
        this.custname = sub_name;
        this.custaddr = Sub_full;

    }



    // Inflate the layout when ViewHolder created
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=inflater.inflate(R.layout.container_customers, parent,false);
        MyHolder holder=new MyHolder(view);
        return holder;
    }
    // Bind data
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        // Get current position of item in RecyclerView to bind data and assign values from list
        MyHolder myHolder= (MyHolder) holder;
        //DataCustomers current=data.get(position);
        myHolder.textCustName.setText(custname.get(position));
        myHolder.textAddr.setText(custaddr.get(position));
        myHolder.shopId.setText(custid.get(position));

    }
    // return total item from List
    @Override
    public int getItemCount() {
        return custid.size();

    }
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    class MyHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView textCustName;
        TextView textAddr;
        TextView shopId;
        TextView h;
        EditText invoice_txt,amount_txt;

        // create constructor to get widget reference
        public MyHolder(View itemView) {
            super(itemView);
            textCustName = (TextView) itemView.findViewById(R.id.cust_nametxt);
            textAddr = (TextView) itemView.findViewById(R.id.cust_addr_txt);
            shopId = (TextView) itemView.findViewById(R.id.shopid);
            itemView.setOnClickListener(this);
        }

        // Click event for all items
        @Override
        public void onClick(View v) {
            SQLiteDataBaseBuild();
            sqLiteHelperb = new SQLiteHelperb(context);
            LayoutInflater layoutInflaterAndroid = LayoutInflater.from(context);
            View mView = layoutInflaterAndroid.inflate(R.layout.add_invoice_dialog_box, null);
            AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(context);
            alertDialogBuilderUserInput.setView(mView);
            SharedPreferences shared = context.getSharedPreferences("Mypref", Context.MODE_PRIVATE);
            final String shop_number = shopId.getText().toString();
            final String shop_name = textCustName.getText().toString();
            final String load_id =shared.getString("load_id", "");


            h = (TextView) mView.findViewById(R.id.h);
            invoice_txt = (EditText) mView.findViewById(R.id.invoice_txt);
            amount_txt = (EditText) mView.findViewById(R.id.amount_txt);
            h.setText(shop_name);

            alertDialogBuilderUserInput
                    .setCancelable(false)
                    .setPositiveButton("SUBMIT", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogBox, int id) {



                            if (invoice_txt.getText().toString().equalsIgnoreCase("")){
                                Toast.makeText(context, "Please enter invoice number!", Toast.LENGTH_SHORT).show();
                            }
                            else if (amount_txt.getText().toString().equalsIgnoreCase("")){
                                Toast.makeText(context, "Please enter amount!", Toast.LENGTH_SHORT).show();
                            }
                            else{


                                sendLoads(load_id,shop_number,shop_name,invoice_txt.getText().toString(),amount_txt.getText().toString());

                            }



                        }
                    })

                    .setNegativeButton("CANCEL",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialogBox, int id) {
                                    //dialogBox.cancel();
                                }
                            });

            AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
            alertDialogAndroid.show();

        }
    }
    public void SQLiteDataBaseBuild(){

        sqLiteDatabase = context.openOrCreateDatabase(SQLiteHelper.DATABASE_NAME, Context.MODE_PRIVATE, null);

    }
    public void sendLoads(final String load_id,final String shop_id,final String shop_name,final String invoice_number,final String invoice_amount){

        StringRequest stringRequest = new StringRequest(Request.Method.POST, EndPoints.SEND_LOAD_SHOPS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String result) {
                       // Toast.makeText(context, ""+result, Toast.LENGTH_SHORT).show();
                        try {
                            JSONArray jArray = new JSONArray(result);

                            for (int i = 0; i < jArray.length(); i++) {
                                JSONObject d = jArray.getJSONObject(i);
                                JSONObject json_data = jArray.getJSONObject(i);
                                String invoice_number = json_data.getString("invoice_number");

                                //updateSync(invoice_number);

                                sqLiteDatabase.beginTransaction(); //getTimestamp
                                String database = "loads_shops";
                                String SQLiteDataBaseQueryHolder = "INSERT INTO "+database+" (load_id,shop_id,shop_name,invoice_number,invoice_amount,sync_status" +
                                        ") VALUES('"+load_id+"', '"+shop_id+"', '"+shop_name+"','"+invoice_number+"','"+invoice_amount+"', 'synced');";
                                sqLiteDatabase.execSQL(SQLiteDataBaseQueryHolder);
                                Toast.makeText(context, "Invoice Inserted", Toast.LENGTH_SHORT).show();
                                sqLiteDatabase.setTransactionSuccessful();
                                sqLiteDatabase.endTransaction();

                                ((AppCompatActivity)context).finish();


                            }


                        } catch (JSONException e) {
                            e.printStackTrace();

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        sqLiteDatabase.beginTransaction(); //getTimestamp
                        String database = "loads_shops";
                        String SQLiteDataBaseQueryHolder = "INSERT INTO "+database+" (load_id,shop_id,shop_name,invoice_number,invoice_amount,sync_status" +
                                ") VALUES('"+load_id+"', '"+shop_id+"', '"+shop_name+"','"+invoice_number+"','"+invoice_amount+"', 'not_synced');";
                        sqLiteDatabase.execSQL(SQLiteDataBaseQueryHolder);
                        Toast.makeText(context, "Invoice Inserted", Toast.LENGTH_SHORT).show();
                        sqLiteDatabase.setTransactionSuccessful();
                        sqLiteDatabase.endTransaction();

                        ((AppCompatActivity)context).finish();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("load_id", load_id);
                params.put("shop_id", shop_id);
                params.put("shop_name", shop_name);
                params.put("invoice_number", invoice_number);
                params.put("invoice_amount", invoice_amount);
                return params;
            }
        };
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }
    }
