package com.findeet.veegloapps.nldmanagement;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.chootdev.csnackbar.Align;
import com.chootdev.csnackbar.Duration;
import com.chootdev.csnackbar.Snackbar;
import com.chootdev.csnackbar.Type;
import com.findeet.veegloapps.nldmanagement.firebase.SharedPrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.findeet.veegloapps.nldmanagement.MainActivity.RequestPermissionCode;
import static com.findeet.veegloapps.nldmanagement.Utils.SyncRoutes.updateToken;


public class Login extends AppCompatActivity {
    EditText username, password;
    Button login;
    String url = "http://nextld.co.za/salesadmin/app/signin.php";
    String pass, user_name;
    private ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        changeStatusBar();
        setContentView(R.layout.activity_login);
        EnableRuntimePermission();
        username = findViewById(R.id.user_name);
        password = findViewById(R.id.password);
        login = findViewById(R.id.btn_login);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String seuser_name = username.getText().toString();
                String sepass = password.getText().toString();
                if (seuser_name.isEmpty() || sepass.isEmpty()) {

//                    Snackbar.make(v, "both username and pass are required!", Snackbar.LENGTH_LONG)
//                    .setAction("Action", null).show();
                    Snackbar.with(Login.this,null)
                            .type(Type.ERROR)
                            .message("username and pass are required!")
                            .duration(Duration.SHORT)
                            .fillParent(true)
                            .textAlign(Align.LEFT)
                            .show();
                    Toast.makeText(Login.this, "username and pass are required!" , Toast.LENGTH_SHORT).show();

                } else {

                    login(seuser_name, sepass);
                }
            }
        });
    }
    public void login(final String seuser_name, final String sepass) {

        Log.i("Hiteshurl", "" + url);
        RequestQueue requestQueueone = Volley.newRequestQueue(Login.this);
        StringRequest stringRequestone = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("result");
                    JSONObject jsonObject1 = jsonArray.getJSONObject(0);
                    String status = jsonObject1.getString("status");
                    String user_type = jsonObject1.getString("user_type");
                    String user_full_name = jsonObject1.getString("user_full_name");
                    String user_name = jsonObject1.getString("user_name");
                    String user_email = jsonObject1.getString("user_email");
                    String password = jsonObject1.getString("password");
                    String message = jsonObject1.getString("message");


                    if (status.equalsIgnoreCase("passed")) {

                        SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = shared.edit();
                        editor.putString("pass", password);
                        editor.putString("user_name", user_name);
                        editor.putString("user_type", user_type);
                        editor.putString("user_full_name", user_full_name);
                        editor.putString("user_email", user_email);
                        editor.commit();
                        String token = SharedPrefManager.getInstance(Login.this).getDeviceToken();
                        updateToken(user_name,token);

                        SharedPreferences mshared = getSharedPreferences("SetupPref", Context.MODE_PRIVATE);
                        String loaded = mshared.getString("loaded", "");
                        if (loaded.length() == 0) {

                            Intent intent = new Intent(Login.this, SaveSqlCustomers.class);
                            startActivity(intent);
                            finish();


                        } else {

//                            Snackbar.with(Login.this,null)
//                            .type(Type.SUCCESS)
//                                    .message(message)
//                                    .duration(Duration.SHORT)
//                                    .fillParent(true)
//                                    .textAlign(Align.LEFT)
//                                    .show();
                           Toast.makeText(Login.this, "" + message, Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(Login.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        }

                    } else {
                        Toast.makeText(Login.this, "" + message, Toast.LENGTH_SHORT).show();

//                        Snackbar.with(Login.this,null)
//                                .type(Type.SUCCESS)
//                                .message(message)
//                                .duration(Duration.SHORT)
//                                .fillParent(true)
//                                .textAlign(Align.LEFT)
//                                .show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.i("Hitesh", "" + error);
                Snackbar.with(Login.this,null)
                        .type(Type.ERROR)
                        .message("Error! Please check your network connection")
                        .duration(Duration.SHORT)
                        .fillParent(true)
                        .textAlign(Align.LEFT)
                        .show();
                Toast.makeText(Login.this, "" + error, Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> stringMap = new HashMap<>();
                stringMap.put("username", seuser_name);
                stringMap.put("password", sepass);

                return stringMap;
            }

        };

        requestQueueone.add(stringRequestone);
        //initialize the progress dialog and show it
        progressDialog = new ProgressDialog(Login.this);
        progressDialog.setMessage("Signing in...");
        progressDialog.show();
    }

    public void updateToken(final String user_name, final String token) {

        Log.i("Hiteshurl", "" + url);
        RequestQueue requestQueueone = Volley.newRequestQueue(Login.this);
        StringRequest stringRequestone = new StringRequest(Request.Method.POST, EndPoints.UPDATE_TOKEN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.i("Hitesh", "" + error);
//                Snackbar.with(Login.this,null)
//                        .type(Type.ERROR)
//                        .message("Error! Please check your network connection")
//                        .duration(Duration.SHORT)
//                        .fillParent(true)
//                        .textAlign(Align.LEFT)
//                        .show();
//                Toast.makeText(Login.this, "" + error, Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> stringMap = new HashMap<>();
                stringMap.put("user_name", user_name);
                stringMap.put("token", token);

                return stringMap;
            }

        };

        requestQueueone.add(stringRequestone);

    }
    public void EnableRuntimePermission(){

        if (ActivityCompat.shouldShowRequestPermissionRationale(Login.this,
                Manifest.permission.ACCESS_FINE_LOCATION))
        {

            Toast.makeText(Login.this,"ACCESS_FINE_LOCATION permission allows us to Access GPS in app", Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(Login.this,new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION}, RequestPermissionCode);

        }
    }

    @Override
    public void onRequestPermissionsResult(int RC, String per[], int[] PResult) {

        switch (RC) {

            case RequestPermissionCode:

                if (PResult.length > 0 && PResult[0] == PackageManager.PERMISSION_GRANTED) {

                    //Toast.makeText(getActivity(),"Permission Granted.", Toast.LENGTH_LONG).show();

                } else {

                    Toast.makeText(Login.this,"Permission Canceled.", Toast.LENGTH_LONG).show();

                }
                break;
        }
    }
    public void changeStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }
    }
    }
