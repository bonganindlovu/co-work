package com.findeet.veegloapps.nldmanagement;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.chootdev.csnackbar.Align;
import com.chootdev.csnackbar.Duration;
import com.chootdev.csnackbar.Snackbar;
import com.chootdev.csnackbar.Type;
import com.findeet.veegloapps.nldmanagement.firebase.SharedPrefManager;
import com.github.gcacace.signaturepad.views.SignaturePad;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class ConfirmSignature extends AppCompatActivity {
    Boolean check = true;
    String ImagePath = "image_path" ;

    private ProgressDialog progressDialog;
    TextView save_btn;
    private SignaturePad mSignaturePadby;
    private TextView mClearButton,mSaveButton,cat_txt;
    Bitmap signatureBitmapby;
    TextView  sig_by_txt, username_txt,action_txt;
    LinearLayout all;

    ImageView sigimgby;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_signature);



        Intent intent = getIntent();
        String task_id = intent.getStringExtra("order_id");
        loadConfirmation(task_id);

        username_txt =(TextView)findViewById(R.id.user_name) ;
        action_txt =(TextView)findViewById(R.id.action) ;


        sig_by_txt=(TextView)findViewById(R.id.sig_by_txt);
        save_btn=(TextView)findViewById(R.id.save_btn);
        sigimgby = (ImageView)findViewById(R.id.imgby);
        mSignaturePadby = (SignaturePad) findViewById(R.id.signature_by);
        all = (LinearLayout) findViewById(R.id.all);

        mSignaturePadby.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
                //Toast.makeText(AsignByTab.this, "OnStartSigning", Toast.LENGTH_SHORT).show();
                signatureBitmapby = mSignaturePadby.getSignatureBitmap();
                sigimgby.setImageBitmap(signatureBitmapby);

            }

            @Override
            public void onSigned() {
                // set the save button to be true
                sig_by_txt.setText("signed");

            }

            @Override
            public void onClear() {
                // set the save button to be true and all fields
                sig_by_txt.setText("not_signed");
            }
        });
        mClearButton = (TextView) findViewById(R.id.clear);
        mSaveButton = (TextView) findViewById(R.id.save_btn);
        mClearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSignaturePadby.clear();
                sig_by_txt.setText("not_signed");
            }
        });

        save_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String by_sig = sig_by_txt.getText().toString();
                if( by_sig.equalsIgnoreCase("not_signed")){
                    //toast
                    Toast.makeText(ConfirmSignature.this, "Please Sign  signature tab!",
                            Toast.LENGTH_LONG).show();
                }

                else{
                    //save the expense
                    ImageUploadToServerFunction();
                }


            }
        });
    }
    public void showAll(){
        all.setVisibility(View.VISIBLE);
        save_btn.setVisibility(View.VISIBLE);

    }
    public void hideAll(){
        all.setVisibility(View.INVISIBLE);
        save_btn.setVisibility(View.INVISIBLE);

    }

    public void loadConfirmation(final String task_id) {

        RequestQueue requestQueueone = Volley.newRequestQueue(ConfirmSignature.this);
        StringRequest stringRequestone = new StringRequest(Request.Method.POST, EndPoints.FETCH_TASKS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("result");
                    JSONObject jsonObject1 = jsonArray.getJSONObject(0);
                    String user_name = jsonObject1.getString("user_name");
                    String task_name = jsonObject1.getString("task_name");
                    String task_time = jsonObject1.getString("task_time");

                    username_txt.setText(user_name);
                    action_txt.setText(task_name+" at "+task_time);

                    showAll();

                } catch (JSONException e) {
                    Toast.makeText(ConfirmSignature.this, "error loading details ", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.i("Hitesh", "" + error);
              hideAll();
                Toast.makeText(ConfirmSignature.this, "loading details failed ", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> stringMap = new HashMap<>();
                stringMap.put("task_id", task_id);


                return stringMap;
            }

        };

        requestQueueone.add(stringRequestone);
        //initialize the progress dialog and show it
        progressDialog = new ProgressDialog(ConfirmSignature.this);
        progressDialog.setMessage("Fetching details...");
        progressDialog.show();
    }


    public void ImageUploadToServerFunction(){
        Bitmap bm= mSignaturePadby.getSignatureBitmap();
        ByteArrayOutputStream ba=new ByteArrayOutputStream(  );
        bm.compress( Bitmap.CompressFormat.JPEG, 100,ba );
        byte[] by=ba.toByteArray();
        final String encod= Base64.encodeToString( by,Base64.DEFAULT );



        final String ConvertImage = encod;


        class AsyncTaskUploadClass extends AsyncTask<Void,Void,String> {

            @Override
            protected void onPreExecute() {

                super.onPreExecute();
                //show progressbar
                progressDialog = new ProgressDialog(ConfirmSignature.this);
                progressDialog.setMessage("Saving Signatures...");
                progressDialog.show();
            }

            @Override
            protected void onPostExecute(String string1) {

                super.onPostExecute(string1);

                Intent intent = new Intent(ConfirmSignature.this, Tasks.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                // Printing uploading success message coming from server on android app.
                Toast.makeText(ConfirmSignature.this, "Clock Signature saved!", Toast.LENGTH_LONG).show();
                progressDialog.dismiss();

            }

            @Override
            protected String doInBackground(Void... params) {

                Intent intent = getIntent();
                String task_id = intent.getStringExtra("order_id");


                ConfirmSignature.ImageProcessClass imageProcessClass = new ConfirmSignature.ImageProcessClass();

                HashMap<String,String> HashMapParams = new HashMap<String,String>();

                HashMapParams.put(ImagePath, ConvertImage);
                HashMapParams.put("task_id",task_id);
                String FinalData = imageProcessClass.ImageHttpRequest(EndPoints.CONFIRM_TASKS, HashMapParams);

                return FinalData;
            }
        }
        AsyncTaskUploadClass AsyncTaskUploadClassOBJ = new AsyncTaskUploadClass();
        AsyncTaskUploadClassOBJ.execute();
    }
    public class ImageProcessClass{

        public String ImageHttpRequest(String requestURL,HashMap<String, String> PData) {

            StringBuilder stringBuilder = new StringBuilder();

            try {

                URL url;
                HttpURLConnection httpURLConnectionObject ;
                OutputStream OutPutStream;
                BufferedWriter bufferedWriterObject ;
                BufferedReader bufferedReaderObject ;
                int RC ;

                url = new URL(requestURL);

                httpURLConnectionObject = (HttpURLConnection) url.openConnection();

                httpURLConnectionObject.setReadTimeout(19000);

                httpURLConnectionObject.setConnectTimeout(19000);

                httpURLConnectionObject.setRequestMethod("POST");

                httpURLConnectionObject.setDoInput(true);

                httpURLConnectionObject.setDoOutput(true);

                OutPutStream = httpURLConnectionObject.getOutputStream();

                bufferedWriterObject = new BufferedWriter(

                        new OutputStreamWriter(OutPutStream, "UTF-8"));

                bufferedWriterObject.write(bufferedWriterDataFN(PData));

                bufferedWriterObject.flush();

                bufferedWriterObject.close();

                OutPutStream.close();

                RC = httpURLConnectionObject.getResponseCode();

                if (RC == HttpsURLConnection.HTTP_OK) {

                    bufferedReaderObject = new BufferedReader(new InputStreamReader(httpURLConnectionObject.getInputStream()));

                    stringBuilder = new StringBuilder();

                    String RC2;

                    while ((RC2 = bufferedReaderObject.readLine()) != null){

                        stringBuilder.append(RC2);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return stringBuilder.toString();
        }
        private String bufferedWriterDataFN(HashMap<String, String> HashMapParams) throws UnsupportedEncodingException {

            StringBuilder stringBuilderObject;

            stringBuilderObject = new StringBuilder();

            for (Map.Entry<String, String> KEY : HashMapParams.entrySet()) {

                if (check)

                    check = false;
                else
                    stringBuilderObject.append("&");

                stringBuilderObject.append(URLEncoder.encode(KEY.getKey(), "UTF-8"));

                stringBuilderObject.append("=");

                stringBuilderObject.append(URLEncoder.encode(KEY.getValue(), "UTF-8"));
            }

            return stringBuilderObject.toString();
        }

    }
}
