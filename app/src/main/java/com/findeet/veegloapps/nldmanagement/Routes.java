package com.findeet.veegloapps.nldmanagement;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.findeet.veegloapps.nldmanagement.Utils.SQLiteHelper;
import com.findeet.veegloapps.nldmanagement.Utils.SQLiteHelperb;
import com.findeet.veegloapps.nldmanagement.Utils.SyncRoutes;
import com.findeet.veegloapps.nldmanagement.Utils.VolleySingleton;
import com.findeet.veegloapps.nldmanagement.adapters.AdapterPulled;
import com.findeet.veegloapps.nldmanagement.adapters.AdapterRoutes;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Routes extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {
    android.support.v7.widget.Toolbar routes_toolbar;
    private RecyclerView list_q,zmycustRecycleViewa;
    private AdapterRoutes zmAdaptera;
    SwipeRefreshLayout myrefresh;
    SQLiteHelperb sqLiteHelperb;
    SQLiteDatabase sqLiteDatabase;
    ImageView refresh;
    Cursor cursor;
    private final Context context = this;
    ArrayList<String> route_name_Array,avr_days_Array;
    private BroadcastReceiver broadcastReceiver;

    private Location location;
    private GoogleApiClient googleApiClient;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private LocationRequest locationRequest;
    private static final long UPDATE_INTERVAL = 5000, FASTEST_INTERVAL = 5000; // = 5 seconds
    // lists for permissions
    private ArrayList<String> permissionsToRequest;
    private ArrayList<String> permissionsRejected = new ArrayList<>();
    private ArrayList<String> permissions = new ArrayList<>();
    // integer for permissions results request
    private static final int ALL_PERMISSIONS_RESULT = 1011;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_routes);
        routes_toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.routes_toolbar);
        setSupportActionBar(routes_toolbar);
        refresh = (ImageView)findViewById(R.id.refresh) ;
        SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
        String repname =shared.getString("user_name", "");
        ActionBar mybar = getSupportActionBar();
        mybar.setTitle(repname+"'s routes");
        SQLiteDataBaseBuild();



        route_name_Array = new ArrayList<String>();
        avr_days_Array = new ArrayList<String>();
        myrefresh = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout) ;
        sqLiteHelperb = new SQLiteHelperb(this);
        RoutesSQLite();
        getRoutes(repname);
        myrefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                RoutesSQLite();
            }
        });

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
                String repname =shared.getString("user_name", "");
                getRoutes(repname);
                Toast.makeText(Routes.this, "Refreshed !", Toast.LENGTH_SHORT).show();
                //( (SyncRoutes) Activity.this).getRoutes();

            }
        });

        //the broadcast receiver to update sync status
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                //loading the names again
                RoutesSQLite();
            }
        };
        //registering the broadcast receiver to update sync status
        registerReceiver(broadcastReceiver, new IntentFilter(EndPoints.ORDER_SAVED_BROADCAST+"routes"));


        // we add permissions we need to request location of the users
        permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);

        permissionsToRequest = permissionsToRequest(permissions);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (permissionsToRequest.size() > 0) {
                requestPermissions(permissionsToRequest.toArray(
                        new String[permissionsToRequest.size()]), ALL_PERMISSIONS_RESULT);
            }
        }

        // we build google api client
        googleApiClient = new GoogleApiClient.Builder(this).
                addApi(LocationServices.API).
                addConnectionCallbacks(this).
                addOnConnectionFailedListener(this).build();
    }
    private ArrayList<String> permissionsToRequest(ArrayList<String> wantedPermissions) {
        ArrayList<String> result = new ArrayList<>();

        for (String perm : wantedPermissions) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
        }

        return true;
    }

    public void SQLiteDataBaseBuild() {
        sqLiteDatabase = openOrCreateDatabase(SQLiteHelper.DATABASE_NAME, Context.MODE_PRIVATE, null);
        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS routes (date_created VARCHAR(50),route_name VARCHAR(50), avr_days DOUBLE(13,1), sync_state VARCHAR(50))");
        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS routes_shops (id INT(200),route_name VARCHAR(100),cust_number VARCHAR(100),cust_name VARCHAR(100), last_date_seen VARCHAR(100), last_order_size VARCHAR(100), last_person_seen VARCHAR(100),last_message TEXT, sync_state VARCHAR(50))");
    }
    private void RoutesSQLite() {

        sqLiteDatabase = sqLiteHelperb.getWritableDatabase();
        cursor = sqLiteDatabase.rawQuery("SELECT * FROM routes ORDER BY date_created DESC ", null);
        route_name_Array.clear();
        avr_days_Array.clear();
        myrefresh.setRefreshing(false);
        if (cursor.moveToFirst()) {
            do {
                //  DOUBLE(13,2),note VARCHAR(50),
                route_name_Array.add(cursor.getString(cursor.getColumnIndex("route_name")));
                avr_days_Array.add(cursor.getString(cursor.getColumnIndex("avr_days")));
            }

            while (cursor.moveToNext());
        }

        // Setup and Handover data to recyclerview
        zmycustRecycleViewa = (RecyclerView) findViewById(R.id.list_routes);
        zmAdaptera = new AdapterRoutes (this, route_name_Array,avr_days_Array);
        zmycustRecycleViewa.setAdapter(zmAdaptera);
        zmycustRecycleViewa.setLayoutManager(new LinearLayoutManager(this));

        cursor.close();
    }

    public void getRoutes(final String repname) {

        RequestQueue requestQueuert = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, EndPoints.GET_ROUTES,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String result) {
                        try {
                            JSONArray jArray = new JSONArray(result);

                            for (int i = 0; i < jArray.length(); i++) {
                                JSONObject d = jArray.getJSONObject(i);
                                JSONObject json_data = jArray.getJSONObject(i);
                                String date_created = json_data.getString("date_created");
                                String route_name = json_data.getString("route_name");
                                String avr_days = json_data.getString("avr_days");
                                String sync_status = json_data.getString("sync_status");
                                String route_status = json_data.getString("route_status");


                                if(route_status.equalsIgnoreCase("done") ){
                                    removeRoute(route_name);
                                }
                                else {
                                    sqLiteDatabase = sqLiteHelperb.getWritableDatabase();
                                    cursor = sqLiteDatabase.rawQuery("SELECT * FROM routes WHERE  route_name ='"+route_name+"'", null);
                                    if (cursor.moveToFirst()) {

                                    }
                                    else{
                                        InsertRoute(date_created,route_name,avr_days);
                                        getRoutesShops(route_name);
                                    }
                                }



                            }
                            sendBroadcast(new Intent(EndPoints.ORDER_SAVED_BROADCAST+"routes"));


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("rep_name", repname);
                return params;
            }
        };

        requestQueuert.add(stringRequest);
    }

    public void InsertRoute(final String date_created, final String route_name, final String avr_days){
        String SQLiteDataBaseQueryHolder = "INSERT INTO routes (date_created, route_name, avr_days, sync_state) VALUES('" + date_created + "','" + route_name + "', '" + avr_days + "', 'synced');";
        sqLiteDatabase.execSQL(SQLiteDataBaseQueryHolder);
        updateRoutes(route_name);

    }
    public void updateRoutes(final String route_name) {
        RequestQueue requestQueuert = Volley.newRequestQueue(Routes.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, EndPoints.UPDATE_ROUTES,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
//                            JSONObject jsonObject = new JSONObject(response);
//                            JSONArray jsonArray = jsonObject.getJSONArray("result");
//                            JSONObject jsonObject1 = jsonArray.getJSONObject(0);
//
//                            String status = jsonObject1.getString("status");



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("route_name", route_name);
                return params;
            }
        };

        requestQueuert.add(stringRequest);
    }
    public void getRoutesShops(final String route_name) {
        RequestQueue requestQueuert = Volley.newRequestQueue(Routes.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, EndPoints.GET_ROUTES_SHOPS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String result) {
                        try {
                            JSONArray jArray = new JSONArray(result);

                            for (int i = 0; i < jArray.length(); i++) {
                                JSONObject d = jArray.getJSONObject(i);
                                JSONObject json_data = jArray.getJSONObject(i);
                                String id = json_data.getString("id");
                                String last_seen = json_data.getString("last_seen");
                                String cust_number = json_data.getString("cust_number");
                                String c_name = json_data.getString("cust_name");
                                String last_order = json_data.getString("last_order");
                                String cust_name = c_name.replace("'","");

                                InsertRouteShops(id,route_name,cust_number,cust_name,last_seen,last_order);
                                //once insterted call http to update route as synced

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("route_name", route_name);
                return params;
            }
        };

        requestQueuert.add(stringRequest);
    }
    public void InsertRouteShops(final String id, final String route_name, final String cust_number, final String cust_name, final String last_seen, final String last_order)
    {
        String SQLiteDataBaseQueryHolder = "INSERT INTO routes_shops (id, route_name, cust_number, cust_name, last_date_seen,last_order_size) VALUES('" +id+ "','" + route_name + "', '" +cust_number+ "', '" +cust_name+ "', '" +last_seen+ "', '" +last_order+ "' );";
        sqLiteDatabase.execSQL(SQLiteDataBaseQueryHolder);

    }
    public void removeRoute(final String route_name){
        sqLiteDatabase.execSQL("DELETE FROM routes WHERE route_name ='"+route_name+"' ");
        sqLiteDatabase.execSQL("DELETE FROM routes_shops WHERE route_name ='"+route_name+"' ");

    }
    @Override
    protected void onStart() {
        super.onStart();

        if (googleApiClient != null) {
            googleApiClient.connect();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!checkPlayServices()) {
            //        SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = shared.edit();
//        editor.putString("LatLong", String.valueOf(latLng));
//        editor.apply();
            Toast.makeText(this, "You need to install Google Play Services to use the App properly", Toast.LENGTH_LONG).show();


        }
    }
    @Override
    protected void onPause() {
        super.onPause();

        // stop location updates
        if (googleApiClient != null  &&  googleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
            googleApiClient.disconnect();
        }
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);

        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST);
            } else {
                finish();
            }

            return false;
        }
        return true;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                &&  ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        // Permissions ok, we get last location
        location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);

        if (location != null) {
            //locationTv.setText("Latitude : " + location.getLatitude() + "\nLongitude : " + location.getLongitude());

            String latLng = location.getLatitude()+","+location.getLongitude();
            SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = shared.edit();
            editor.putString("LatLong", latLng);
            editor.apply();
            //Toast.makeText(this, "Your location is "+latLng, Toast.LENGTH_LONG).show();

        }

        startLocationUpdates();
    }

    private void startLocationUpdates() {

        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(UPDATE_INTERVAL);
        locationRequest.setFastestInterval(FASTEST_INTERVAL);

        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                &&  ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "You need to enable permissions to display location !", Toast.LENGTH_SHORT).show();
        }

        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            //locationTv.setText("Latitude : " + location.getLatitude() + "\nLongitude : " + location.getLongitude());
            String latLng = location.getLatitude()+","+location.getLongitude();
            SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = shared.edit();
            editor.putString("LatLong", latLng);
            editor.apply();
            //Toast.makeText(this, "Your location is "+latLng, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch(requestCode) {
            case ALL_PERMISSIONS_RESULT:
                for (String perm : permissionsToRequest) {
                    if (!hasPermission(perm)) {
                        permissionsRejected.add(perm);
                    }
                }

                if (permissionsRejected.size() > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) {
                            new AlertDialog.Builder(Routes.this).
                                    setMessage("These permissions are mandatory to get your location. You need to allow them.").
                                    setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(permissionsRejected.
                                                        toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    }).setNegativeButton("Cancel", null).create().show();

                            return;
                        }
                    }
                } else {
                    if (googleApiClient != null) {
                        googleApiClient.connect();
                    }
                }

                break;
        }
    }
}
