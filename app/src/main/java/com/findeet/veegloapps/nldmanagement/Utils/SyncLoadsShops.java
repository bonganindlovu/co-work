package com.findeet.veegloapps.nldmanagement.Utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.findeet.veegloapps.nldmanagement.EndPoints;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by DEBONGZ on 8/22/2018.
 */

public class SyncLoadsShops extends BroadcastReceiver {

    //context and database helper object
    private Context context;
    SQLiteDatabase sqLiteDatabase;
    SQLiteHelper sqLiteHelper;
    Cursor cursor;

    @Override
    public void onReceive(Context context, Intent intent) {

        this.context = context;

        sqLiteHelper = new SQLiteHelper(context);
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        //if there is a network
        if (activeNetwork != null) {

            checkLoads();

        }
    }

    public void checkLoads(){
        sqLiteDatabase = sqLiteHelper.getWritableDatabase();
        cursor = sqLiteDatabase.rawQuery("SELECT * FROM loads_shops WHERE sync_status = 'not_synced'  ", null);
        if (cursor.moveToFirst()) {

            do {
                String load_id= cursor.getString(cursor.getColumnIndex("load_id"));
                String shop_id= cursor.getString(cursor.getColumnIndex("shop_id"));
                String shop_name= cursor.getString(cursor.getColumnIndex("shop_name"));
                String invoice_number = cursor.getString(cursor.getColumnIndex("invoice_number"));
                String invoice_amount= cursor.getString(cursor.getColumnIndex("invoice_amount"));


                sendLoads(load_id,shop_id,shop_name,invoice_number,invoice_amount);
            }

            while (cursor.moveToNext());




        }


    }

public void sendLoads(final String load_id,final String shop_id,final String shop_name,final String invoice_number,final String invoice_amount){

    StringRequest stringRequest = new StringRequest(Request.Method.POST, EndPoints.SEND_LOAD_SHOPS,
            new Response.Listener<String>() {
                @Override
                public void onResponse(String result) {
                   // Toast.makeText(context, ""+result, Toast.LENGTH_SHORT).show();
                    try {
                        JSONArray jArray = new JSONArray(result);

                        for (int i = 0; i < jArray.length(); i++) {
                            JSONObject d = jArray.getJSONObject(i);
                            JSONObject json_data = jArray.getJSONObject(i);
                            String invoice_number = json_data.getString("invoice_number");

                            updateSync(invoice_number);

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();

                    }
                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }) {
        @Override
        protected Map<String, String> getParams() throws AuthFailureError {
            Map<String, String> params = new HashMap<>();
            params.put("load_id", load_id);
            params.put("shop_id", shop_id);
            params.put("shop_name", shop_name);
            params.put("invoice_number", invoice_number);
            params.put("invoice_amount", invoice_amount);
            return params;
        }
    };
    VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
}

    public void updateSync(final String invoice_number)
    {
        String SQLiteDataBaseQueryHolder = "UPDATE loads_shops SET sync_status ='synced' WHERE invoice_number='"+invoice_number+"' ";
        sqLiteDatabase.execSQL(SQLiteDataBaseQueryHolder);
        context.sendBroadcast(new Intent(EndPoints.ORDER_SAVED_BROADCAST+"shops"));

    }
}
