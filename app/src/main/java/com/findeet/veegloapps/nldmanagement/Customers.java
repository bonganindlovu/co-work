package com.findeet.veegloapps.nldmanagement;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.findeet.veegloapps.nldmanagement.Utils.DBController;
import com.findeet.veegloapps.nldmanagement.Utils.NetworkStateChecker;
import com.findeet.veegloapps.nldmanagement.Utils.SQLiteHelper;
import com.findeet.veegloapps.nldmanagement.adapters.AdapterCustomers;

import java.util.ArrayList;

public class Customers extends AppCompatActivity {
    // CONNECTION_TIMEOUT and READ_TIMEOUT are in milliseconds
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 15000;
    private RecyclerView mycustRecycleView;
    private AdapterCustomers mAdapter;
    ProgressBar loadcustbar;
    TextView search;
    DBController controller = new DBController(this);
    SearchView searchView = null;
    SQLiteDatabase sqLiteDatabase;
    ArrayList<String> cust_number_Array;
    ArrayList<String> cust_NAME_Array;
    ArrayList<String> custAddr_Array;
    FloatingActionButton add_new_customer;
    private BroadcastReceiver broadcastReceiver;
    android.support.v7.widget.Toolbar customers_toolbar;
    SQLiteHelper sqLiteHelper;
    Cursor cursor;

    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customers);
        loadcustbar = (ProgressBar)findViewById(R.id.load_re);
        search = (TextView)findViewById(R.id.serach_cust_txt);
        sqLiteHelper = new SQLiteHelper(this);
        add_new_customer = (FloatingActionButton)findViewById(R.id.add_new_customer);

        customers_toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.shop_toolbar);
        setSupportActionBar(customers_toolbar);
        SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
        String repname =shared.getString("user_name", "");
        ActionBar mybar = getSupportActionBar();
        mybar.setTitle("");


        cust_number_Array = new ArrayList<String>();
        cust_NAME_Array = new ArrayList<String>();
        custAddr_Array = new ArrayList<String>();
        add_new_customer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(Customers.this, CreateNewCustomer.class);
                startActivity(intent);
                //getActivity().finish();
            }
        });
        ShowSQLiteDBdata() ;
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                //Updating Array Adapter ListView after typing inside EditText.
                String e = search.getText().toString();
                ShowSQLiteDBdata() ;
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            ShowSQLiteDBdata() ;
            }

            @Override
            public void afterTextChanged(Editable editable) {
                ShowSQLiteDBdata() ;
            }
        });

        //the broadcast receiver to update sync status
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                //loading the names again
                ShowSQLiteDBdata() ;
            }
        };

        //registering the broadcast receiver to update sync status
        registerReceiver(broadcastReceiver, new IntentFilter(EndPoints.CUSTOMER_SAVED_BROADCAST));

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.refresh_sql, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.refresh:
                new AlertDialog.Builder(Customers.this)
                        .setTitle("Alert")
                        .setMessage("Warning, pending customers might be permanently erased! \n To avoid this please ask your admin to approve all your customers before you continue")
                        .setCancelable(true)
                        .setPositiveButton("CONTINUE", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                final Intent intent = new Intent(Customers.this, SaveSqlCustomers.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        })
                        .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.cancel();
                            }
                        })
                        .show();
                break;
            default:
                break;
        }
        return true;
    }

    private void ShowSQLiteDBdata() {

        sqLiteDatabase = sqLiteHelper.getWritableDatabase();
        // List<DataCustomers> data=new ArrayList<>();

        String value = "%" + search.getText().toString() + "%";
        String value2 = search.getText().toString();

        cursor = sqLiteDatabase.rawQuery("SELECT * FROM "+SQLiteHelper.TABLE_NAME+" WHERE "+SQLiteHelper.Table_Column_1_Subject_Cname+" LIKE '"+value+"' OR cust_number = '"+value2+"' ", null);
        cust_number_Array.clear();
        cust_NAME_Array.clear();
        custAddr_Array.clear();
//        cursor.getColumnCount();

        if (cursor.moveToFirst()) {
            do {
                cust_number_Array.add(cursor.getString(cursor.getColumnIndex(SQLiteHelper.Table_Column_1_Subject_Cnumber)));
                cust_NAME_Array.add(cursor.getString(cursor.getColumnIndex(SQLiteHelper.Table_Column_1_Subject_Cname)));
                custAddr_Array.add(cursor.getString(cursor.getColumnIndex(SQLiteHelper.Table_Column_1_Subject_Caddr)));
            }

            while (cursor.moveToNext());
        }

        mycustRecycleView = (RecyclerView) findViewById(R.id.list_view);
        mAdapter = new AdapterCustomers(Customers.this, cust_number_Array,cust_NAME_Array,custAddr_Array);
        mycustRecycleView.setAdapter(mAdapter);
        mycustRecycleView.setLayoutManager(new LinearLayoutManager(Customers.this));

        cursor.close();
    }

    @Override
    protected void onResume() {
        registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        ShowSQLiteDBdata() ;

        super.onResume();
    }
}
