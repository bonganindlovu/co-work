package com.findeet.veegloapps.nldmanagement.Utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.findeet.veegloapps.nldmanagement.EndPoints;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by DEBONGZ on 8/22/2018.
 */

public class CustomerFeedBack extends BroadcastReceiver {

    //context and database helper object
    private Context context;
    SQLiteDatabase sqLiteDatabase;
    SQLiteHelper sqLiteHelper;
    Cursor cursor;
    String url = "http://nextld.co.za/salesadmin/app/add_feedback.php";

    @Override
    public void onReceive(Context context, Intent intent) {

        this.context = context;

        sqLiteHelper = new SQLiteHelper(context);
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        //if there is a network
        if (activeNetwork != null) {
            //if connected to wifi or mobile data plan
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI || activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                ShowSQLiteDBdata();
            }
        }
    }
    private void ShowSQLiteDBdata() {

        sqLiteDatabase = sqLiteHelper.getWritableDatabase();
        String value ="synced";
        cursor = sqLiteDatabase.rawQuery("SELECT * FROM routes_shops WHERE sync_state != '"+value+"'  ", null);

        int count = 0;
        if (cursor.moveToFirst()) {
            do {
                String route_name =cursor.getString(cursor.getColumnIndex("route_name"));
                String cust_number =cursor.getString(cursor.getColumnIndex("cust_number"));
                String last_person_seen =cursor.getString(cursor.getColumnIndex("last_person_seen"));
                String last_message =cursor.getString(cursor.getColumnIndex("last_message"));
                String latLong =cursor.getString(cursor.getColumnIndex("lat_long"));
                POSTITEM(route_name, cust_number, last_person_seen, last_message,latLong);
                count+=count+1;

            }

            while (cursor.moveToNext());
        }

        int rowCount = count;
        //Toast.makeText(context, "" + rowCount, Toast.LENGTH_SHORT).show();
    }


    /*
    * method taking two arguments
    * name that is to be saved and id of the name from SQLite
    * if the name is successfully sent
    * we will update the status as synced in SQLite
    * */

    public void updateSyncStatus(final String route_name,final String cust_number){
        sqLiteDatabase.execSQL("UPDATE routes_shops SET sync_state ='synced' WHERE route_name ='"+route_name+"' AND cust_number ='"+cust_number+"' ");
    }

    public void POSTITEM(final String route_name, final String cust_number, final String last_person_seen, final String last_message, final String latLong) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("result");
                            JSONObject jsonObject1 = jsonArray.getJSONObject(0);
                            String status = jsonObject1.getString("status");


                            if(status.equalsIgnoreCase("passed")) {
                            //update synce status
                                updateSyncStatus(route_name,cust_number);
                                //context.sendBroadcast(new Intent(EndPoints.ITEM_SAVED_BROADCAST));
                            }
                          else{}
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("route_name",route_name);
                params.put("cust_number",cust_number);
                params.put("last_person_seen",last_person_seen);
                params.put("last_message",last_message);
                params.put("lat_long",latLong);
                return params;
                //route_name, cust_number, last_person_seen, last_message
            }
        };

        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }
}
