package com.findeet.veegloapps.nldmanagement.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.findeet.veegloapps.nldmanagement.Dispatch;
import com.findeet.veegloapps.nldmanagement.DispatchLoads;
import com.findeet.veegloapps.nldmanagement.Order;
import com.findeet.veegloapps.nldmanagement.OrderItem;
import com.findeet.veegloapps.nldmanagement.R;
import com.findeet.veegloapps.nldmanagement.Utils.SQLiteHelper;
import com.findeet.veegloapps.nldmanagement.Utils.SQLiteHelperb;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by DEBONGZ on 1/6/2018.
 */

public class AdapterDispatch extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

//sync_state_Array,load_time_Array

    ArrayList<String> pload_id;
    ArrayList<String> pvehicle_name;
    ArrayList<String> pdriver_name;
    ArrayList<String> psync_state;
    ArrayList<String> pload_time;
    SQLiteDatabase sqLiteDatabase;
    SQLiteHelperb sqLiteHelperb;
    Cursor cursor;

    private Context context;
    private LayoutInflater inflater;
//    List<DataProducts> data= Collections.emptyList();
//    DataProducts current;
    int currentPos=0;



    public AdapterDispatch(Context context,
                           ArrayList<String> load_id,
                           ArrayList<String> vehicle_name,
                           ArrayList<String> driver_name,
                           ArrayList<String> sync_state,
                           ArrayList<String> load_time){
        this.context=context;
        inflater= LayoutInflater.from(context);
        //this.data=data;
        this.pload_id = load_id;
        this.pvehicle_name = vehicle_name;
        this.pdriver_name = driver_name;
        this.psync_state = sync_state;
        this.pload_time = load_time;
        sqLiteHelperb = new SQLiteHelperb(context);

    }



    // Inflate the layout when ViewHolder created
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=inflater.inflate(R.layout.container_quotes, parent,false);
        MyHolder holder=new MyHolder(view);

        return holder;
    }

    // Bind data
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        // Get current position of item in RecyclerView to bind data and assign values from list
        final MyHolder myHolder= (MyHolder) holder;
        //DataProducts current=data.get(position);
        SQLiteDataBaseBuild();
        sqLiteDatabase = sqLiteHelperb.getWritableDatabase();
        cursor = sqLiteDatabase.rawQuery("SELECT * FROM loads_shops WHERE load_id = '"+pload_id.get(position)+"' ", null);

        int count = 0;
        if (cursor.moveToFirst()) {
            do {
                count = count++;

            }

            while (cursor.moveToNext());
        }

        //Toast.makeText(context, ""+count, Toast.LENGTH_SHORT).show();

        String loads = String.valueOf(cursor.getCount() );
        long unixSeconds = Long.parseLong(pload_time.get(position));



// convert seconds to milliseconds
        Date date = new java.util.Date(unixSeconds*1000L);
// the format of your date
        SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm");
// give a timezone reference for formatting (see comment at the bottom)
        sdf.setTimeZone(TimeZone.getDefault());

        String formattedDate = sdf.format(date);
        myHolder.txtQcustName.setText(pvehicle_name.get(position)+" ("+pdriver_name.get(position)+")");
        myHolder.txtQorderTotal.setText(loads);
        myHolder.txtQOrderDate.setText(formattedDate);
        myHolder.txtQOrderNum.setText(pload_id.get(position));
        myHolder.txtQCustAddr.setText("");
        String synce_status = psync_state.get(position);

        if (synce_status.equalsIgnoreCase("not_synced")){
            myHolder.Rsent_badge.setVisibility(View.GONE);
        }
        else{
            myHolder.Rsent_badge.setVisibility(View.VISIBLE);
        }
        final String load_id =  pload_id.get(position);
        final String vehicle_name = pvehicle_name.get(position)+" ("+pdriver_name.get(position)+")";

        myHolder.oder_line.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //Toast.makeText(context, ""+load_id, Toast.LENGTH_SHORT).show();

                Intent intent= new Intent(context, DispatchLoads.class);
                intent.putExtra("load_id", load_id);
                intent.putExtra("vehicle_name", vehicle_name);
                context.startActivity(intent);

            }
        });


    }
    // return total item from List
    @Override
    public int getItemCount() {
        return pload_id.size();
    }



    class MyHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView txtQcustName;
        TextView txtQorderTotal;
        TextView txtQOrderDate;
        TextView txtQOrderNum;
        TextView txtQCustAddr;
        RelativeLayout Porder_sync_prog;
        RelativeLayout Rsent_badge,oder_line;

        // create constructor to get widget reference
        public MyHolder(View itemViewa) {
            super(itemViewa);
            txtQcustName = (TextView) itemViewa.findViewById(R.id.txtqname);
            txtQorderTotal = (TextView) itemViewa.findViewById(R.id.txtqtotal);
            txtQOrderDate = (TextView) itemViewa.findViewById(R.id.txtqdate);
            txtQOrderNum = (TextView) itemViewa.findViewById(R.id.txtqid);
            txtQCustAddr = (TextView) itemViewa.findViewById(R.id.txtqcustAddr);
            Porder_sync_prog =(RelativeLayout)itemViewa.findViewById(R.id.order_sync_status);
            Rsent_badge =(RelativeLayout) itemViewa.findViewById(R.id.sent_badge);
            oder_line =(RelativeLayout) itemViewa.findViewById(R.id.quote_line);
            itemViewa.setOnClickListener(this);
        }



        // Click event for all items
        @Override
        public void onClick(View v) {



        }
    }

    public void SQLiteDataBaseBuild(){

        sqLiteDatabase = context.openOrCreateDatabase(SQLiteHelper.DATABASE_NAME, Context.MODE_PRIVATE, null);

    }

}
