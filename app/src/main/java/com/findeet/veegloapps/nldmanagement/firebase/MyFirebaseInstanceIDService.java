package com.findeet.veegloapps.nldmanagement.firebase;//package com.findeet.veegloapps.findeet.firebase;
//
//import android.util.Log;
//
//import com.google.firebase.iid.FirebaseInstanceId;
//import com.google.firebase.iid.FirebaseInstanceIdService;
//
///**
// * Created by DEBONGZ on 3/14/2018.
// */
//
//public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
//    private static final String TAG = "MyFirebaseIIDService";
//
//    @Override
//    public void onTokenRefresh() {
//
//        //Getting registration token
//        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
//
//        //Displaying token on logcat
//        Log.d(TAG, "Refreshed token: " + refreshedToken);
//
//        //calling the method store token and passing token
//        storeToken(refreshedToken);
//    }
//
//    public void storeToken(String token) {
//        //we will save the token in sharedpreferences later
//        //saving the token on shared preferences
//        SharedPrefManager.getInstance(getApplicationContext()).saveDeviceToken(token);
//    }
//
//}
