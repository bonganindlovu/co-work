package com.findeet.veegloapps.nldmanagement;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.findeet.veegloapps.nldmanagement.Utils.SQLiteHelperb;
import com.findeet.veegloapps.nldmanagement.Utils.VolleySingleton;
import com.findeet.veegloapps.nldmanagement.adapters.AdapterDelLoads;
import com.findeet.veegloapps.nldmanagement.adapters.AdapterDispatchLoads;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class DeliveryShops extends AppCompatActivity {
    android.support.v7.widget.Toolbar toolbar_loads;
    SQLiteDatabase sqLiteDatabase;
    SQLiteHelperb sqLiteHelperb;
    /*TextView noLoadShops; */
    private RecyclerView zmycustRecycleViewa;
    private AdapterDelLoads zmAdaptera;
    Cursor cursor;
    BroadcastReceiver broadcastReceiver;
    ArrayList<String> id_Array,shop_name_Array,list_order_Array,shop_adress_Array,shop_lat_Array,shop_long_Array,payment_status_Array;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_shops);

        SQLiteDataBaseBuild();
        sqLiteHelperb = new SQLiteHelperb(this);
        Intent in = getIntent();
        String load_id= in.getExtras().getString("load_id");
        String vehicle_name= in.getExtras().getString("vehicle_name");

        toolbar_loads = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar_deliver);
        setSupportActionBar(toolbar_loads);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(vehicle_name);

        id_Array = new ArrayList<String>();
        list_order_Array = new ArrayList<String>();
        shop_adress_Array = new ArrayList<String>();
        shop_name_Array = new ArrayList<String>();
        shop_lat_Array = new ArrayList<String>();
        shop_long_Array = new ArrayList<String>();
        payment_status_Array = new ArrayList<String>();

        String sort ="DESC";
        checkLoads(load_id,sort);
        fetchLoadShops(load_id);

        //the broadcast receiver to update sync status
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Intent in = getIntent();
                String load_id= in.getExtras().getString("load_id");
                //loading the names again
                fetchLoadShops(load_id);
            }
        };


        //registering the broadcast receiver to update sync status
        registerReceiver(broadcastReceiver, new IntentFilter(EndPoints.ORDER_SAVED_BROADCAST+"delSop"));


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.sort_loads, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.first:
                Intent in = getIntent();
                String load_id= in.getExtras().getString("load_id");
                String sort ="ASC";
                checkLoads(load_id,sort);
                fetchLoadShops(load_id);
                Toast.makeText(DeliveryShops.this, "Refreshed !", Toast.LENGTH_SHORT).show();
                break;
             case R.id.last:
                 in = getIntent();
                 load_id= in.getExtras().getString("load_id");
                 sort ="DESC";
                 checkLoads(load_id,sort);
                 fetchLoadShops(load_id);
                 Toast.makeText(DeliveryShops.this, "Refreshed !", Toast.LENGTH_SHORT).show();
                break;
                default:
                break;
        }
        return true;
    }
    private void SQLiteDataBaseBuild() {

    }
    public void fetchLoadShops(final String load_idmy){
        sqLiteDatabase = sqLiteHelperb.getWritableDatabase();
        cursor = sqLiteDatabase.rawQuery("SELECT * FROM driver_shops WHERE load_id ='"+load_idmy+"'  ORDER BY list_order ASC ", null);
        id_Array.clear();
        list_order_Array.clear();
        shop_adress_Array.clear();
        shop_name_Array.clear();
        shop_lat_Array.clear();
        shop_long_Array.clear();
        payment_status_Array.clear();

        if (cursor.moveToFirst()) {
            do {
                id_Array.add(cursor.getString(cursor.getColumnIndex("id")));
                shop_adress_Array.add(cursor.getString(cursor.getColumnIndex("shop_adress")));
                list_order_Array.add(cursor.getString(cursor.getColumnIndex("list_order")));
                shop_name_Array.add(cursor.getString(cursor.getColumnIndex("shop_name")));
                shop_lat_Array.add(cursor.getString(cursor.getColumnIndex("shop_lat")));
                shop_long_Array.add(cursor.getString(cursor.getColumnIndex("shop_long")));

            }

            while (cursor.moveToNext());
        }
        else{
            //noLoadShops.setVisibility(View.VISIBLE);
        }
    //list_order_Array,shop_adress_Array,shop_lat_Array,shop_long_Array,amount_paid_Array;

        zmycustRecycleViewa = (RecyclerView) findViewById(R.id.delshopRecycle);
        zmAdaptera = new AdapterDelLoads(DeliveryShops.this, id_Array,list_order_Array,shop_adress_Array,shop_name_Array,shop_lat_Array,shop_long_Array, payment_status_Array);
        zmycustRecycleViewa.setAdapter(zmAdaptera);
        zmycustRecycleViewa.setLayoutManager(new LinearLayoutManager(DeliveryShops.this));

        cursor.close();
    }
    private void checkLoads(final String load_idmy ,final String load_sort) {
        RequestQueue requestQueuemom = Volley.newRequestQueue(this);

        StringRequest stringRequestabc = new StringRequest(Request.Method.GET, EndPoints.GET_DEL_SHOPS+"?load_id="+load_idmy+"&load_sort="+load_sort,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String result) {
                        //Toast.makeText(DeliveryShops.this, "Res ="+result, Toast.LENGTH_SHORT).show();
                        try {

                            JSONArray jArray = new JSONArray(result);
                          for (int i = 0; i < jArray.length(); i++) {
                                JSONObject json_data = jArray.getJSONObject(i);
                                //proData.user_id = json_data.getString("user_id");
                                String load_id = json_data.getString("load_id");
                                String id = json_data.getString("id");
                                String list_order = json_data.getString("list_order");
                                String shop_name = json_data.getString("shop_name");
                                String shop_adress = json_data.getString("shop_addr");
                                String shop_lat = json_data.getString("shop_lat");
                                String shop_long = json_data.getString("shop_long");

                                sqLiteDatabase = sqLiteHelperb.getWritableDatabase();
                                cursor = sqLiteDatabase.rawQuery("SELECT * FROM driver_shops WHERE id = '"+id+"'  ", null);
                                if (cursor.moveToFirst()) {
                                    UpdateOn( id,list_order);

                                }
                                else{
                                    //id ,list_order,shop_name,shop_adress,shop_lat,shop_long,payment_status
                                    AddLoadOn(id,list_order,shop_name,shop_adress,shop_lat,shop_long,load_id)  ;
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {

        };
        VolleySingleton.getInstance(this).addToRequestQueue(stringRequestabc);
    }
    public void AddLoadOn(final String id,final String list_order,final String shop_name,final String shop_adress,final String shop_lat,final String shop_long,final String load_id){
        sqLiteDatabase.beginTransaction(); //getTimestamp
        String database = "driver_shops";


        String SQLiteDataBaseQueryHolder = "INSERT INTO "+database+" (id,load_id,list_order,shop_name,shop_adress,shop_lat,shop_long" +
                ") VALUES('"+id+"','"+load_id+"', '"+list_order+"', '"+shop_name+"', '"+shop_adress+"','"+shop_lat+"','"+shop_long+"');";
        sqLiteDatabase.execSQL(SQLiteDataBaseQueryHolder);
        sqLiteDatabase.setTransactionSuccessful();
        sqLiteDatabase.endTransaction();
        sendBroadcast(new Intent(EndPoints.ORDER_SAVED_BROADCAST+"delSop"));
    }
    public void UpdateOn(final String id,final String list_order){
       // sqLiteDatabase.execSQL("UPDATE driver_shops SET list_order ='"+list_order+"' WHERE id ='"+id+"' ");
    }
}
