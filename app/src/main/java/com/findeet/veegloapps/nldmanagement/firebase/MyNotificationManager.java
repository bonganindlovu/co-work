package com.findeet.veegloapps.nldmanagement.firebase;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.text.Html;

import com.findeet.veegloapps.nldmanagement.R;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import static com.findeet.veegloapps.nldmanagement.firebase.Constants.CHANNEL_ID;

/**
 * Created by DEBONGZ on 3/14/2018.
 */

public class MyNotificationManager {
    public static final int ID_BIG_NOTIFICATION = (int) System.currentTimeMillis();
    public static final int ID_SMALL_NOTIFICATION = (int) System.currentTimeMillis();

    private Context mCtx;

    public MyNotificationManager(Context mCtx) {
        this.mCtx = mCtx;
    }

    //the method will show a big notification with an image
    //parameters are title for message title, message for message text, url of the big image and an intent that will open
    //when you will tap on the notification
    public void showBigNotification(String title, String message, String url, Intent intent) {
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        mCtx,
                        ID_BIG_NOTIFICATION,
                        intent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        SharedPreferences setshared =  mCtx.getSharedPreferences("FindiT_Settings", Context.MODE_PRIVATE);
        String message_vibrate = setshared.getString("message_vibrate", "");
        int defaults = 0;

        if (message_vibrate.equalsIgnoreCase("on")){

            defaults = defaults | Notification.DEFAULT_LIGHTS;
            defaults = defaults | Notification.DEFAULT_VIBRATE;
        }
        else{defaults = defaults | Notification.DEFAULT_LIGHTS;}



        NotificationCompat.BigPictureStyle bigPictureStyle = new NotificationCompat.BigPictureStyle();
        bigPictureStyle.setBigContentTitle(title);
        bigPictureStyle.setSummaryText(Html.fromHtml(message).toString());
        //bigPictureStyle.bigPicture(getBitmapFromURL(url));
        NotificationManager notificationManager = (NotificationManager) mCtx.getSystemService(Context.NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {


            String CHANNEL_ID = "my_channel_01";
            CharSequence name = "my_channel";
            String Description = "This is my channel";
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            mChannel.setShowBadge(true);
            notificationManager.createNotificationChannel(mChannel);
        }
       setshared =  mCtx.getSharedPreferences("FindiT_Settings", Context.MODE_PRIVATE);
        String message_sound = setshared.getString("message_sound", "");


        Uri alarmSound = Uri.parse("");
        if (message_sound.equalsIgnoreCase("on")){
            alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        }
        else{alarmSound = Uri.parse("");}

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mCtx,CHANNEL_ID);
        Notification notification;
        notification = mBuilder.setSmallIcon(R.mipmap.ic_launcher).setTicker(title).setWhen(System.currentTimeMillis())
                .setContentIntent(resultPendingIntent)
                .setContentTitle(title)
                .setShowWhen(true)
                .setAutoCancel(true)
                //.setBadgeIconType()
                .setGroupSummary(true)
                //.setStyle(bigPictureStyle)
                //.setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL)
                //.setSmallIcon(R.drawable.ic_launcher)
                .setLargeIcon(getBitmapFromURL(url))
                .setContentText(message)
                .setGroup("chat")
                .setPriority(Notification.PRIORITY_HIGH)
                .setOngoing(false)
                .setDefaults(defaults)
                .setSound(alarmSound)
                .setTimeoutAfter(60000)
                .build();



        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notificationManager.notify(ID_BIG_NOTIFICATION, notification);



    }

    //the method will show a small notification
    //parameters are title for message title, message for message text and an intent that will open
    //when you will tap on the notification
    public void showSmallNotification(String title, String message, Intent intent) {
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        mCtx,
                        ID_SMALL_NOTIFICATION,
                        intent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        int defaults = 0;
        defaults = defaults | Notification.DEFAULT_LIGHTS;
        defaults = defaults | Notification.DEFAULT_VIBRATE;

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mCtx);
        Notification notification;
        notification = mBuilder.setDefaults(defaults)
                .build();

        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        NotificationManager notificationManager = (NotificationManager) mCtx.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(ID_SMALL_NOTIFICATION, notification);
    }

    //The method will return Bitmap from an image URL
    private Bitmap getBitmapFromURL(String strURL) {
        try {
            URL url = new URL(strURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
    public void getImage(){
int mu = (R.mipmap.ic_launcher);
    }
    private int getNotificationIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.drawable.badge_background : R.mipmap.ic_launcher;
    }

    /**
     * Playing notification sound
     */
    public void playNotificationSound2() {
        try {
            Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                    + "://" + mCtx.getPackageName() + "/raw/facebook_messenger");
            //Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(mCtx, alarmSound);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void playNotificationSoundMain() {
        try {
            Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(mCtx, alarmSound);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}