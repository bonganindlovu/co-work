package com.findeet.veegloapps.nldmanagement.Utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.findeet.veegloapps.nldmanagement.EndPoints;
import com.findeet.veegloapps.nldmanagement.Orders;
import com.findeet.veegloapps.nldmanagement.Routes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by DEBONGZ on 8/22/2018.
 */

public class SalesOrderItemsSQL extends BroadcastReceiver {

    //context and database helper object
    private Context context;
    SQLiteDatabase sqLiteDatabase;
    SQLiteHelper sqLiteHelper;
    Cursor cursor;
    String url = "http://nextld.co.za/salesadmin/app/add_item.php";

    @Override
    public void onReceive(Context context, Intent intent) {

        this.context = context;

        sqLiteHelper = new SQLiteHelper(context);
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        //if there is a network
        if (activeNetwork != null) {
            //if connected to wifi or mobile data plan
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI || activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                ShowSQLiteDBdata();
            }
        }
    }
    private void ShowSQLiteDBdata() {

        sqLiteDatabase = sqLiteHelper.getWritableDatabase();
        String value ="not_synced";
        cursor = sqLiteDatabase.rawQuery("SELECT * FROM sales_order_items WHERE sync_state = '"+value+"'  ", null);

        int count = 0;
        if (cursor.moveToFirst()) {
            do {
                String seorderid = cursor.getString(cursor.getColumnIndex("order_id"));
                String seitemnum = cursor.getString(cursor.getColumnIndex("item_number"));
                String seitemname = cursor.getString(cursor.getColumnIndex("item_name"));
                String seitemunit = cursor.getString(cursor.getColumnIndex("item_unit"));
                String seitemquantity = cursor.getString(cursor.getColumnIndex("item_quantity"));
                POSTITEM(seorderid, seitemnum, seitemname, seitemquantity, seitemunit);
                count+=count+1;

            }

            while (cursor.moveToNext());
        }

        int rowCount = count;
        //Toast.makeText(context, "" + rowCount, Toast.LENGTH_SHORT).show();
    }


    /*
    * method taking two arguments
    * name that is to be saved and id of the name from SQLite
    * if the name is successfully sent
    * we will update the status as synced in SQLite
    * */

    public void updateSyncStatus(final String order_id,final String itemnumber,final String item_total){
        sqLiteDatabase.execSQL("UPDATE sales_order_items SET sync_state ='synced', item_total ='"+item_total+"' WHERE order_id ='"+order_id+"' AND item_number ='"+itemnumber+"'");
    }

    public void POSTITEM(final String seorderid, final String seitemnum, final String seitemname, final String seitemquantity, final String seitemunit) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            //Toast.makeText(context, ""+response, Toast.LENGTH_SHORT).show();
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("result");
                            JSONObject jsonObject1 = jsonArray.getJSONObject(0);
                            String status = jsonObject1.getString("status");
                            String message = jsonObject1.getString("message");
                            String item_total = jsonObject1.getString("item_total");


                            if(status.equalsIgnoreCase("passed")) {
                            //update synce status
                                updateSyncStatus(seorderid,seitemnum,item_total);
                                context.sendBroadcast(new Intent(EndPoints.ITEM_SAVED_BROADCAST));
                            }
                          else{}
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Toast.makeText(context, ""+error, Toast.LENGTH_SHORT).show();

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("order_id",seorderid);
                params.put("item_number",seitemnum);
                params.put("item_name",seitemname);
                params.put("item_unit",seitemunit);
                params.put("item_quantity",seitemquantity);
                return params;
            }
        };

        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }
}
