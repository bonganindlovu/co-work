package com.findeet.veegloapps.nldmanagement.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.findeet.veegloapps.nldmanagement.OrderItem;
import com.findeet.veegloapps.nldmanagement.R;
import com.findeet.veegloapps.nldmanagement.datas.DataOrder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by DEBONGZ on 1/6/2018.
 */

public class AdapterOrder extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private LayoutInflater inflater;

    ArrayList<String> oitem_name;
    ArrayList<String> oitem_unit;
    ArrayList<String> oitem_quantity;
    ArrayList<String> oitem_total;
    ArrayList<String> oitem_number;
    ArrayList<String> oitem_synce_state;

    int currentPos=0;

    // create constructor to initialize context and data sent from MainActivity
    public AdapterOrder(Context context, ArrayList<String> item_name, ArrayList<String> item_unit, ArrayList<String>
            item_quantity, ArrayList<String> item_total, ArrayList<String> item_number, ArrayList<String> item_synce_state){
        this.context=context;
        inflater= LayoutInflater.from(context);
        this.oitem_name = item_name;
        this.oitem_unit = item_unit;
        this.oitem_quantity = item_quantity;
        this.oitem_total = item_total;
        this.oitem_number = item_number;
        this.oitem_synce_state = item_synce_state;
    }

    // Inflate the layout when ViewHolder created
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=inflater.inflate(R.layout.container_orders, parent,false);
        MyHolder holder=new MyHolder(view);

        return holder;
    }

    // Bind data
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        // Get current position of item in RecyclerView to bind data and assign values from list
        MyHolder myHolder= (MyHolder) holder;

        myHolder.txtOname.setText(oitem_name.get(position));
        myHolder.txtOunit.setText(oitem_unit.get(position));
        myHolder.txtOqty.setText(oitem_quantity.get(position));
        myHolder.txtOtotal.setText(oitem_total.get(position));
        myHolder.txtOnum.setText(oitem_number.get(position));
        String synce_status = oitem_synce_state.get(position);
        if (synce_status.equalsIgnoreCase("synced")){
            myHolder.Pitem_sync_prog.setVisibility(View.GONE);
        }
        else{
            myHolder.Pitem_sync_prog.setVisibility(View.VISIBLE);
        }

    }
    // return total item from List
    @Override
    public int getItemCount() {
        return oitem_unit.size();
    }

    class MyHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView txtOname;
        TextView txtOunit;
        TextView txtOqty;
        TextView txtOtotal;
        TextView txtOnum;
        RelativeLayout Pitem_sync_prog;

        // create constructor to get widget reference
        public MyHolder(View itemViewa) {
            super(itemViewa);
            txtOname = (TextView) itemViewa.findViewById(R.id.txtoname);
            txtOunit = (TextView) itemViewa.findViewById(R.id.txtounit);
            txtOqty = (TextView) itemViewa.findViewById(R.id.txtoqty);
            txtOtotal = (TextView) itemViewa.findViewById(R.id.txtototal);
            txtOnum = (TextView) itemViewa.findViewById(R.id.txtonumber);
            Pitem_sync_prog =(RelativeLayout)itemViewa.findViewById(R.id.item_sync_prog);
            itemViewa.setOnClickListener(this);
        }

        // Click event for all items
        @Override
        public void onClick(View v) {



            String item_number = txtOnum.getText().toString();
            String item_name = txtOname.getText().toString();
            String item_unit = txtOunit.getText().toString();
            String item_quantity = txtOqty.getText().toString();

            Intent intent= new Intent(context, OrderItem.class);
            intent.putExtra("item_number", item_number);
            intent.putExtra("item_name", item_name);
            intent.putExtra("item_unit", item_unit);
            intent.putExtra("item_quantity", item_quantity);
            context.startActivity(intent);


// go to the new intent with the shop name and adress, and send it to database to create new invoive
//            Intent intent=new Intent(context.getApplicationContext(), Order.class);
//            intent.putExtra("cust_number", shop_number);
//            context.getApplicationContext().startActivity(intent);


        }
    }


}
