package com.findeet.veegloapps.nldmanagement.picker.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.findeet.veegloapps.nldmanagement.Order;
import com.findeet.veegloapps.nldmanagement.R;
import com.findeet.veegloapps.nldmanagement.datas.DataOpen;

import java.util.Collections;
import java.util.List;

/**
 * Created by DEBONGZ on 4/19/2018.
 */

public class AdapterOpenO extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private LayoutInflater inflater;
    List<DataOpen> data= Collections.emptyList();
    DataOpen current;
    int currentPos=0;

    // create constructor to initialize context and data sent from MainActivity
    public AdapterOpenO(Context context, List<DataOpen> data){
        this.context=context;
        inflater= LayoutInflater.from(context);
        this.data=data;
    }
    // Inflate the layout when ViewHolder created
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=inflater.inflate(R.layout.container_quotes2, parent,false);
        MyHolder holder=new MyHolder(view);
        return holder;
    }
    // Bind data
    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        // Get current position of item in RecyclerView to bind data and assign values from list
        MyHolder myHolder= (MyHolder) holder;
        final DataOpen current=data.get(position);

        myHolder.txtQnum.setText(current.order_total);
        myHolder.txtQid.setText(current.order_id);
        myHolder.txtQshop_name.setText("["+current.order_block+"] "+current.order_cust_name);
        myHolder.txtStatus.setText(current.order_cust_addr);
        myHolder.txtQdate.setText(current.order_quote_number);
        //myHolder.txtQdate_required.setText(current.order_date_required);

        myHolder.quote_line.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String shop_number = "";
                String shop_name = current.order_cust_name;
                String shop_addr = current.order_cust_addr;
                String uniqueID = current.order_id;

                SharedPreferences shared = context.getSharedPreferences("Order", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = shared.edit();
                editor.putString("order_id", uniqueID);
                editor.putString("cust_number", shop_number);
                editor.putString("cust_name", shop_name);
                editor.putString("cust_addr", shop_addr);
                editor.commit();


                Intent intent= new Intent(context, com.findeet.veegloapps.nldmanagement.picker.Order.class);
                context.startActivity(intent);
                //finish this
                ((Activity)context).finish();


            }
        });




    }
    // return total item from List
    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyHolder extends RecyclerView.ViewHolder  {
        RelativeLayout quote_line;
        TextView txtQnum;
        TextView txtQshop_name;
        TextView txtQid;
        TextView txtStatus;
        TextView txtQdate;
        //TextView txtQdate_required;




        // create constructor to get widget reference
        public MyHolder(View itemView) {
            super(itemView);
            quote_line=  (RelativeLayout)  itemView.findViewById(R.id.quote_line);
            txtQnum = (TextView) itemView.findViewById(R.id.txtqtotal);
            txtQshop_name = (TextView) itemView.findViewById(R.id.txtqname);
            txtStatus = (TextView) itemView.findViewById(R.id.txtqstatus);
            txtQdate = (TextView) itemView.findViewById(R.id.txtqdate);
            txtQid=(TextView) itemView.findViewById(R.id.txtqid);
            //txtQdate_required = (TextView) itemView.findViewById(R.id.likes_user_txt);
        }


    }


}
