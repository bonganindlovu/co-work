package com.findeet.veegloapps.nldmanagement;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.findeet.veegloapps.nldmanagement.Utils.SQLiteHelper;
import com.findeet.veegloapps.nldmanagement.Utils.SQLiteHelperb;
import com.findeet.veegloapps.nldmanagement.Utils.SalesOrderItemsSQL;
import com.findeet.veegloapps.nldmanagement.adapters.AdapterOrder;
import com.findeet.veegloapps.nldmanagement.adapters.AdapterProducts;
import com.findeet.veegloapps.nldmanagement.adapters.AdapterProductsBar;

import java.util.ArrayList;

public class ChoseBarItem extends AppCompatActivity {
    TextView cust_name,shop_addr,sales_person,save_quote;
    ProgressBar loadprobar,loaditem;
    LinearLayout notdiv;
    private RecyclerView mycustRecycleViewa,zmycustRecycleViewa;
    private AdapterProductsBar mAdaptera;
    private AdapterOrder zmAdaptera;

    EditText editText;
    SQLiteDatabase sqLiteDatabase;
    ArrayList<String> p_name_Array,p_id_NAME_Array,p_unit_Array;
    private BroadcastReceiver broadcastReceiver;

    ArrayList<String> item_name_Array,item_unit_Array,item_quantity_Array,item_total_Array,item_numberArray,item_synce_state_Array;


    SQLiteHelperb sqLiteHelperb;
    Cursor cursor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_bar);

        sqLiteHelperb = new SQLiteHelperb(this);
        registerReceiver(new SalesOrderItemsSQL(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        //Make call to AsyncTask
        SQLiteDataBaseBuild();
        p_name_Array = new ArrayList<String>();
        p_id_NAME_Array = new ArrayList<String>();
        p_unit_Array = new ArrayList<String>();
//-----------------------------------------------------------------------
        item_name_Array = new ArrayList<String>();
        item_unit_Array = new ArrayList<String>();
        item_quantity_Array = new ArrayList<String>();
        item_total_Array = new ArrayList<String>();
        item_numberArray = new ArrayList<String>();
        item_synce_state_Array= new ArrayList<String>();
//=========================================================================

        editText = (EditText) findViewById(R.id.serach_txt);

//==========================================================================
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                //Updating Array Adapter ListView after typing inside EditText.
                String e = editText.getText().toString();
                ShowSQLiteDBdata() ;


                //new AsyncFetchSearch().execute();



            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                ShowSQLiteDBdata() ;
                //new AsyncFetchSearch().execute();
            }

            @Override
            public void afterTextChanged(Editable editable) {

                //new AsyncFetchSearch().execute();

            }
        });
;
        ShowSQLiteDBdata() ;

    }
    public void SQLiteDataBaseBuild() {
        sqLiteDatabase = openOrCreateDatabase(SQLiteHelper.DATABASE_NAME, Context.MODE_PRIVATE, null);
        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS sales_order (order_id VARCHAR(200),cust_name VARCHAR(100), cust_addr TEXT, time_sent INT(100), date_of_sale VARCHAR(100),order_status VARCHAR(50), sales_rep VARCHAR(100),order_total DOUBLE(13,2),note VARCHAR(50), sync_state VARCHAR(50))");
        //sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS products (id INTEGER, cust_number TEXT, cust_name TEXT, addr_line_one TEXT)");

    }

    private void ShowSQLiteDBdata() {

        sqLiteDatabase = sqLiteHelperb.getWritableDatabase();
        // List<DataCustomers> data=new ArrayList<>();

        String value = "%" + editText.getText().toString() + "%";

        cursor = sqLiteDatabase.rawQuery("SELECT * FROM "+ SQLiteHelperb.TABLE_P_NAME+" WHERE "+SQLiteHelperb.Table_Column_1_Subject_Pname+" LIKE '"+value+"'  ", null);
        p_name_Array.clear();
        p_id_NAME_Array.clear();
        p_unit_Array.clear();

        if (cursor.moveToFirst()) {
            do {

                p_id_NAME_Array.add(cursor.getString(cursor.getColumnIndex(SQLiteHelperb.Table_Column_1_Subject_Pid)));

                p_unit_Array.add(cursor.getString(cursor.getColumnIndex(SQLiteHelperb.Table_Column_1_Subject_Punit)));

                p_name_Array.add(cursor.getString(cursor.getColumnIndex(SQLiteHelperb.Table_Column_1_Subject_Pname)));



            }

            while (cursor.moveToNext());
        }

        // Setup and Handover data to recyclerview
        mycustRecycleViewa = (RecyclerView) findViewById(R.id.product_list);
        mAdaptera = new AdapterProductsBar(ChoseBarItem.this, p_id_NAME_Array,p_name_Array,p_unit_Array);
        mycustRecycleViewa.setAdapter(mAdaptera);
        mycustRecycleViewa.setLayoutManager(new LinearLayoutManager(ChoseBarItem.this));


        cursor.close();
    }

    @Override
    public void onBackPressed() {
        SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = shared.edit();
        editor.putString("thebarcode", "");
        editor.commit();
        super.onBackPressed();
    }

    public void hideThePop() {
        SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = shared.edit();
        editor.putString("thebarcode", "");
        editor.commit();
    }
}
