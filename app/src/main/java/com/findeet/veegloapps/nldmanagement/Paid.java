package com.findeet.veegloapps.nldmanagement;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.findeet.veegloapps.nldmanagement.Utils.SQLiteHelper;
import com.findeet.veegloapps.nldmanagement.Utils.VolleySingleton;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Paid extends AppCompatActivity {
    ImageView invoice_image;
    TextView capture_invoice,save_invoice,image_state;
    Spinner payment_method;
    EditText payment_amount;
    SQLiteDatabase sqLiteDatabase;
    Bitmap bitmap;
    SQLiteHelper sqLiteHelper;
    Cursor cursor;
    private ProgressDialog progressDialog;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paid);
        SQLiteDataBaseBuild();

        capture_invoice = (TextView) findViewById(R.id.capture_invoice);
        save_invoice = (TextView) findViewById(R.id.save_invoice);
        invoice_image = (ImageView) findViewById(R.id.invoice_image);
        image_state= (TextView) findViewById(R.id.image_state);

        payment_method = (Spinner) findViewById(R.id.payment_method);
        payment_amount = (EditText) findViewById(R.id.payment_txt);
        sqLiteHelper = new SQLiteHelper(this);

        capture_invoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CropImage.activity(null).setGuidelines(CropImageView.Guidelines.ON)
                        //.setBorderLineColor(Color.RED)
                        //.setScaleType(FIT_CENTER)

                        .start(Paid.this);

            }
        });


        save_invoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String payment_method_text = String.valueOf(payment_method.getSelectedItem());
                String image_txt = image_state.getText().toString();
                //cap
                if (payment_method_text.equalsIgnoreCase("Select Payment")) {
                    Toast.makeText(Paid.this, "Please select Payment!", Toast.LENGTH_SHORT).show();
                }
                else if(image_txt.equalsIgnoreCase("not_cap")){
                    Toast.makeText(Paid.this, "Invoice not captured", Toast.LENGTH_SHORT).show();
                }

                else {
                    Intent in = getIntent();
                    String order_id = in.getExtras().getString("order_id");
                    String payment_method = payment_method_text;
                    String payment_amount_text = payment_amount.getText().toString();
                    String action = "payment";
                    Bitmap bm=((BitmapDrawable)invoice_image.getDrawable()).getBitmap();
                    ByteArrayOutputStream ba=new ByteArrayOutputStream(  );
                    bm.compress( Bitmap.CompressFormat.JPEG, 100,ba );
                    byte[] by=ba.toByteArray();
                    final String encod= Base64.encodeToString( by,Base64.DEFAULT );


                    final String ConvertImage = encod;

                    upDatePayment(order_id, payment_method, payment_amount_text, action,ConvertImage);


                }
            }
        });
    }

    private void upDatePayment(final String order_id, final String payment_method_txt, final String payment_amount_text, final String action,final  String ConvertImage){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, EndPoints.UPDATE_LOADS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String result) {
                        progressDialog.dismiss();
                        try {
                            JSONArray jArray = new JSONArray(result);
                            Toast.makeText(Paid.this, "Payment status updated!", Toast.LENGTH_SHORT).show();
                            for (int i = 0; i < jArray.length(); i++) {
//                                JSONObject d = jArray.getJSONObject(i);
//                                JSONObject json_data = jArray.getJSONObject(i);
                                //String load_id = json_data.getString("load_id");

                                upDatePaymentOn(order_id,payment_method_txt,payment_amount_text);
                                finish();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(Paid.this, "Payment status update failed ", Toast.LENGTH_SHORT).show();

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("load_id", order_id);
                params.put("payment_method", payment_method_txt);
                params.put("payment_amount", payment_amount_text);
                params.put("action", action);
                params.put("image_path", ConvertImage);

                return params;
            }
        };
        VolleySingleton.getInstance(Paid.this).addToRequestQueue(stringRequest);
        progressDialog = new ProgressDialog(Paid.this);
        progressDialog.setMessage("Saving...");
        progressDialog.show();

    }
    private void upDatePaymentOn(final String order_id, final String payment_method_txt, final String payment_amount_text){
        sqLiteDatabase = sqLiteHelper.getWritableDatabase();
        sqLiteDatabase.execSQL("UPDATE driver_shops SET sync_status ='synced', payment_status ='"+payment_method_txt+"', amount_paid ='"+payment_amount_text+"' WHERE id ='"+order_id+"' ");
    }

    public void SQLiteDataBaseBuild(){

        sqLiteDatabase = openOrCreateDatabase(SQLiteHelper.DATABASE_NAME, Context.MODE_PRIVATE, null);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                Uri uri = result.getUri();

                try {

                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                     invoice_image.setVisibility(View.VISIBLE);
                    ((ImageView) findViewById(R.id.invoice_image)).setImageURI(result.getUri());
                    image_state.setText("cap");

                    // ImageUploadToServerFunction();

                } catch (IOException e) {

                    e.printStackTrace();
                }
            }
            else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }

        }
    }

}
