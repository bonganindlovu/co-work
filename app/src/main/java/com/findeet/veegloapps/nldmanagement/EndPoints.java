package com.findeet.veegloapps.nldmanagement;

/**
 * Created by DEBONGZ on 4/20/2018.
 */

public class EndPoints {
    public static final String URL_FETCH_OPEN = "http://nextld.co.za/salesadmin/app/select_quotes.php";
    public static final String URL_FETCH_DELIVERED = "http://nextld.co.za/salesadmin/app/rqb_fetch_delivered_quotes.php";
    public static final String URL_FETCH_PULLED = "http://nextld.co.za/salesadmin/app/rqb_fetch_pulled_quotes_by.php";
    public static final String URL_FETCH_PULLED_BY = "http://nextld.co.za/salesadmin/app/rqb_fetch_pulled_quotes_by.php";
    public static final String ServerUploadPath ="http://nextld.co.za/salesadmin/app/rsavebysig.php" ;
    public static final String ServerUploadPathDel ="http://nextld.co.za/salesadmin/app/rsavedeletesig.php" ;





    public static final String CUSTOMER_SAVED_BROADCAST ="veegloapps.nldmanagement.cust_saved" ;
    public static final String ITEM_SAVED_BROADCAST ="veegloapps.nldmanagement.item_saved" ;
    public static final String ORDER_SAVED_BROADCAST ="veegloapps.nldmanagement.order_saved" ;

    public static final String ORDER_TICK ="http://nextld.co.za/salesadmin/app/order_tick.php" ;

    public static final String CHECK_ORDER_STATUS ="http://nextld.co.za/salesadmin/app/check_state.php" ;
    public static final String GET_ROUTES ="http://nextld.co.za/salesadmin/app/get_routes.php" ;
    public static final String GET_ROUTES_SHOPS ="http://nextld.co.za/salesadmin/app/get_routes_shops.php" ;
    public static final String UPDATE_ROUTES ="http://nextld.co.za/salesadmin/app/update_routes.php" ;

    public static final String UPDATE_TOKEN ="http://nextld.co.za/salesadmin/app/update_token.php" ;
    public static final String MOVE_SHOP ="http://nextld.co.za/salesadmin/app/move_shop.php" ;

    public static final String SEND_LOADS ="http://nextld.co.za/salesadmin/app/send_loads.php" ;
    public static final String URL_FETCH_NOT_THREAD ="http://nextld.co.za/salesadmin/app/load_notifications.php" ;
    public static final String URL_FETCH_USER_TASKS ="http://nextld.co.za/salesadmin/app/load_users_tasks.php" ;

    public static final String URL_FETCH_USERS_TASKS ="http://nextld.co.za/salesadmin/app/load_all_users_tasks.php" ;
    public static final String UPDATE_LOADS ="http://nextld.co.za/salesadmin/app/update_loads.php" ;

    public static final String GET_LOADS ="http://nextld.co.za/salesadmin/app/get_loads.php" ;
    public static final String GET_DEL_SHOPS ="http://nextld.co.za/salesadmin/app/distance.php" ;
    public static final String SEND_LOAD_SHOPS ="http://nextld.co.za/salesadmin/app/send_load_shops.php" ;
    public static final String PICK_QUOTES ="http://nextld.co.za/salesadmin/app/pick_quotes.php" ;

    public static final String SEND_TASKS ="http://nextld.co.za/salesadmin/app/send_tasks.php" ;
    public static final String FETCH_TASKS ="http://nextld.co.za/salesadmin/app/fetch_task.php" ;
    public static final String CONFIRM_TASKS ="http://nextld.co.za/salesadmin/app/confirm_task.php";

}
