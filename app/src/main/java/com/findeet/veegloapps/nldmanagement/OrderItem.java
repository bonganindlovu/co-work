package com.findeet.veegloapps.nldmanagement;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.findeet.veegloapps.nldmanagement.Utils.SQLiteHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class OrderItem extends AppCompatActivity {
    TextView proNum, ProName, ProUnit, SaveOrder;
    EditText Qty;
    String url = "http://nextld.co.za/salesadmin/app/add_item.php";
    String urlb = "http://nextld.co.za/salesadmin/app/delete_item.php";
    private ProgressDialog progressDialog;
    SQLiteDatabase sqLiteDatabase;
    SQLiteHelper sqLiteHelper;
    Cursor cursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_item);

        Toolbar mytoolbar = (Toolbar) findViewById(R.id.toolbaritem);
        setSupportActionBar(mytoolbar);
        getSupportActionBar().setTitle("ADD ITEM");
        SQLiteDataBaseBuild();
        sqLiteHelper = new SQLiteHelper(this);

        SharedPreferences ordershared = getSharedPreferences("Order", Context.MODE_PRIVATE);
        final String order_id = ordershared.getString("order_id", "");
        String cust_nu = ordershared.getString("cust_number", "");
        String cust_na = ordershared.getString("cust_name", "");
        String cust_addr = ordershared.getString("cust_addr", "");

        proNum = (TextView)findViewById(R.id.item_number);
        ProName= (TextView)findViewById(R.id.item_name);
        ProUnit= (TextView)findViewById(R.id.item_unit);
        SaveOrder = (TextView)findViewById(R.id.save_order);
        Qty = (EditText)findViewById(R.id.input_qty);

        Intent in = getIntent();
        String item_number= in.getExtras().getString("item_number");
        String item_name= in.getExtras().getString("item_name");
        String item_unit= in.getExtras().getString("item_unit");
        String item_quantity= in.getExtras().getString("item_quantity");


        //string for the quantity
        String quantity = Qty.getText().toString();



        proNum.setText(item_number); // change to item_number
        ProName.setText(item_name);
        ProUnit.setText(item_unit);
        Qty.setText(item_quantity);






        SaveOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = getIntent();
                String seorderid = order_id;
                String seitemnum = in.getExtras().getString("item_number");
                String seitemname = in.getExtras().getString("item_name");
                String seitemunit = in.getExtras().getString("item_unit");
                String seitemquantity = Qty.getText().toString();




                if (seitemquantity.isEmpty()) {
                    Toast.makeText(OrderItem.this, "Quantity Value cannot be empty", Toast.LENGTH_SHORT).show();

                } else {

                    POSTITEM(seorderid, seitemnum, seitemname, seitemquantity, seitemunit);
                }
            }
        });



    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_item, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.delete:
                SharedPreferences ordershared = getSharedPreferences("Order", Context.MODE_PRIVATE);
                String order_id = ordershared.getString("order_id", "");
                Intent in = getIntent();
                String seorderid = order_id;
                String seitemnum = in.getExtras().getString("item_number");
                String seitemname = in.getExtras().getString("item_name");

                DELETEITEM(seorderid, seitemnum);

                break;
            case R.id.save:
                ordershared = getSharedPreferences("Order", Context.MODE_PRIVATE);
                order_id = ordershared.getString("order_id", "");
                in = getIntent();
                seorderid = order_id;
                seitemnum = in.getExtras().getString("item_number");
                String fseitemname = in.getExtras().getString("item_name");
                String seitemunit = in.getExtras().getString("item_unit");
                String seitemquantity = Qty.getText().toString();
                seitemname = fseitemname.replaceAll("'", "");




                if (seitemquantity.isEmpty()) {
                    Toast.makeText(OrderItem.this, "Quantity Value cannot be empty", Toast.LENGTH_SHORT).show();

                } else {

                    POSTITEM(seorderid, seitemnum, seitemname, seitemquantity, seitemunit);

                }
                break;
            default:
                break;
        }
        return true;
    }


    public void goback(View view) {
        super.onBackPressed();
    }

    @Override
    public void onBackPressed()
    {

        super.onBackPressed();
    }

    public void POSTITEM(final String seorderid, final String seitemnum, final String seitemname, final String seitemquantity, final String seitemunit) {
        sqLiteDatabase = sqLiteHelper.getWritableDatabase();
        cursor = sqLiteDatabase.rawQuery("SELECT * FROM sales_order_items WHERE order_id = '"+seorderid+"' AND item_number = '"+seitemnum+"'   ", null);
        if (cursor.moveToFirst()) {
            //do
            sqLiteDatabase.execSQL("UPDATE sales_order_items SET sync_state ='not_synced', item_quantity ='"+seitemquantity+"' WHERE order_id ='"+seorderid+"' AND item_number = '"+seitemnum+"'");
            finish();
        }
        else{
            // add item to sqlite
            Toast.makeText(OrderItem.this,"Item Added", Toast.LENGTH_LONG).show();
            String SQLiteDataBaseQueryHolder = "INSERT INTO sales_order_items (order_id,item_number,item_name,item_unit,item_quantity,sync_state) VALUES('" + seorderid + "','" + seitemnum + "', '" + seitemname + "', '" + seitemunit + "', '" + seitemquantity + "', 'not_synced');";
            sqLiteDatabase.execSQL(SQLiteDataBaseQueryHolder);
            finish();
        }
    }
    public void DELETEITEM(final String seorderid, final String seitemnum) {
        sqLiteDatabase = sqLiteHelper.getWritableDatabase();
        cursor = sqLiteDatabase.rawQuery("SELECT * FROM sales_order_items WHERE order_id = '"+seorderid+"' AND item_number = '"+seitemnum+"'   ", null);
        if (cursor.moveToFirst()) {
            String sync_state = cursor.getString(cursor.getColumnIndex("sync_state"));
            if (sync_state.equalsIgnoreCase("synced")){
                //delete from mysql and sqlite
                deleteMysql(seorderid,seitemnum);
            }
            else{
                sqLiteDatabase.execSQL("DELETE FROM sales_order_items WHERE order_id = '"+seorderid+"' AND item_number = '"+seitemnum+"' ");
            }

        }



    }

    public void SQLiteDataBaseBuild() {
        sqLiteDatabase = openOrCreateDatabase(SQLiteHelper.DATABASE_NAME, Context.MODE_PRIVATE, null);
        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS sales_order_items (order_id VARCHAR(200),item_number VARCHAR(50), item_name VARCHAR(100), item_unit VARCHAR(10), item_quantity INT(10),item_total DOUBLE(13,2), sync_state VARCHAR(50))");
        //sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS products (id INTEGER, cust_number TEXT, cust_name TEXT, addr_line_one TEXT)");

    }

    public void deleteMysql(final String seorderid, final String seitemnum) {

        Log.i("Hiteshurl", "" + urlb);
        RequestQueue requestQueuea = Volley.newRequestQueue(OrderItem.this);
        StringRequest stringRequesta = new StringRequest(Request.Method.POST, urlb, new Response.Listener<String>(){
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("result");
                    JSONObject jsonObject1 = jsonArray.getJSONObject(0);
                    String status = jsonObject1.getString("status");
                    String message = jsonObject1.getString("message");



                    if(status.equalsIgnoreCase("passed")) {
                        sqLiteDatabase.execSQL("DELETE FROM sales_order_items WHERE order_id = '"+seorderid+"' AND item_number = '"+seitemnum+"' ");
                        Toast.makeText(OrderItem.this, ""+message, Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(OrderItem.this, Order.class);
                        startActivity(intent);
                        finish();
                    }
                    else{
                        Toast.makeText(OrderItem.this, ""+message, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i("Hitesh",""+error);
                Toast.makeText(OrderItem.this, "Error ! Please check your network connection", Toast.LENGTH_SHORT).show();

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> stringMapa = new HashMap<>();
                stringMapa.put("order_id",seorderid);
                stringMapa.put("item_number",seitemnum);
                return stringMapa;
            }

        };

        requestQueuea.add(stringRequesta);
        stringRequesta.setRetryPolicy(new DefaultRetryPolicy(
                DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
                0,  // maxNumRetries = 0 means no retry
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

    }
}
