package com.findeet.veegloapps.nldmanagement;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.findeet.veegloapps.nldmanagement.Utils.SQLiteHelper;
import com.findeet.veegloapps.nldmanagement.Utils.SQLiteHelperb;
import com.findeet.veegloapps.nldmanagement.adapters.AdapterDispatchLoads;

import java.util.ArrayList;

public class DispatchLoads extends AppCompatActivity {
    android.support.v7.widget.Toolbar toolbar_loads;
    SQLiteDatabase sqLiteDatabase;
    SQLiteHelperb sqLiteHelperb;
    TextView noLoadShops;
    private RecyclerView zmycustRecycleViewa;
    private AdapterDispatchLoads zmAdaptera;
    Cursor cursor;
    BroadcastReceiver broadcastReceiver;
    ArrayList<String> id_Array,load_id_Array,shop_id_Array,shop_name_Array,invoice_number_Array,invoice_amount_Array,sync_state_Array;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SQLiteDataBaseBuild();
        sqLiteHelperb = new SQLiteHelperb(this);
        setContentView(R.layout.activity_dispatch_loads);
        Intent in = getIntent();
        String load_id= in.getExtras().getString("load_id");
        String vehicle_name= in.getExtras().getString("vehicle_name");
        noLoadShops = (TextView)findViewById(R.id.noLoadShops);

        toolbar_loads = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar_loads);
        setSupportActionBar(toolbar_loads);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(vehicle_name);

        id_Array = new ArrayList<String>();
        load_id_Array = new ArrayList<String>();
        shop_id_Array = new ArrayList<String>();
        shop_name_Array = new ArrayList<String>();
        invoice_number_Array = new ArrayList<String>();
        invoice_amount_Array = new ArrayList<String>();
        sync_state_Array = new ArrayList<String>();

        //the broadcast receiver to update sync status
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Intent in = getIntent();
                String load_id= in.getExtras().getString("load_id");
                //loading the names again
                fetchLoadShops(load_id);
            }
        };

        //registering the broadcast receiver to update sync status
        registerReceiver(broadcastReceiver, new IntentFilter(EndPoints.ORDER_SAVED_BROADCAST+"shops"));


        fetchLoadShops(load_id);
    }

    public void fetchLoadShops(final String load_idmy){
        sqLiteDatabase = sqLiteHelperb.getWritableDatabase();

        cursor = sqLiteDatabase.rawQuery("SELECT * FROM loads_shops WHERE load_id ='"+load_idmy+"'  ORDER BY id DESC ", null);
        id_Array.clear();
        load_id_Array.clear();
        shop_id_Array.clear();
        shop_name_Array.clear();
        invoice_number_Array.clear();
        invoice_amount_Array.clear();
        sync_state_Array.clear();
        sync_state_Array.clear();

        if (cursor.moveToFirst()) {
            do {
                id_Array.add(cursor.getString(cursor.getColumnIndex("id")));
                load_id_Array.add(cursor.getString(cursor.getColumnIndex("load_id")));
                shop_id_Array.add(cursor.getString(cursor.getColumnIndex("shop_id")));
                shop_name_Array.add(cursor.getString(cursor.getColumnIndex("shop_name")));
                invoice_number_Array.add(cursor.getString(cursor.getColumnIndex("invoice_number")));
                invoice_amount_Array.add(cursor.getString(cursor.getColumnIndex("invoice_amount")));
                sync_state_Array.add(cursor.getString(cursor.getColumnIndex("sync_status")));

            }

            while (cursor.moveToNext());
        }
        else{
            noLoadShops.setVisibility(View.VISIBLE);
        }

        zmycustRecycleViewa = (RecyclerView) findViewById(R.id.loadRecycle);
        zmAdaptera = new AdapterDispatchLoads(DispatchLoads.this, id_Array,load_id_Array,shop_id_Array,shop_name_Array,invoice_number_Array,invoice_amount_Array,sync_state_Array);
        zmycustRecycleViewa.setAdapter(zmAdaptera);
        zmycustRecycleViewa.setLayoutManager(new LinearLayoutManager(DispatchLoads.this));

        cursor.close();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_load, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_load:
                Intent in = getIntent();
                String load_id= in.getExtras().getString("load_id");
                SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = shared.edit();
                editor.putString("load_id", load_id);
                editor.apply();
                Intent intent = new Intent(DispatchLoads.this, LoadCustomers.class);
                startActivity(intent);
                break;
            default:
                break;
        }
        return true;
    }

    @Override
    protected void onResume() {
        Intent in = getIntent();
        String load_id= in.getExtras().getString("load_id");
        fetchLoadShops(load_id);
        super.onResume();
    }

    public void SQLiteDataBaseBuild(){

        sqLiteDatabase = openOrCreateDatabase(SQLiteHelper.DATABASE_NAME, Context.MODE_PRIVATE, null);

    }
}
