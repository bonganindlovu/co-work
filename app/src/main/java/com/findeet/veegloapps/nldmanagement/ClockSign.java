package com.findeet.veegloapps.nldmanagement;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.gcacace.signaturepad.views.SignaturePad;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class ClockSign extends AppCompatActivity {
    Boolean check = true;
    String ImagePath = "image_path" ;
    String ServerUploadPath ="http://nextld.co.za/salesadmin/app/expense/save_expense.php" ;
    private ProgressDialog progressDialog;
    TextView save_btn;
    private SignaturePad mSignaturePadto;
    private SignaturePad mSignaturePadby;
    private TextView mClearButton,mSaveButton,cat_txt;
    Bitmap signatureBitmapby;
    Bitmap signatureBitmapto;
    TextView  sig_to_txt,sig_by_txt, username_txt,action_txt;


    ImageView sigimgto;
    ImageView sigimgby;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clock_sign);

        Intent intent = getIntent();
        String user_name = intent.getStringExtra("user_name");
        String task_name = intent.getStringExtra("action");


        username_txt =(TextView)findViewById(R.id.user_name) ;
        action_txt =(TextView)findViewById(R.id.action) ;

        username_txt.setText(user_name);
        action_txt.setText(task_name);


        sig_to_txt=(TextView)findViewById(R.id.sig_to_txt);
        sig_by_txt=(TextView)findViewById(R.id.sig_by_txt);
        save_btn=(TextView)findViewById(R.id.save_btn);
        sigimgto = (ImageView)findViewById(R.id.imgto);
        sigimgby = (ImageView)findViewById(R.id.imgby);
        mSignaturePadto = (SignaturePad) findViewById(R.id.signature_to);
        mSignaturePadby = (SignaturePad) findViewById(R.id.signature_by);


        mSignaturePadby.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
                //Toast.makeText(AsignByTab.this, "OnStartSigning", Toast.LENGTH_SHORT).show();
                signatureBitmapby = mSignaturePadby.getSignatureBitmap();
                sigimgby.setImageBitmap(signatureBitmapby);

            }

            @Override
            public void onSigned() {
                // set the save button to be true
                sig_by_txt.setText("signed");

            }

            @Override
            public void onClear() {
                // set the save button to be true and all fields
                sig_by_txt.setText("not_signed");
            }
        });
        mSignaturePadto.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
                //Toast.makeText(AsignByTab.this, "OnStartSigning", Toast.LENGTH_SHORT).show();
                signatureBitmapto = mSignaturePadto.getSignatureBitmap();
                sigimgto.setImageBitmap(signatureBitmapto);

            }

            @Override
            public void onSigned() {
                // set the save button to be true
                sig_to_txt.setText("signed");

            }

            @Override
            public void onClear() {
                // set the save button to be true and all fields
                sig_to_txt.setText("not_signed");
            }
        });

        mClearButton = (TextView) findViewById(R.id.clear);
        mSaveButton = (TextView) findViewById(R.id.save_btn);

        mClearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSignaturePadby.clear();
                mSignaturePadto.clear();
                sig_to_txt.setText("not_signed");
                sig_by_txt.setText("not_signed");
            }
        });

        save_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String to_sig = sig_to_txt.getText().toString();
                String by_sig = sig_by_txt.getText().toString();
              if(to_sig.equalsIgnoreCase("not_signed") && by_sig.equalsIgnoreCase("not_signed")){
                    //toast
                    Toast.makeText(ClockSign.this, "Please Sign Employee signature tab!",
                            Toast.LENGTH_LONG).show();
                }

              else if(by_sig.equalsIgnoreCase("not_signed") ){
                 LayoutInflater layoutInflaterAndroid = LayoutInflater.from(ClockSign.this);
                 View mView = layoutInflaterAndroid.inflate(R.layout.sig_by_req_dialog_box, null);
                 AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(ClockSign.this);
                 alertDialogBuilderUserInput.setView(mView);

                 alertDialogBuilderUserInput
                         .setCancelable(true)
                         .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                             @Override
                             public void onClick(DialogInterface dialogBox, int id) {
                                 //delete the order
                                 String by_sig2 = sig_by_txt.getText().toString();
                                 ImageUploadToServerFunction(by_sig2);
                                 dialogBox.cancel();

                             }
                         })

                         .setNegativeButton("NO",
                                 new DialogInterface.OnClickListener() {
                                     @Override
                                     public void onClick(DialogInterface dialogBox, int id) {

                                         dialogBox.cancel();

                                     }
                                 });


                 AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
                 alertDialogAndroid.show();
                }
                else if(to_sig.equalsIgnoreCase("not_signed") ){
                    //toast
                    Toast.makeText(ClockSign.this, "Please Sign Employee signature tab!",
                            Toast.LENGTH_LONG).show();
                }
                else{
                    //save the expense
                    ImageUploadToServerFunction(by_sig);
                }


            }
        });


    }


    public void ImageUploadToServerFunction(final String sig_by){
        Bitmap bm= mSignaturePadby.getSignatureBitmap();
        ByteArrayOutputStream ba=new ByteArrayOutputStream(  );
        bm.compress( Bitmap.CompressFormat.JPEG, 100,ba );
        byte[] by=ba.toByteArray();
        final String encod= Base64.encodeToString( by,Base64.DEFAULT );

        Bitmap bma=mSignaturePadto.getSignatureBitmap();
        ByteArrayOutputStream baa=new ByteArrayOutputStream(  );
        bma.compress( Bitmap.CompressFormat.JPEG, 100,baa );
        byte[] bya=baa.toByteArray();
        final String encoda= Base64.encodeToString( bya,Base64.DEFAULT );


        final String ConvertImage = encod;
        final String ConvertImagea = encoda;

        class AsyncTaskUploadClass extends AsyncTask<Void,Void,String> {

            @Override
            protected void onPreExecute() {

                super.onPreExecute();
                //show progressbar
                progressDialog = new ProgressDialog(ClockSign.this);
                progressDialog.setMessage("Saving Signatures...");
                progressDialog.show();
            }

            @Override
            protected void onPostExecute(String string1) {

                super.onPostExecute(string1);

                Intent intent = new Intent(ClockSign.this, Tasks.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                // Printing uploading success message coming from server on android app.
                Toast.makeText(ClockSign.this, "Clock Signature saved!", Toast.LENGTH_LONG).show();
                progressDialog.dismiss();

            }

            @Override
            protected String doInBackground(Void... params) {

                Intent intent = getIntent();
                String user_name = intent.getStringExtra("user_name");
                String task_name = intent.getStringExtra("action");

                ImageProcessClass imageProcessClass = new ImageProcessClass();

                HashMap<String,String> HashMapParams = new HashMap<String,String>();

                HashMapParams.put(ImagePath, ConvertImage);
                HashMapParams.put(ImagePath+"2", ConvertImagea);
                HashMapParams.put("user_name",user_name);
                HashMapParams.put("task_name",task_name);
                HashMapParams.put("sig_by",sig_by);
                String FinalData = imageProcessClass.ImageHttpRequest(EndPoints.SEND_TASKS, HashMapParams);

                return FinalData;
            }
        }
        AsyncTaskUploadClass AsyncTaskUploadClassOBJ = new AsyncTaskUploadClass();
        AsyncTaskUploadClassOBJ.execute();
    }
    public class ImageProcessClass{

        public String ImageHttpRequest(String requestURL,HashMap<String, String> PData) {

            StringBuilder stringBuilder = new StringBuilder();

            try {

                URL url;
                HttpURLConnection httpURLConnectionObject ;
                OutputStream OutPutStream;
                BufferedWriter bufferedWriterObject ;
                BufferedReader bufferedReaderObject ;
                int RC ;

                url = new URL(requestURL);

                httpURLConnectionObject = (HttpURLConnection) url.openConnection();

                httpURLConnectionObject.setReadTimeout(19000);

                httpURLConnectionObject.setConnectTimeout(19000);

                httpURLConnectionObject.setRequestMethod("POST");

                httpURLConnectionObject.setDoInput(true);

                httpURLConnectionObject.setDoOutput(true);

                OutPutStream = httpURLConnectionObject.getOutputStream();

                bufferedWriterObject = new BufferedWriter(

                        new OutputStreamWriter(OutPutStream, "UTF-8"));

                bufferedWriterObject.write(bufferedWriterDataFN(PData));

                bufferedWriterObject.flush();

                bufferedWriterObject.close();

                OutPutStream.close();

                RC = httpURLConnectionObject.getResponseCode();

                if (RC == HttpsURLConnection.HTTP_OK) {

                    bufferedReaderObject = new BufferedReader(new InputStreamReader(httpURLConnectionObject.getInputStream()));

                    stringBuilder = new StringBuilder();

                    String RC2;

                    while ((RC2 = bufferedReaderObject.readLine()) != null){

                        stringBuilder.append(RC2);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return stringBuilder.toString();
        }
        private String bufferedWriterDataFN(HashMap<String, String> HashMapParams) throws UnsupportedEncodingException {

            StringBuilder stringBuilderObject;

            stringBuilderObject = new StringBuilder();

            for (Map.Entry<String, String> KEY : HashMapParams.entrySet()) {

                if (check)

                    check = false;
                else
                    stringBuilderObject.append("&");

                stringBuilderObject.append(URLEncoder.encode(KEY.getKey(), "UTF-8"));

                stringBuilderObject.append("=");

                stringBuilderObject.append(URLEncoder.encode(KEY.getValue(), "UTF-8"));
            }

            return stringBuilderObject.toString();
        }

    }

    public void hideCatLay(View view) {
    }
}
