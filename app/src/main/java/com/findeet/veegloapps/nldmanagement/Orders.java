package com.findeet.veegloapps.nldmanagement;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.findeet.veegloapps.nldmanagement.Utils.NetworkStateChecker;
import com.findeet.veegloapps.nldmanagement.Utils.SalesOrderFullSQL;
import com.findeet.veegloapps.nldmanagement.fragments.InvoicedFragment;
import com.findeet.veegloapps.nldmanagement.fragments.OpenFragment;
import com.findeet.veegloapps.nldmanagement.fragments.PulledFragment;
import com.findeet.veegloapps.nldmanagement.fragments.QuotedFragment;

import java.util.ArrayList;
import java.util.List;

public class Orders extends AppCompatActivity {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    public static final String URL_SAVE_NAME = "http://nextld.co.za/salesadmin/app/sync_customers.php";
    android.support.v7.widget.Toolbar orders_toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders);
        viewPager = (ViewPager) findViewById(R.id.orders_viewpager);
        setupViewPager(viewPager);
        orders_toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(orders_toolbar);
        SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
        String repname =shared.getString("user_name", "");
        ActionBar mybar = getSupportActionBar();
        mybar.setTitle(repname+"'s orders");

        tabLayout = (TabLayout) findViewById(R.id.order_tabs);
        tabLayout.setupWithViewPager(viewPager);
        registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        registerReceiver(new SalesOrderFullSQL(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }
    private void setupViewPager(ViewPager viewPager) {
        Orders.ViewPagerAdapter adapter = new Orders.ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new OpenFragment(), "OPEN");
        adapter.addFragment(new QuotedFragment(), "QUOTED");
        adapter.addFragment(new PulledFragment(), "PULLED");
        adapter.addFragment(new InvoicedFragment(), "INVOICED");
        viewPager.setAdapter(adapter);
    }
    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }
        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    protected void onResume() {
        registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        registerReceiver(new SalesOrderFullSQL(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        final Intent intent = new Intent(Orders.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
