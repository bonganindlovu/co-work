package com.findeet.veegloapps.nldmanagement.Utils;

/**
 * Created by DEBONGZ on 1/23/2018.
 */

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SQLiteHelper extends SQLiteOpenHelper {

    public static String DATABASE_NAME="nextld";
    public static final String ID_COLUMN="cust_number";

    public static final String Table_Column_1_Subject_Cnumber="cust_number";
    public static final String Table_Column_1_Subject_Cname="cust_name";
    public static final String Table_Column_1_Subject_Caddr="addr_line_one";
    public static final String Table_Column_1_Subject_Sync_Status="sync_status";
    public static final String Table_COLUMN_LONG="cust_long";
    public static final String Table_COLUMN_LAT="cust_lat";
    public static final String Table_COLUMN_CONTACT_NAME="contact_name";
    public static final String Table_COLUMN_CONTACT_PHONE="contact_phone";
    public static final String Table_COLUMN_ROUTE="route_name";
    public static final String Table_COLUMN_USER_ID="user_id";
    public static final String Table_COLUMN_CUST_ID="cust_id";

    public static final String TABLE_NAME="customers";

    public static final String Table_Column_1_Subject_Pid="mpid";
    public static final String Table_Column_1_Subject_Pname="mpname";
    public static final String Table_Column_1_Subject_Punit="mpunit";

    public static final String TABLE_P_NAME="inve";

    public SQLiteHelper(Context context) {

        super(context, DATABASE_NAME, null, 4);

    }

    @Override
    public void onCreate(SQLiteDatabase database) {

        database.execSQL("CREATE TABLE IF NOT EXISTS customers (id INTEGER, cust_number TEXT, cust_name TEXT, addr_line_one TEXT, cust_lat TEXT,cust_long TEXT, contact_name TEXT, contact_phone TEXT,route_name TEXT,user_id TEXT, sync_status TEXT)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS customers");
        onCreate(db);

    }

}