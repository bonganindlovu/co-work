package com.findeet.veegloapps.nldmanagement.adapters;

/**
 * Created by DEBONGZ on 1/5/2018.
 */
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.findeet.veegloapps.nldmanagement.CustomerDetailsList;
import com.findeet.veegloapps.nldmanagement.R;

import java.util.ArrayList;

public class AdapterCustomerList extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<String> custid;
    ArrayList<String> custname;
    ArrayList<String> custaddr;

    private Context context;
    private LayoutInflater inflater;
    //List<DataCustomers> data= Collections.emptyList();
    //DataCustomers current;
    int currentPos=0;


    // create constructor to initialize context and data sent from MainActivity
    public AdapterCustomerList(Context context,
                               ArrayList<String> id,
                               ArrayList<String> sub_name,
                               ArrayList<String> Sub_full){
        this.context=context;
        inflater= LayoutInflater.from(context);
        //this.data=data;
        this.custid = id;
        this.custname = sub_name;
        this.custaddr = Sub_full;
    }



    // Inflate the layout when ViewHolder created
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=inflater.inflate(R.layout.container_list_customers, parent,false);
        MyHolder holder=new MyHolder(view);
        return holder;
    }
    // Bind data
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        // Get current position of item in RecyclerView to bind data and assign values from list
        MyHolder myHolder= (MyHolder) holder;
        //DataCustomers current=data.get(position);
        String name = (custname.get(position));
        String ava = name.substring(0,1).toUpperCase();

        myHolder.txtName.setText(custname.get(position));
        myHolder.txtAdress.setText(custaddr.get(position));
        myHolder.txtId.setText(custid.get(position));
        myHolder.txtAvatar.setText(ava);

//
//        myHolder.expense_line.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View v) {
//
//                    new AlertDialog.Builder(context)
//                            .setTitle("Chose Action")
//                            .setCancelable(true)
//                            .setPositiveButton("Call Customer", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int whichButton) {
//
//                                }
//                            })
//                            .setNegativeButton("Navigate", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int whichButton) {
//                                    dialog.cancel();
//                                }
//                            })
//                            .show();
//
//                return false;
//        }
//    });


}
    // return total item from List
    @Override
    public int getItemCount() {
        return custid.size();

    }
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    class MyHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        TextView txtName;
        TextView txtId;
        TextView txtAdress;
        TextView txtAvatar;
        RelativeLayout expense_line;


        // create constructor to get widget reference
        public MyHolder(View itemView) {
            super(itemView);
            expense_line = (RelativeLayout) itemView.findViewById(R.id.expense_line);
            txtName = (TextView) itemView.findViewById(R.id.customer_name);
            txtId = (TextView) itemView.findViewById(R.id.customer_id);
            txtAdress = (TextView) itemView.findViewById(R.id.customer_address);
            txtAvatar = (TextView) itemView.findViewById(R.id.customer_avatar);
            itemView.setOnClickListener(this);
        }

        // Click event for all items
        @Override
        public void onClick(View v) {
            String shop_number = txtId.getText().toString();
           //go to customer full details
            Intent intent= new Intent(context, CustomerDetailsList.class);
            intent.putExtra("customer_id", shop_number);
            context.startActivity(intent);
            //((Activity)context).finish();
        }
    }


    }
