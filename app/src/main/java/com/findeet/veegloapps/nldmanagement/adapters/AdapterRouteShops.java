package com.findeet.veegloapps.nldmanagement.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.findeet.veegloapps.nldmanagement.R;
import com.findeet.veegloapps.nldmanagement.SalesHistory;
import com.findeet.veegloapps.nldmanagement.Utils.SQLiteHelper;
import com.findeet.veegloapps.nldmanagement.Utils.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by DEBONGZ on 1/6/2018.
 */

public class AdapterRouteShops extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private LayoutInflater inflater;
    SQLiteDatabase sqLiteDatabase;
    ArrayList<String> oroute_name;
    ArrayList<String> ocust_number;
    ArrayList<String> ocust_name;
    ArrayList<String> olast_date_seen;
    ArrayList<String> olast_order_size;
    SQLiteHelper sqLiteHelper;
    Cursor cursor;
    String url = "http://nextld.co.za/salesadmin/app/add_feedback.php";


    ArrayList<String> oitem_synce_state;
    int currentPos=0;

    ///cust_number_Array,route_name_Array,cust_number_Array,cust_name_Array,last_date_seen_Array,last_order_size_Array
    public AdapterRouteShops(Context context, ArrayList<String> route_name, ArrayList<String> cust_number, ArrayList<String> cust_name, ArrayList<String> last_date_seen, ArrayList<String> last_order_size){
        this.context=context;
        inflater= LayoutInflater.from(context);
        this.oroute_name = route_name;
        this.ocust_number = cust_number;
        this.ocust_name = cust_name;
        this.olast_date_seen = last_date_seen;
        this.olast_order_size = last_order_size;

    }

    // Inflate the layout when ViewHolder created
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=inflater.inflate(R.layout.container_quotes, parent,false);
        MyHolder holder=new MyHolder(view);

        return holder;
    }

    // Bind data
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        // Get current position of item in RecyclerView to bind data and assign values from list
        final MyHolder myHolder= (MyHolder) holder;
        myHolder.txtQcustName.setText(ocust_number.get(position)+" "+ocust_name.get(position));
        myHolder.txtQorderTotal.setText(olast_order_size.get(position));
        myHolder.txtQOrderDate.setText(olast_date_seen.get(position));
        myHolder.Tcustomer_number.setText(ocust_number.get(position));
        myHolder.Troute_name.setText(oroute_name.get(position));
        myHolder.Tcustname.setText(ocust_name.get(position));

        myHolder.Rquote_line.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                LayoutInflater layoutInflaterAndroid = LayoutInflater.from(context);
                View mView = layoutInflaterAndroid.inflate(R.layout.route_input_dialog_box, null);
                AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(context);
                alertDialogBuilderUserInput.setView(mView);
                sqLiteHelper = new SQLiteHelper(context);
                sqLiteDatabase = sqLiteHelper.getWritableDatabase();
                String route_name = myHolder.Troute_name.getText().toString();
                String cust_number = myHolder.Tcustomer_number.getText().toString();

                cursor = sqLiteDatabase.rawQuery("SELECT * FROM routes_shops WHERE route_name ='"+route_name+"' AND cust_number ='"+cust_number+"'  ", null);
                if (cursor.moveToFirst()) {
                    String curfeedback =cursor.getString(cursor.getColumnIndex("last_person_seen"));
                    String curperson =cursor.getString(cursor.getColumnIndex("last_message"));
                }


                final EditText feedback = (EditText) mView.findViewById(R.id.feedback_txt);
                final EditText last_person = (EditText) mView.findViewById(R.id.last_txt);
                final TextView custnamed = (TextView) mView.findViewById(R.id.cust_name_dialog);
                String custname = myHolder.Tcustname.getText().toString();
                custnamed.setText(custname);

                if (cursor.moveToFirst()) {
                    String curperson =cursor.getString(cursor.getColumnIndex("last_person_seen"));
                    String curfeedback =cursor.getString(cursor.getColumnIndex("last_message"));
                    feedback.setText(curfeedback);
                    last_person.setText(curperson);
                }

                alertDialogBuilderUserInput
                        .setCancelable(false)
                        .setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                if (feedback.getText().toString().isEmpty() || last_person.getText().toString().isEmpty()){
                                    Toast.makeText(context, "Did not submit! All fields required", Toast.LENGTH_SHORT).show();
                                }

                                else{
                                 //Toast.makeText(context, ""+last_person.getText().toString(), Toast.LENGTH_SHORT).show();
                                    //update mysql
                                    String route_name = myHolder.Troute_name.getText().toString();
                                    String cust_number = myHolder.Tcustomer_number.getText().toString();
                                    String sfeedback = feedback.getText().toString();
                                    String slast_person = last_person.getText().toString();
                                    SharedPreferences shared2 = context.getSharedPreferences("Mypref", Context.MODE_PRIVATE);
                                    String latLong  = shared2.getString("LatLong", "");


                                    upDate(route_name,cust_number,sfeedback,slast_person,latLong);
                                    Toast.makeText(context, "Submitted", Toast.LENGTH_SHORT).show();
                                    ShowSQLiteDBdata();
                                }


                            }
                        })

                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogBox, int id) {
                                        //dialogBox.cancel();
                                    }
                                });

                AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
                alertDialogAndroid.show();


                return false;
            }
        });


    }
    // return total item from List
    @Override
    public int getItemCount() {
        return oroute_name.size();
    }

    class MyHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView txtQcustName;
        TextView txtQorderTotal;
        TextView txtQOrderDate;
        TextView txtQOrderNum;
        TextView txtQCustAddr;
        RelativeLayout Porder_sync_prog;
        RelativeLayout Rsent_badge;
        RelativeLayout Rquote_line;
        TextView Tcustomer_number;
        TextView Troute_name;
        TextView Tcustname;


        // create constructor to get widget reference
        public MyHolder(View itemViewa) {
            super(itemViewa);
            Tcustname = (TextView) itemViewa.findViewById(R.id.custname);
            Troute_name = (TextView) itemViewa.findViewById(R.id.route_name);
            Tcustomer_number = (TextView) itemViewa.findViewById(R.id.customer_number);
            txtQcustName = (TextView) itemViewa.findViewById(R.id.txtqname);
            txtQorderTotal = (TextView) itemViewa.findViewById(R.id.txtqtotal);
            txtQOrderDate = (TextView) itemViewa.findViewById(R.id.txtqdate);
            txtQOrderNum = (TextView) itemViewa.findViewById(R.id.txtqid);
            txtQCustAddr = (TextView) itemViewa.findViewById(R.id.txtqcustAddr);
            Porder_sync_prog =(RelativeLayout)itemViewa.findViewById(R.id.order_sync_status);
            Rsent_badge =(RelativeLayout) itemViewa.findViewById(R.id.sent_badge);
            Rquote_line =(RelativeLayout) itemViewa.findViewById(R.id.quote_line);
            itemViewa.setOnClickListener(this);
        }

        // Click event for all items
        @Override
        public void onClick(View v) {

            String cust_number = Tcustomer_number.getText().toString();
            String route_name = Troute_name.getText().toString();
            Intent intent=new Intent(context.getApplicationContext(), SalesHistory.class);
            intent.putExtra("cust_number", cust_number);
            intent.putExtra("route_name", route_name);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.getApplicationContext().startActivity(intent);


        }
    }


    public void upDate(final String route_name, final String cust_number, final String sfeedback, final String slast_person, final String latLong){
        String myfb = sfeedback.replace("'","");
        String mylp = slast_person.replace("'","");

        sqLiteDatabase = context.openOrCreateDatabase(SQLiteHelper.DATABASE_NAME, Context.MODE_PRIVATE, null);
        sqLiteDatabase.execSQL("UPDATE routes_shops SET last_person_seen ='"+mylp+"', last_message ='"+myfb+"', sync_state ='not_synced',lat_long='"+latLong+"' WHERE route_name ='"+route_name+"' AND cust_number ='"+cust_number+"' ");
    }

    private void ShowSQLiteDBdata() {

        sqLiteDatabase = sqLiteHelper.getWritableDatabase();
        String value ="synced";
        cursor = sqLiteDatabase.rawQuery("SELECT * FROM routes_shops WHERE sync_state != '"+value+"'  ", null);

        int count = 0;
        if (cursor.moveToFirst()) {
            do {
                String route_name =cursor.getString(cursor.getColumnIndex("route_name"));
                String cust_number =cursor.getString(cursor.getColumnIndex("cust_number"));
                String last_person_seen =cursor.getString(cursor.getColumnIndex("last_person_seen"));
                String last_message =cursor.getString(cursor.getColumnIndex("last_message"));
                String latLong =cursor.getString(cursor.getColumnIndex("lat_long"));

                POSTITEM(route_name, cust_number, last_person_seen, last_message,latLong);
                count+=count+1;
            }
            while (cursor.moveToNext());
        }

        int rowCount = count;
        //Toast.makeText(context, "" + rowCount, Toast.LENGTH_SHORT).show();
    }
    public void POSTITEM(final String route_name, final String cust_number, final String last_person_seen, final String last_message,final String latLong) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("result");
                            JSONObject jsonObject1 = jsonArray.getJSONObject(0);
                            String status = jsonObject1.getString("status");


                            if(status.equalsIgnoreCase("passed")) {
                                //update synce status
                                updateSyncStatus(route_name,cust_number);
                                //context.sendBroadcast(new Intent(EndPoints.ITEM_SAVED_BROADCAST));
                            }
                            else{}
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("route_name",route_name);
                params.put("cust_number",cust_number);
                params.put("last_person_seen",last_person_seen);
                params.put("last_message",last_message);
                params.put("lat_long",latLong);
                return params;
                //route_name, cust_number, last_person_seen, last_message
            }
        };

        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }
    public void updateSyncStatus(final String route_name,final String cust_number){
        sqLiteDatabase.execSQL("UPDATE routes_shops SET sync_state ='synced' WHERE route_name ='"+route_name+"' AND cust_number ='"+cust_number+"' ");
    }
}

