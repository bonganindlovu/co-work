package com.findeet.veegloapps.nldmanagement;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.findeet.veegloapps.nldmanagement.Utils.SQLiteHelper;
import com.findeet.veegloapps.nldmanagement.Utils.SQLiteHelperb;
import com.findeet.veegloapps.nldmanagement.Utils.VolleySingleton;
import com.findeet.veegloapps.nldmanagement.adapters.AdapterDelivery;
import com.findeet.veegloapps.nldmanagement.adapters.AdapterDispatch;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Delivery extends AppCompatActivity {
    Toolbar toolbar;
    SQLiteDatabase sqLiteDatabase;
    SQLiteHelperb sqLiteHelperb;
    FloatingActionButton add_dispatch;
    Spinner vehicle_txt,driver_txt;
    BroadcastReceiver broadcastReceiver;
    private RecyclerView zmycustRecycleViewa;
    private AdapterDelivery zmAdaptera;
    Cursor cursor;
    ArrayList<String> load_id_Array,vehicle_name_Array,driver_name_Array,load_time_Array,sync_state_Array;
    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery);
        toolbar = (Toolbar) findViewById(R.id.toolbar_deliver);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Deliveries");
        SQLiteDataBaseBuild();
        sqLiteHelperb = new SQLiteHelperb(this);

        final Context c = this;
        add_dispatch = (FloatingActionButton)findViewById(R.id.add_dispatch);
        add_dispatch.setVisibility(View.GONE);
        load_id_Array = new ArrayList<String>();
        vehicle_name_Array = new ArrayList<String>();
        driver_name_Array = new ArrayList<String>();
        sync_state_Array=  new ArrayList<String>();
        load_time_Array=new ArrayList<String>();
        checkLoads();
        loadsSQLite();


        //the broadcast receiver to update sync status
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //loading the names again
                loadsSQLite();
            }
        };

        //registering the broadcast receiver to update sync status
        registerReceiver(broadcastReceiver, new IntentFilter(EndPoints.ORDER_SAVED_BROADCAST+"del"));
    }

    public void SQLiteDataBaseBuild(){

        sqLiteDatabase = openOrCreateDatabase(SQLiteHelper.DATABASE_NAME, Context.MODE_PRIVATE, null);

    }
    private void loadsSQLite(){
        sqLiteDatabase = sqLiteHelperb.getWritableDatabase();
        SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
        String repname =shared.getString("user_name", "");

        cursor = sqLiteDatabase.rawQuery("SELECT * FROM loads WHERE drive_name ='"+repname+"'  ORDER BY load_time DESC ", null);
        load_id_Array.clear();
        vehicle_name_Array.clear();
        driver_name_Array.clear();
        sync_state_Array.clear();
        load_time_Array.clear();
        if (cursor.moveToFirst()) {
            do {
                //  DOUBLE(13,2),note VARCHAR(50),
                load_id_Array.add(cursor.getString(cursor.getColumnIndex("load_id")));
                vehicle_name_Array.add(cursor.getString(cursor.getColumnIndex("vehicle_name")));
                driver_name_Array.add(cursor.getString(cursor.getColumnIndex("drive_name")));
                sync_state_Array.add(cursor.getString(cursor.getColumnIndex("synce_status")));
                load_time_Array.add(cursor.getString(cursor.getColumnIndex("load_time")));
            }

            while (cursor.moveToNext());
        }

        zmycustRecycleViewa = (RecyclerView) findViewById(R.id.dispatchRecycle);
        zmAdaptera = new AdapterDelivery(Delivery.this, load_id_Array,vehicle_name_Array,driver_name_Array,sync_state_Array,load_time_Array);
        zmycustRecycleViewa.setAdapter(zmAdaptera);
        zmycustRecycleViewa.setLayoutManager(new LinearLayoutManager(Delivery.this));

        cursor.close();
    }

    private void checkLoads() {
        RequestQueue requestQueuemom = Volley.newRequestQueue(this);
        SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
        String repname =shared.getString("user_name", "");

        StringRequest stringRequestabc = new StringRequest(Request.Method.GET, EndPoints.GET_LOADS+"?user_name="+repname,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String result) {
                        //Toast.makeText(Delivery.this, "Res ="+result, Toast.LENGTH_SHORT).show();
                        try {
                            JSONArray jArray = new JSONArray(result);
                            for (int i = 0; i < jArray.length(); i++) {
                                JSONObject json_data = jArray.getJSONObject(i);
                                //proData.user_id = json_data.getString("user_id");
                                String load_id = json_data.getString("load_id");
                                String vehicle_name = json_data.getString("vehicle_name");
                                String load_time = json_data.getString("load_time");
                                sqLiteDatabase = sqLiteHelperb.getWritableDatabase();
                                cursor = sqLiteDatabase.rawQuery("SELECT * FROM loads WHERE load_id = '"+load_id+"'  ", null);
                                if (cursor.moveToFirst()) {

                                }
                                else{

                                    AddLoadOn(vehicle_name,load_id, load_time)  ;
                                }



                            }



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {

        };
        VolleySingleton.getInstance(this).addToRequestQueue(stringRequestabc);
    }

    public void AddLoadOn(final String vehicle_name, final String load_id, final String load_time){
        sqLiteDatabase.beginTransaction(); //getTimestamp
        String database = "loads";
        SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
        String driver_name =shared.getString("user_name", "");

        String SQLiteDataBaseQueryHolder = "INSERT INTO "+database+" (load_id,vehicle_name,drive_name,load_time,synce_status" +
                ") VALUES('"+load_id+"', '"+vehicle_name+"', '"+driver_name+"','"+load_time+"', 'synced');";
        sqLiteDatabase.execSQL(SQLiteDataBaseQueryHolder);
        sqLiteDatabase.setTransactionSuccessful();
        sqLiteDatabase.endTransaction();
        sendBroadcast(new Intent(EndPoints.ORDER_SAVED_BROADCAST+"del"));
    }
}
